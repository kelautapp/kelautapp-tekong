import i18n from "i18next";
// import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
import malay from "./utilities/i18next-malay";
import english from "./utilities/i18next-english";

i18n.use(initReactI18next).init({
  fallbackLng: "my",
  lng: "my",
  resources: {
    en: english,
    my: malay,
  },
  debug: false,

  // have a common namespace used around the full app
  ns: ["translations"],
  defaultNS: "translations",

  keySeparator: false, // we use content as keys

  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
