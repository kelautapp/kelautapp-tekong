import { toast } from "react-toastify";

export default class ToastrService {
  static success(content) {
    toast.success(content);
  }

  static error(content) {
    toast.error(content);
  }

  static warning(content) {
    toast.warning(content);
  }
}
