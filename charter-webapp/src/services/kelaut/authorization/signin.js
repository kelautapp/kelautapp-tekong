import { signIn } from "../../aws/authentication";

export class SignInProcess {
  static async onSignIn(data) {
    return await signIn(data);
  }

  static async init(data) {
    return await this.onSignIn(data);
  }
}
