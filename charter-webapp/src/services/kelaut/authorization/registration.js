import {
  AWSCognitoSignup,
  signUp,
  confirmSignUp,
} from "../../aws/authentication";
import { API, graphqlOperation } from "aws-amplify";
import { createProfile } from "../../../graphql/mutations";

export class PreRegistrationProcess {
  static async onSignUp(data) {
    return await signUp(data);
  }

  static convertToCognito(data) {
    return new AWSCognitoSignup(
      data.username,
      data.password,
      data.email,
      data.phoneNumber
    );
  }

  static async init(data) {
    const signup = this.convertToCognito(data);
    return await this.onSignUp(signup);
  }
}

export class ConfirmRegistrationProcess {
  static async onConfirmSignup(data) {
    return await confirmSignUp(data.username, data.code);
  }
  static async init(confirmData, userData) {
    await this.onConfirmSignup(confirmData);
    return await API.graphql(
      graphqlOperation(createProfile, { input: { ...userData } })
    );
  }
}
