import { Storage } from "aws-amplify";
import imageCompression from "browser-image-compression";

const compressOptions = {
  maxSizeMB: 1,
  maxWidthOrHeight: 1024,
  useWebWorker: false,
};
export async function uploadFile(file, filename) {
  try {
    const compressFile = await imageCompression(file, compressOptions);
    const uploadResult = await Storage.put(
      filename ? filename : file.name,
      compressFile,
      {
        contentType: compressFile.type,
      }
    );
    return uploadResult;
  } catch (error) {
    throw Error(error);
  }
}
