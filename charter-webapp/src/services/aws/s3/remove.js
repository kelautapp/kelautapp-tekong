import { Storage } from "aws-amplify";

export async function removeFile(filename) {
  try {
    const removeResult = await Storage.remove(filename);
    return removeResult;
  } catch (error) {
    throw Error(error);
  }
}
