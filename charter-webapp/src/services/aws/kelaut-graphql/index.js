import { API, graphqlOperation } from "aws-amplify";

export const KelautGraphql = async (schemaAction, schemData) => {
  const results = await API.graphql(
    graphqlOperation(schemaAction, { ...schemData })
  );
  if (results.hasOwnProperty("errors") && results.data === null) {
    throw new Error(results.errors[0].message);
  }

  return results;
};
