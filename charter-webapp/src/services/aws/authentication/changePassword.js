import { Auth } from "aws-amplify";

export async function changePassword(data) {
  try {
    const user = await Auth.currentAuthenticatedUser();
    // return user;
    const result = await Auth.changePassword(
      user,
      data.oldPassword,
      data.newPassword
    );
    return result;
  } catch (error) {
    throw error;
  }
}
