import { Auth } from "aws-amplify";

export async function signIn(data) {
  try {
    const user = await Auth.signIn(data.username, data.password);
    return user;
  } catch (error) {
    console.log("error signing in", error);
    throw error;
  }
}
