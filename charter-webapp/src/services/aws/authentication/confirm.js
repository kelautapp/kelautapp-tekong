import { Auth } from "aws-amplify";

export async function confirmSignUp(username, code) {
  try {
    const result = await Auth.confirmSignUp(username, code);
    return result;
  } catch (error) {
    throw new Error("Invalid code");
  }
}

export async function resendConfirmSignup(username) {
  try {
    const result = await Auth.resendSignUp(username);
    return result;
  } catch (error) {
    throw new Error("Resend code failed!");
  }
}
