import { Auth } from "aws-amplify";

// Send confirmation code to user's email

export async function forgotPassword(data) {
  try {
    const result = await Auth.forgotPassword(data);
    return result;
  } catch (error) {
    throw error;
  }
}

export async function forgotPasswordSubmit(data) {
  try {
    const result = await Auth.forgotPasswordSubmit(
      data.email,
      data.code,
      data.password
    );
    return result;
  } catch (error) {
    throw error;
  }
}
