import { Auth } from "aws-amplify";

export async function signUp(data) {
  try {
    const result = await Auth.signUp({
      ...data,
    });
    return result;
  } catch (error) {
    throw error;
  }
}

export class AWSCognitoSignup {
  username = null;
  password = null;
  attributes = {};
  constructor(username, password, email, phone_number) {
    this.username = username;
    this.password = password;
    this.attributes["email"] = email;
    this.attributes["phone_number"] = phone_number;
  }
}
