import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { FullApp } from "./app/FullApp";
import "./i18n";
import { register as registerServiceWorker } from "./serviceWorkerRegistration";
import { Amplify, Auth } from "aws-amplify";
import awsExport from "./aws-exports";
import { SetupContextState } from "./app/hooks";
import "react-datepicker/dist/react-datepicker.css";
import { LoaderContextState } from "./app/hooks/loader";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "react-loading-skeleton/dist/skeleton.css";
import "react-date-range/dist/styles.css"; // main style file
import "react-date-range/dist/theme/default.css"; // theme css file
import swConfig from "./swConfig";

Amplify.configure({
  ...awsExport,
  aws_appsync_graphqlEndpoint: process.env.REACT_APP_APPSYNC_GRAPHQL_ENDPOINT,
  aws_appsync_region: process.env.REACT_APP_APPSYNC_REGION,
  aws_appsync_authenticationType: process.env.REACT_APP_APPSYNC_AUTHENTICATION,
  aws_appsync_apiKey: process.env.REACT_APP_APPSYNC_API_KEY,
});
Auth.configure({
  authenticationFlowType: "USER_PASSWORD_AUTH",
});

ReactDOM.render(
  <BrowserRouter basename="/">
    <ToastContainer />
    <LoaderContextState>
      <SetupContextState>
        <FullApp />
      </SetupContextState>
    </LoaderContextState>
  </BrowserRouter>,
  document.getElementById("root")
);

registerServiceWorker(swConfig);
