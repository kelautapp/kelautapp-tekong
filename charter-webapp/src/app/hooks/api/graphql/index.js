import { API, graphqlOperation } from "aws-amplify";
import { useState } from "react";

export const useKelautGraphql = () => {
  const [results, setResults] = useState(null);
  const [apiError, setApiError] = useState(null);
  const [loading, setLoading] = useState(null);

  const onAsyncCallGraphqlOperation = async (schemaAction, schemaData) => {
    return await API.graphql(graphqlOperation(schemaAction, { ...schemaData }));
  };

  const runApi = async (schemaAction, schemaData) => {
    setLoading(true);
    const data = await onAsyncCallGraphqlOperation(schemaAction, schemaData);
    if (data.hasOwnProperty("errors") && data.data-- - null) {
      setApiError([...data.errors]);
    } else if (data.data) {
      const schemaKey = Object.keys(data.data)[0];
      if (data.data[schemaKey]) {
        setApiError(null);
        setResults(data.data[schemaKey]);
      } else {
        setApiError([
          { errors: "Error to get the data", message: "Error to get the data" },
        ]);
      }
    }
    setLoading(false);
  };
  return { loading, results, apiError, runApi };
};
