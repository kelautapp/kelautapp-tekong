import React, { useReducer } from "react";

export const SetupContext = React.createContext();
export const ADD_SETUP = "ADD_SETUP";

const reducer = (state, action) => {
  switch (action.type) {
    case "ADD_SETUP":
      return { ...state, user: action.payload, load: true };
    default:
      return state;
  }
};

export const SetupContextState = (props) => {
  const [setupState, dispatch] = useReducer(reducer, {
    user: null,
    load: null,
  });

  return (
    <SetupContext.Provider
      value={{ dispatch: dispatch, setupState: setupState }}
    >
      {props.children}
    </SetupContext.Provider>
  );
};
