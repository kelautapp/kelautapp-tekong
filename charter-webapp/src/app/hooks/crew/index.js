import { useState } from "react";

import {
  createCrew,
  updateCrew,
  updateCrewStatus,
} from "../../../graphql/mutations";
import { getCrewByProfileId } from "../../../graphql/queries";
import { KelautGraphql } from "../../../services/aws/kelaut-graphql";

export function useCrew() {
  const [loading, setLoading] = useState(null);
  const [data, setData] = useState(null);
  const [datas, setDatas] = useState([]);
  const [error, setError] = useState(null);
  const [complete, setComplete] = useState(null);
  const [completeUpdate, setCompleteUpdate] = useState(null);
  const [completeChangeStatus, setCompleteChangeStatus] = useState(null);

  async function onAsyncCreate(data) {
    return await KelautGraphql(createCrew, {
      input: { ...data },
    });
  }

  async function onAsyncUpdate(data) {
    return await KelautGraphql(updateCrew, {
      input: { ...data },
    });
  }

  async function onAsyncUpdateStatus(data) {
    return await KelautGraphql(updateCrewStatus, {
      input: { ...data },
    });
  }

  async function onAsynGet(profileId) {
    return await KelautGraphql(getCrewByProfileId, {
      profileId,
    });
  }

  async function onCreate(data) {
    let status = null;
    setComplete(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncCreate(data);
      if (result.data.createCrew) {
        setData({ ...result.data.createCrew });
        setDatas((prevData) => [...prevData, { ...result.data.createCrew }]);
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to register crew",
          code: "CrewRegistrationException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setComplete(status);
  }

  async function onUpdate(data) {
    let status = null;
    setCompleteUpdate(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdate(data);
      if (result.data.updateCrew) {
        setData({ ...result.data.updateCrew });
        setDatas((prevData) => {
          return prevData.map((d) => {
            if (d.id === result.data.updateCrew.id) {
              return { ...result.data.updateCrew };
            } else {
              return d;
            }
          });
        });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to register crew",
          code: "CrewRegistrationException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpdate(status);
  }

  async function onUpdateStatus(data) {
    let status = null;
    setCompleteChangeStatus(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdateStatus(data);
      if (result.data.updateCrewStatus) {
        setData({ ...result.data.updateCrewStatus });
        setDatas((prevData) => {
          return prevData.map((d) => {
            if (d.id === result.data.updateCrewStatus.id) {
              return { ...result.data.updateCrewStatus };
            } else {
              return d;
            }
          });
        });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to update crew status",
          code: "CrewRegistrationException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteChangeStatus(status);
  }

  async function onGet(profileId) {
    setLoading(true);
    setError(false);

    try {
      const result = await onAsynGet(profileId);
      setDatas([...result.data.getCrewByProfileId]);
    } catch (error) {
      setError(error.message);
    }
    setLoading(false);
  }

  return {
    loading,
    data,
    onCreate,
    complete,
    error,
    datas,
    setDatas,
    onGet,
    onUpdate,
    onUpdateStatus,
    completeChangeStatus,
    completeUpdate,
  };
}
