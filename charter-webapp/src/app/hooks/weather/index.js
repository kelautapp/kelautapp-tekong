import { useState } from "react";
import OpenMapHttp from "../../../services/api/weather";

export function useWeatherMap() {
  const [loading, setLoading] = useState(null);
  const [datas, setDatas] = useState([]);
  const [error, setError] = useState(null);
  const [complete, setComplete] = useState(null);

  async function getCurrentWeatherByCoordinate(lat, lon, cnt) {
    const d = new Date();
    let diff = d.getTimezoneOffset();
    setLoading(true);
    try {
      let results = await OpenMapHttp.get(
        `forecast/?lat=${lat}&lon=${lon}&APPID=${process.env.REACT_APP_OPEN_WEATHER_KEY}&units=metric&timezone=${diff}&cnt=${cnt}`
      );
      setDatas(results.data.list);
    } catch (error) {
      setError(error.message);
    }
    setComplete(true);
    setLoading(false);
  }

  return {
    loading,
    complete,
    error,
    datas,
    setDatas,
    getCurrentWeatherByCoordinate,
  };
}
