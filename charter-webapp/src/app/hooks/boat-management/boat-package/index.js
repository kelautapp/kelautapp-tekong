import { useState } from "react";
import {
  createBoatPackage,
  deleteBoatPackage,
  updateBoatPackage,
} from "../../../../graphql/mutations";
import { getBoatPackage } from "../../../../graphql/queries";

import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export function useBoatPackage() {
  const [loading, setLoading] = useState(null);
  const [getLoading, setGetLoading] = useState(null);
  const [data, setData] = useState(null);
  const [datas, setDatas] = useState([]);
  const [error, setError] = useState(null);
  const [complete, setComplete] = useState(null);
  const [completeDelete, setCompleteDelete] = useState(null);
  const [completeUpdate, setCompleteUpdate] = useState(null);

  async function onAsyncDelete(boatPackageId) {
    return await KelautGraphql(deleteBoatPackage, {
      input: { id: boatPackageId },
    });
  }

  async function onAsyncGet(boatId) {
    return await KelautGraphql(getBoatPackage, {
      boatId,
    });
  }

  async function onAsyncUpdate(data) {
    return await KelautGraphql(updateBoatPackage, {
      input: { ...data },
    });
  }

  async function onAsyncCreate(data) {
    return await KelautGraphql(createBoatPackage, {
      input: { ...data },
    });
  }

  async function onDelete(data) {
    let status = null;
    setCompleteDelete(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncDelete(data.id);
      if (result.data.deleteBoatPackage) {
        // setData({ ...result.data.deleteBoatPackage });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to delete boat package",
          code: "BoatPackageDeleteException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteDelete(status);
  }

  async function onCreate(data) {
    let status = null;
    setComplete(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncCreate(data);
      if (result.data.createBoatPackage) {
        setData({ ...result.data.createBoatPackage });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to create boat package",
          code: "BoatPackagePasswordException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setComplete(status);
  }

  async function onUpdate(data) {
    let status = null;
    setCompleteUpdate(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdate(data);
      setData({ ...result.data.updateBoatPackage });
      status = true;
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpdate(status);
  }

  async function onGet(boatId) {
    setGetLoading(true);
    setError(false);

    try {
      const result = await onAsyncGet(boatId);
      setDatas([...result.data.getBoatPackage]);
    } catch (error) {
      setError(error.message);
    }
    setGetLoading(false);
  }

  return {
    loading,
    data,
    onCreate,
    complete,
    error,
    onGet,
    datas,
    completeUpdate,
    onUpdate,
    getLoading,
    setDatas,
    onDelete,
    completeDelete,
  };
}
