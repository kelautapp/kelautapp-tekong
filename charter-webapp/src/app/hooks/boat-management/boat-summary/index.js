import { useState } from "react";
import { updateBoatSummary } from "../../../../graphql/mutations";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export function useBoatSummary() {
  const [loading, setLoading] = useState(null);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [completeUpdate, setCompleteUpdate] = useState(null);

  async function onAsyncUpdate(data) {
    return await KelautGraphql(updateBoatSummary, {
      input: { ...data },
    });
  }

  async function onUpdate(data) {
    let status = null;
    setCompleteUpdate(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdate(data);
      if (result.data.updateBoatSummary) {
        setData({ ...result.data.updateBoatSummary });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          code: "BoatSummaryUpdateFailed",
          message: "Fail to update boat summary",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpdate(status);
  }

  return {
    loading,
    data,
    error,
    completeUpdate,
    onUpdate,
    setData,
  };
}
