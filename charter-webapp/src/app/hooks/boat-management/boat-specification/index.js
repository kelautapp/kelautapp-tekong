import { useState } from "react";
import { updateBoatSpecification } from "../../../../graphql/mutations";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export function useBoatSpecification() {
  const [loading, setLoading] = useState(null);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [completeUpdate, setCompleteUpdate] = useState(null);

  async function onAsyncUpdate(data) {
    return await KelautGraphql(updateBoatSpecification, {
      input: { ...data },
    });
  }

  async function onUpdate(data) {
    let status = null;
    setCompleteUpdate(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdate(data);
      if (result.data.updateBoatSpecification) {
        setData({ ...result.data.updateBoatSpecification });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          code: "BoatSpecUpdateFailed",
          message: "Fail to update boat specification",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpdate(status);
  }

  return {
    loading,
    data,
    error,
    completeUpdate,
    onUpdate,
  };
}
