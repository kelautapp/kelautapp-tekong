import { useState } from "react";
import {
  createBoatFishingType,
  updateBoatFishingType,
} from "../../../../graphql/mutations";
import { getBoatFishingType } from "../../../../graphql/queries";

import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export function useBoatFishingType() {
  const [loading, setLoading] = useState(null);
  const [data, setData] = useState(null);
  const [datas, setDatas] = useState([]);
  const [error, setError] = useState(null);
  const [complete, setComplete] = useState(null);
  const [completeUpdate, setCompleteUpdate] = useState(null);

  async function onAsyncGet(boatId) {
    return await KelautGraphql(getBoatFishingType, {
      boatId,
    });
  }

  async function onAsyncUpdate(data) {
    return await KelautGraphql(updateBoatFishingType, {
      input: { ...data },
    });
  }

  async function onAsyncCreate(data) {
    return await KelautGraphql(createBoatFishingType, {
      input: { ...data },
    });
  }

  async function onCreate(data) {
    let status = null;
    setComplete(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncCreate(data);
      if (result.data.createBoatFishingType) {
        setData({ ...result.data.createBoatFishingType });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          code: "BoatFishingTypeFailed",
          message: "Fail to create boat type",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setComplete(status);
  }

  async function onUpdate(data) {
    let status = null;
    setCompleteUpdate(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdate(data);
      setData({ ...result.data.updateBoatFishingType });
      status = true;
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpdate(status);
  }

  async function onGet(boatId) {
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncGet(boatId);
      setDatas([...result.data.getBoatFishingType]);
    } catch (error) {
      setError(error.message);
    }
    setLoading(false);
  }

  return {
    loading,
    data,
    onCreate,
    complete,
    error,
    onGet,
    datas,
    completeUpdate,
    onUpdate,
  };
}
