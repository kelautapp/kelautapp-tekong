import { useState } from "react";
import {
  createBoat,
  updateBoat,
  updateBoatLocation,
  updateBoatStatus,
} from "../../../../graphql/mutations";
import { getBoat, getBoats } from "../../../../graphql/queries";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export default function useBoat() {
  const [errors, setErrors] = useState(null);
  const [loading, setLoading] = useState(null);
  const [boat, setBoat] = useState(null);
  const [boats, setBoats] = useState([]);
  const [completeCreateBoat, setCompleteCreateBoat] = useState(null);
  const [completeUpdateBoat, setCompleteUpdateBoat] = useState(null);

  const onAsyncUpdateBoatStatus = async (data) => {
    return await KelautGraphql(updateBoatStatus, {
      input: { ...data },
    });
  };

  const onAsyncCreateBoat = async (data) => {
    return await KelautGraphql(createBoat, {
      input: { ...data },
    });
  };

  const onAsyncUpdateBoat = async (data) => {
    return await KelautGraphql(updateBoat, {
      input: { ...data },
    });
  };

  const onAsyncGetBoat = async (id) => {
    return await KelautGraphql(getBoat, {
      id,
    });
  };

  const onAsyncGetBoats = async (profileId) => {
    return await KelautGraphql(getBoats, {
      profileId,
    });
  };

  const onAsyncUpdateBoatLocation = async (data) => {
    return await KelautGraphql(updateBoatLocation, {
      input: { ...data },
    });
  };

  const onCreateBoat = async (data) => {
    setCompleteCreateBoat(null);
    setErrors(null);
    setLoading(true);
    try {
      const resultBoat = await onAsyncCreateBoat(data);
      setBoat({ ...resultBoat.data.createBoat });
      setCompleteCreateBoat(true);
    } catch (error) {
      setErrors(error.message);
      setCompleteCreateBoat(false);
    }
    setLoading(false);
  };

  const onUpdateBoatStatus = async (data) => {
    setCompleteUpdateBoat(null);
    setErrors(null);
    setLoading(true);
    try {
      const resultBoat = await onAsyncUpdateBoatStatus(data);
      setBoat({ ...resultBoat.data.updateBoatStatus });
      setCompleteUpdateBoat(true);
    } catch (error) {
      setErrors(error.message);
      setCompleteUpdateBoat(false);
    }
    setLoading(false);
  };

  const onUpdateBoat = async (data) => {
    setCompleteUpdateBoat(null);
    setErrors(null);
    setLoading(true);
    try {
      const resultBoat = await onAsyncUpdateBoat(data);
      setBoat({ ...resultBoat.data.updateBoat });
      setCompleteUpdateBoat(true);
    } catch (error) {
      setErrors(error.message);
      setCompleteUpdateBoat(false);
    }
    setLoading(false);
  };

  const onGetBoat = async (id) => {
    setErrors(null);
    setLoading(true);
    try {
      const resultBoat = await onAsyncGetBoat(id);
      setBoat({ ...resultBoat.data.getBoat });
    } catch (error) {
      setErrors(error.message);
    }
    setLoading(false);
  };

  const onGetBoats = async (profileId) => {
    setErrors(null);
    setLoading(true);
    try {
      const resultBoat = await onAsyncGetBoats(profileId);
      setBoats([...resultBoat.data.getBoats]);
    } catch (error) {
      setErrors(error.message);
    }
    setLoading(false);
  };

  const onUpdateBoatLocation = async (data) => {
    setErrors(null);
    setLoading(true);
    try {
      const resultBoat = await onAsyncUpdateBoatLocation(data);
      setBoat({ ...resultBoat.data.updateBoatLocation });
    } catch (error) {
      setErrors(error.message);
    }
    setLoading(false);
  };

  return {
    errors,
    loading,
    onCreateBoat,
    boat,
    completeCreateBoat,
    onGetBoat,
    onUpdateBoatLocation,
    onUpdateBoat,
    completeUpdateBoat,
    onGetBoats,
    boats,
    onUpdateBoatStatus,
  };
}
