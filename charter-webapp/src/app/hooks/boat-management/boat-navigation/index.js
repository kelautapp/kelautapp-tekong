import { useState } from "react";
import {
  createBoatNavigation,
  updateBoatNavigation,
} from "../../../../graphql/mutations";
import { getBoatNavigation } from "../../../../graphql/queries";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export function useBoatNavigation() {
  const [loading, setLoading] = useState(null);
  const [data, setData] = useState(null);
  const [datas, setDatas] = useState([]);
  const [error, setError] = useState(null);
  const [complete, setComplete] = useState(null);
  const [completeUpdate, setCompleteUpdate] = useState(null);

  async function onAsyncGet(boatId) {
    return await KelautGraphql(getBoatNavigation, {
      boatId,
    });
  }

  async function onAsyncUpdate(data) {
    return await KelautGraphql(updateBoatNavigation, {
      input: { ...data },
    });
  }

  async function onAsyncCreate(data) {
    return await KelautGraphql(createBoatNavigation, {
      input: { ...data },
    });
  }

  async function onCreate(data) {
    let status = null;
    setComplete(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncCreate(data);
      setData({ ...result.data.createBoatNavigation });
      status = true;
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setComplete(status);
  }

  async function onUpdate(data) {
    let status = null;
    setCompleteUpdate(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdate(data);
      setData({ ...result.data.updateBoatNavigation });
      status = true;
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpdate(status);
  }

  async function onGet(boatId) {
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncGet(boatId);
      setDatas([...result.data.getBoatNavigation]);
    } catch (error) {
      setError(error.message);
    }
    setLoading(false);
  }

  return {
    loading,
    data,
    onCreate,
    complete,
    error,
    onGet,
    datas,
    completeUpdate,
    onUpdate,
  };
}
