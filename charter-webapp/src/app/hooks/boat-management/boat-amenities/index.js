import { useState } from "react";
import { createBoatAmenities } from "../../../../graphql/mutations";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export function useBoatAmenities() {
  const [loading, setLoading] = useState(null);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [complete, setComplete] = useState(null);

  async function onAsyncCreate(data) {
    return await KelautGraphql(createBoatAmenities, {
      input: { ...data },
    });
  }

  async function onCreate(data) {
    let status = null;
    setComplete(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncCreate(data);
      if (result.data.createBoatAmenities) {
        setData({ ...result.data.createBoatAmenities });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          code: "BoatSpecAmenitiesFailed",
          message: "Fail to create boat amenities",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setComplete(status);
  }

  return {
    loading,
    data,
    error,
    complete,
    onCreate,
  };
}
