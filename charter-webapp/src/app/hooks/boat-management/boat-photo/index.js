import { useState } from "react";
import {
  createBoatPhoto,
  deleteBoatPhoto,
} from "../../../../graphql/mutations";
import { getBoatPhoto } from "../../../../graphql/queries";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";
import { uploadFile } from "../../../../services/aws/s3/upload";
import { Storage } from "aws-amplify";

export function useBoatPhoto() {
  const [loading, setLoading] = useState(null);
  const [imageData, setImageData] = useState([]);
  const [error, setError] = useState(null);
  const [completeDelete, setCompleteDelete] = useState(null);

  const [completeUpload, setCompleteUpload] = useState(null);

  async function combineImageUpload(data) {
    const s3Result = await uploadImageToS3(data);
    const fileData = {
      name: data.file.name,
      type: data.file.type,
      size: data.file.size,
      fileKey: s3Result.key,
      boatId: data.boatId,
    };
    const boatPhotoResult = await onAsynCreate(fileData);
    return boatPhotoResult;
  }

  async function onAsyncGet(boatId) {
    return await KelautGraphql(getBoatPhoto, {
      boatId,
    });
  }

  async function onAsyncDelete(id) {
    return await KelautGraphql(deleteBoatPhoto, {
      input: { id },
    });
  }

  async function onAsynCreate(data) {
    return await KelautGraphql(createBoatPhoto, {
      input: { ...data },
    });
  }

  async function processData(data) {
    let listOfPromise = [];
    data.map((d) => listOfPromise.push(combineImageUpload(d)));
    const photoResult = await Promise.all(listOfPromise);
    return photoResult;
  }

  async function uploadImageToS3(data) {
    const result = await uploadFile(data.file, data.name);
    return result;
  }

  async function getImageBucket(key) {
    const signedURL = await Storage.get(key, {
      level: "public", // defaults to `public`
    }); // get key from Storage.list
    return signedURL;
  }

  async function onUpload(data) {
    let status = null;
    setCompleteUpload(status);
    setLoading(true);
    setError(false);

    try {
      const result = await processData(data);
      if (result.length > 0) {
        let tempPhoto = await Promise.all(
          result.map(async (r) => {
            let temp = { ...r.data.createBoatPhoto };
            const url = await getImageBucket(temp.fileKey);
            return (temp = { ...temp, url });
          })
        );
        status = true;
        setImageData((prevData) => [...prevData, ...tempPhoto]);
        // return result;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          code: "BoatPhotoUploadFail",
          message: "Fail to upload boat photo",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpload(status);
  }

  async function onGet(boatId) {
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncGet(boatId);
      if (result.data.getBoatPhoto) {
        let tempPhoto = [...result.data.getBoatPhoto];
        tempPhoto = await Promise.all(
          tempPhoto.map(async (photo) => {
            const url = await getImageBucket(photo.fileKey);
            return {
              ...photo,
              url: url,
            };
          })
        );
        // setImageData([...tempPhoto]);
        setImageData((prevData) => [...prevData, ...tempPhoto]);
        // return result;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to get boat photo",
          code: "BoatPhotoExtractionException",
        };
      }
    } catch (error) {
      setError(error.message);
    }
    setLoading(false);
  }

  async function onDelete(id) {
    setLoading(true);
    setError(false);
    try {
      const result = await onAsyncDelete(id);
      if (result.data.deleteBoatPhoto) {
        if (imageData.length > 0) {
          setImageData((prevData) =>
            prevData.filter(
              (data) => data.id !== result.data.deleteBoatPhoto.id
            )
          );
        }
        // setDeletedImage([...result.data.deleteBoatPhoto]);
        setCompleteDelete(true);
      } else {
        setCompleteDelete(false);
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to delete boat photo",
          code: "BoatPhotoDeleteException",
        };
      }
    } catch (error) {
      setError(error.message);
    }
    setLoading(false);
  }

  // useEffect(() => {
  //   console.log(imageData, "image data");
  // }, [imageData]);

  return {
    loading,
    imageData,
    error,
    completeUpload,
    onUpload,
    onGet,
    completeDelete,
    onDelete,
  };
}
