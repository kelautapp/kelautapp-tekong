import { useState } from "react";
import {
  createBoatEngine,
  updateBoatEngine,
} from "../../../../graphql/mutations";
import { getBoatEngine } from "../../../../graphql/queries";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export function useBoatEngine() {
  const [loading, setLoading] = useState(null);
  const [boatEngine, setBoatEngine] = useState(null);
  const [boatEngines, setBoatEngines] = useState([]);
  const [error, setError] = useState(null);
  const [completeBoatEngine, setCompleteBoatEngine] = useState(null);
  const [completeUpdateBoatEngine, setCompleteUpdateBoatEngine] =
    useState(null);

  async function onAsyncGetBoatEngine(boatId) {
    return await KelautGraphql(getBoatEngine, {
      boatId,
    });
  }

  async function onAsyncUpdateBoatEngine(data) {
    return await KelautGraphql(updateBoatEngine, {
      input: { ...data },
    });
  }

  async function onAsyncCreateBoatEngine(data) {
    return await KelautGraphql(createBoatEngine, {
      input: { ...data },
    });
  }

  async function onCreateBoatEngine(data) {
    let status = null;
    setCompleteBoatEngine(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncCreateBoatEngine(data);
      setBoatEngine({ ...result.data.createBoatEngine });
      status = true;
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteBoatEngine(status);
  }

  async function onUpdateBoatEngine(data) {
    let status = null;
    setCompleteUpdateBoatEngine(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdateBoatEngine(data);
      setBoatEngine({ ...result.data.updateBoatEngine });
      status = true;
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpdateBoatEngine(status);
  }

  async function onGetBoatEngine(boatId) {
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncGetBoatEngine(boatId);
      setBoatEngines([...result.data.getBoatEngine]);
    } catch (error) {
      setError(error.message);
    }
    setLoading(false);
  }

  return {
    loading,
    boatEngine,
    onCreateBoatEngine,
    completeBoatEngine,
    error,
    onGetBoatEngine,
    boatEngines,
    completeUpdateBoatEngine,
    onUpdateBoatEngine,
  };
}
