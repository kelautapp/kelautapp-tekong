import { useState } from "react";
import {
  createBoatCrew,
  deleteBoatCrew,
  updateBoatCrew,
} from "../../../../graphql/mutations";

import { updateCrewStatus } from "../../../../graphql/mutations";
import { getBoatCrewByBoatId } from "../../../../graphql/queries";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";
import { Storage } from "aws-amplify";

export function useBoatCrew() {
  const [loading, setLoading] = useState(null);
  const [data, setData] = useState(null);
  const [datas, setDatas] = useState([]);
  const [error, setError] = useState(null);
  const [complete, setComplete] = useState(null);
  const [completeUpdate, setCompleteUpdate] = useState(null);
  const [completeDelete, setCompleteDelete] = useState(null);
  const [completeChangeStatus, setCompleteChangeStatus] = useState(null);

  async function getImageBucket(key, data) {
    if (key && key !== "null") {
      const signedURL = await Storage.get(key, {
        level: "public", // defaults to `public`
      }); // get key from Storage.list
      return { ...data, photo: signedURL };
    }
    return {
      ...data,
      photo: null,
    };
  }

  async function onAsyncCreate(data) {
    return await KelautGraphql(createBoatCrew, {
      input: { ...data },
    });
  }

  async function onAsyncDelete(id) {
    return await KelautGraphql(deleteBoatCrew, {
      input: { id },
    });
  }

  async function onAsyncUpdate(data) {
    return await KelautGraphql(updateBoatCrew, {
      input: { ...data },
    });
  }

  async function onAsyncUpdateStatus(data) {
    return await KelautGraphql(updateCrewStatus, {
      input: { ...data },
    });
  }

  async function onAsynGet(boatId) {
    return await KelautGraphql(getBoatCrewByBoatId, {
      boatId,
    });
  }

  async function onCreate(data) {
    let status = null;
    setComplete(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncCreate(data);
      if (result.data.createBoatCrew) {
        let dataWithPhoto = await getImageBucket(
          result.data.createBoatCrew.photo,
          result.data.createBoatCrew
        );
        setData({ ...result.data.createBoatCrew });
        setDatas((prevData) => [...prevData, { ...dataWithPhoto }]);
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to register boat crew",
          code: "BoatCrewRegistrationException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setComplete(status);
  }

  async function onUpdate(data) {
    let status = null;
    setCompleteUpdate(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdate(data);
      if (result.data.updateBoatCrew) {
        setData({ ...result.data.updateBoatCrew });
        setDatas((prevData) => {
          return prevData.map((d) => {
            if (d.id === result.data.updateBoatCrew.id) {
              return { ...result.data.updateBoatCrew, photo: d.photo };
            } else {
              return d;
            }
          });
        });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to update boat crew",
          code: "BoatCrewUpdateException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpdate(status);
  }

  async function onDelete(id) {
    let status = null;
    setCompleteDelete(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncDelete(id);
      if (result.data.deleteBoatCrew) {
        setData({ ...result.data.deleteBoatCrew });
        setDatas((prevData) => {
          return prevData.filter((d) => d.id !== id);
        });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to delete crew",
          code: "BoatCrewDeleteException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteDelete(status);
  }

  async function onUpdateStatus(data) {
    let status = null;
    setCompleteChangeStatus(status);
    setLoading(true);
    setError(false);

    try {
      const result = await onAsyncUpdateStatus(data);
      if (result.data.updateCrewStatus) {
        setData({ ...result.data.updateCrewStatus });
        setDatas((prevData) => {
          return prevData.map((d) => {
            if (d.id === result.data.updateCrewStatus.id) {
              return { ...result.data.updateCrewStatus };
            } else {
              return d;
            }
          });
        });
        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to update crew status",
          code: "CrewRegistrationException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteChangeStatus(status);
  }

  async function onUpdateV2(datas) {
    let status = null;
    setCompleteUpdate(status);
    setLoading(true);
    setError(false);

    try {
      let listOfPromises = datas.map((d) => onAsyncUpdate(d));
      let results = await Promise.all(listOfPromises);
      if (results.length > 0) {
        // eslint-disable-next-line
        results.map((r) => {
          if (r.data.updateBoatCrew) {
            // setData({ ...result.data.updateCrewStatus });
            setDatas((prevData) => {
              return prevData.map((d) => {
                if (d.id === r.data.updateBoatCrew.id) {
                  return { ...r.data.updateBoatCrew, photo: d.photo };
                } else {
                  return d;
                }
              });
            });
          }
        });

        // const result = await onAsyncUpdateStatus(datas);

        status = true;
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to update crew status",
          code: "CrewRegistrationException",
        };
      }
    } catch (error) {
      status = false;
      setError(error.message);
    }
    setLoading(false);
    setCompleteUpdate(status);
  }

  async function onGet(boatId) {
    setLoading(true);
    setError(false);

    try {
      const result = await onAsynGet(boatId);
      if (result.data.getBoatCrewByBoatId.length > 0) {
        let listOfPromise = [];
        result.data.getBoatCrewByBoatId.map((d) =>
          listOfPromise.push(getImageBucket(d.photo, d))
        );
        const photoResult = await Promise.all(listOfPromise);
        setDatas([...photoResult]);
      }
    } catch (error) {
      setError(error.message);
    }
    setLoading(false);
  }

  return {
    loading,
    data,
    onCreate,
    complete,
    error,
    datas,
    setDatas,
    onGet,
    onUpdate,
    onUpdateStatus,
    completeChangeStatus,
    onDelete,
    completeUpdate,
    completeDelete,
    onUpdateV2,
  };
}
