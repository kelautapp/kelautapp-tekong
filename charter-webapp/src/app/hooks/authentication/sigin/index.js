import { Storage } from "aws-amplify";
import { useState, useContext } from "react";
import { ADD_SETUP, SetupContext } from "../..";
import { getProfileByEmail } from "../../../../graphql/queries";
import { signIn } from "../../../../services/aws/authentication";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export default function useSign() {
  const [errors, setErrors] = useState(null);
  const [success, setSuccess] = useState(null);
  const [loading, setLoading] = useState(null);
  const [user, setUser] = useState(null);
  const [confirm, setConfirm] = useState(false);
  const { setupState, dispatch } = useContext(SetupContext);
  const [userData, setUserData] = useState(null);

  const onAsyncSignIn = async (data) => {
    return await signIn(data);
  };

  async function generateUrl(key) {
    return await Storage.get(key, {
      level: "public", // defaults to `public`
    });
  }

  const onSignIn = async (data) => {
    setUserData({ ...data });
    setErrors(null);
    setLoading(true);
    try {
      const signInResult = await onAsyncSignIn(data);
      if (signInResult) {
        setUser(signInResult);
        setSuccess(true);
        setLoading(false);
        const userProfile = await KelautGraphql(getProfileByEmail, {
          email: signInResult.attributes.email,
        });
        const photoUrl =
          userProfile.data.getProfileByEmail.photo &&
          userProfile.data.getProfileByEmail.photo.toLowerCase() !== "null"
            ? await generateUrl(userProfile.data.getProfileByEmail.photo)
            : null;
        const backdropUrl =
          userProfile.data.getProfileByEmail.backdrop &&
          userProfile.data.getProfileByEmail.backdrop.toLowerCase() !== "null"
            ? await generateUrl(userProfile.data.getProfileByEmail.backdrop)
            : null;
        dispatch({
          type: ADD_SETUP,
          payload: {
            ...signInResult.attributes,
            profileId: userProfile.data.getProfileByEmail.id,
            name: userProfile.data.getProfileByEmail.name,
            profileDetails: {
              ...userProfile.data.getProfileByEmail,
              photoUrl,
              backdropUrl,
            },
          },
        });
      } else {
        throw new Error("Invalid credential");
      }
    } catch (error) {
      if (error.code === "UserNotConfirmedException") {
        setConfirm(true);
      } else {
        console.log(error, "error");
        setErrors("Invalid credentials");
      }
      setLoading(false);
    }
  };

  return {
    loading,
    errors,
    success,
    onSignIn,
    user,
    setupState,
    confirm,
    userData,
    setConfirm,
  };
}
