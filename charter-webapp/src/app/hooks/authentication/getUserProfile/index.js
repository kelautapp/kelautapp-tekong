import { useState, useEffect, useContext } from "react";
import { SetupContext } from "../..";
import {
  getProfileByEmail,
  getVerificationByProfileId,
} from "../../../../graphql/queries";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export default function useGetUserProfile() {
  const [loading, setLoading] = useState(null);
  const [userProfile, setUserProfile] = useState(null);
  const [verification, setVerification] = useState(null);
  const { setupState } = useContext(SetupContext);
  const [errors, setErrors] = useState(null);
  // call api to get user profile by email
  const onAsyncGetUserProfile = async (email) => {
    return await KelautGraphql(getProfileByEmail, { email });
  };

  // call api to get verification request by profile id
  const onAsyncGetVerificationRequestByProfile = async (profileId) => {
    return await KelautGraphql(getVerificationByProfileId, { profileId });
  };

  // function to current user profile
  const getUserProfile = async (email) => {
    try {
      const userProfileData = await onAsyncGetUserProfile(email);
      if (userProfileData && userProfileData.data.getProfileByEmail)
        setUserProfile({ ...userProfileData.data.getProfileByEmail });
      else throw new Error("Error to get profile data");
    } catch (error) {
      setErrors("Error to get profile data");
    }
  };

  //   function to get verification data
  const getVerificationRequestBasedOnProfile = async (profileId) => {
    try {
      const verificationResult = await onAsyncGetVerificationRequestByProfile(
        profileId
      );
      if (
        verificationResult &&
        verificationResult.data.getVerificationByProfileId
      )
        setVerification({
          ...verificationResult.data.getVerificationByProfileId,
        });
    } catch (error) {
      setErrors(error);
    }

    setLoading(false);
  };

  const runInitialOperation = async () => {
    setErrors(null);
    setLoading(true);
    await getUserProfile(setupState.user.email);
  };
  useEffect(() => {
    if (setupState.load) {
      runInitialOperation();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setupState]);

  useEffect(() => {
    if (userProfile) {
      getVerificationRequestBasedOnProfile(userProfile.id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userProfile]);

  useEffect(() => {
    if (verification) {
      setLoading(false);
    }
  }, [verification]);

  return {
    loading,
    setupState,
    userProfile,
    verification,
    errors,
    runInitialOperation,
  };
}
