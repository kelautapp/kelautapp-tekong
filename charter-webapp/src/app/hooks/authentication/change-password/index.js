import { useState } from "react";
import { changePassword } from "../../../../services/aws/authentication/changePassword";

export function useChangePassword() {
  const [loading, setLoading] = useState(null);
  const [completeChangePassword, setCompleteChangePassword] = useState(null);
  const [error, setError] = useState(null);

  const onAsyncChangePassword = async (data) => {
    return await changePassword(data);
  };

  const onChangePassword = async (data) => {
    setError(null);
    setLoading(true);
    try {
      const result = await onAsyncChangePassword(data);
      if (result) {
        setCompleteChangePassword(true);
      } else {
        // eslint-disable-next-line no-throw-literal
        throw {
          message: "Failed to change password",
          code: "ChangePasswordException",
        };
      }
    } catch (error) {
      setError(error.message);
    }
    setLoading(false);
  };
  return { loading, error, completeChangePassword, onChangePassword };
}
