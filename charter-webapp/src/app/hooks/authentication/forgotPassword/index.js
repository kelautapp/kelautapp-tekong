import { useState } from "react";
import {
  forgotPassword,
  forgotPasswordSubmit,
} from "../../../../services/aws/authentication/resetPassword";

export function useForgotPassword() {
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);
  const [completeSendCode, setCompleteSendCode] = useState(null);
  const [completeForgotPassword, setCompleteForgotPassword] = useState(null);

  const onAsyncConfirmForgotPassword = async (data) => {
    return await forgotPasswordSubmit(data);
  };
  const onAsyncForgotPasswordSendCode = async (data) => {
    return await forgotPassword(data);
  };

  const onConfirmPassword = async (data) => {
    setLoading(true);
    try {
      await onAsyncConfirmForgotPassword(data);
      setLoading(false);
      setCompleteForgotPassword(true);
    } catch (error) {
      setLoading(false);
      setError(error.message);
    }
  };

  const onSendPasswordCode = async (data) => {
    setError(null);
    setLoading(true);
    try {
      await onAsyncForgotPasswordSendCode(data);
      // return result;
      setCompleteSendCode(true);
    } catch (error) {
      setError(error.message);
    }

    setLoading(false);
  };

  return {
    loading,
    error,
    completeSendCode,
    completeForgotPassword,
    onSendPasswordCode,
    onConfirmPassword,
  };
}
