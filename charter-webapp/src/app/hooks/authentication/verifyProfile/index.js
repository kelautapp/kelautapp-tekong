import { useState, useEffect } from "react";
import { API, graphqlOperation } from "aws-amplify";
import { updateProfile } from "../../../../graphql/mutations";

const ProfileStatus = Object.freeze({
  PENDING: "PENDING",
  PENDING_REVIEW: "PENDING_REVIEW",
});

class UserProfile {
  constructor(data) {
    this.id = data.id;
    this.name = data.name;
    this.email = data.email;
    this.companyName = data.companyName;
    this.location = data.location;
    this.phone = data.phone;
    this.birthDate = data.birthDate;
    this.language = data.language;
    this.nationality = data.nationality;
    this.startCareer = data.startCareer;
    this.photo = data.photo;
    this.backdrop = data.backdrop;
    this.status = ProfileStatus.PENDING;
  }

  setStatusToPendingToReview() {
    this.status = ProfileStatus.PENDING_REVIEW;
  }
}

export default function useUserProfileVerification(data) {
  const [rawData, setRawData] = useState(data);
  const [profile, setProfile] = useState(null);
  const [verified] = useState(null);
  const [icFront] = useState(null);
  const [icBack] = useState(null);
  const [loading, setLoading] = useState(null);
  const [operationInitiator, setOperationInitiator] = useState(0);
  const [complete, setComplete] = useState(null);

  // handle user profile object creation
  const onHandleUserProfileObjectCreation = (data) => {
    const tempUser = new UserProfile(data);
    tempUser.setStatusToPendingToReview();
    return tempUser;
  };

  //   handle update profile
  const onAsyncHandleUpdateUserProfile = async (profileData) => {
    return await API.graphql(
      graphqlOperation(updateProfile, {
        input: { ...profileData },
      })
    );
  };

  const runOperation = () => {
    setOperationInitiator((prevState) => prevState + 1);
  };

  useEffect(() => {
    const verifyOperation = async () => {
      try {
        setComplete(null);
        setLoading(true);
        const updatedProfileData = onHandleUserProfileObjectCreation(rawData);

        const latestProfile = await onAsyncHandleUpdateUserProfile(
          updatedProfileData
        );
        setProfile({ ...latestProfile.data.updateProfile });
        setComplete(true);
      } catch (error) {
        console.log(error, "error when verify user profile");
      }
      setLoading(false);
    };
    if (operationInitiator > 0) {
      verifyOperation();
    }
  }, [rawData, operationInitiator]);

  return {
    profile,
    verified,
    icFront,
    icBack,
    setRawData,
    runOperation,
    loading,
    complete,
  };
}
