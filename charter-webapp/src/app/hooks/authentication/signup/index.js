import { useState } from "react";
import { createProfile } from "../../../../graphql/mutations";

import { getProfileByEmail } from "../../../../graphql/queries";

import {
  AWSCognitoSignup,
  confirmSignUp,
  resendConfirmSignup,
  signUp,
} from "../../../../services/aws/authentication";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";
import { useTranslation } from "react-i18next";

export default function useSignUp() {
  const [t] = useTranslation();
  const [errors, setErrors] = useState(null);
  const [loading, setLoading] = useState(null);
  const [waitingConfirm, setWaitingConfirm] = useState(false);
  const [completeSignUp, setCompleteSignUp] = useState(false);
  const [profile, setProfile] = useState(false);
  const [sucessResend, setSuccessResend] = useState(null);

  const onAsyncSignup = async (data) => {
    return await signUp(data);
  };

  const onConvertToCognitoFormat = (data) => {
    return new AWSCognitoSignup(
      data.username,
      data.password,
      data.email,
      data.phoneNumber
    );
  };

  const onAsynConfirmSignUp = async (data) => {
    return await confirmSignUp(data.username, data.code);
  };

  const onAsyncResendConfirmation = async (username) => {
    return await resendConfirmSignup(username);
  };

  const onPreRegistrationProcess = async (data) => {
    setErrors(null);
    setLoading(true);
    try {
      const existing = await KelautGraphql(getProfileByEmail, {
        email: data.email,
      });
      if (existing.data.getProfileByEmail == null) {
        const tempRegisterData = {
          email: data.email,
          phone: data.phoneNumber,
        };
        const profile = await KelautGraphql(createProfile, {
          input: { ...tempRegisterData },
        });

        if (profile.data.createProfile === null)
          // eslint-disable-next-line no-throw-literal
          throw { code: "fail to register", message: "please try again" };
        setProfile({ ...profile });
      }
      const cognitoDataFormat = onConvertToCognitoFormat(data);
      await onAsyncSignup(cognitoDataFormat);
      setWaitingConfirm(true);
    } catch (error) {
      if (error.code === "UsernameExistsException") {
        setErrors(t("UsernameExistsException"));
      } else {
        setErrors(error.message);
      }
    }
    setLoading(false);
  };

  const onConfirmRegistrationProcess = async (confirmData) => {
    setErrors(null);
    setLoading(true);
    try {
      // const existing = await KelautGraphql(getProfileByEmail, {
      //   email: confirmData.username,
      // });
      // if (existing.data.getProfileByEmail == null) {
      //   const profile = await KelautGraphql(createProfile, {
      //     input: { ...userData },
      //   });
      //   setProfile({ ...profile });
      // }

      await onAsynConfirmSignUp(confirmData);

      setWaitingConfirm(false);
      setLoading(false);
      setCompleteSignUp(true);
    } catch (error) {
      console.log(error);
      setLoading(false);
      setErrors("Invalid code");
    }

    setCompleteSignUp(false);
  };

  const onResendConfirmation = async (username) => {
    setSuccessResend(null);
    setLoading(true);
    try {
      await onAsyncResendConfirmation(username);
      setSuccessResend(true);
    } catch (error) {
      setErrors(t("FailedResend"));
      setSuccessResend(false);
    }

    setLoading(false);
  };

  return {
    errors,
    loading,
    waitingConfirm,
    onPreRegistrationProcess,
    onConfirmRegistrationProcess,
    completeSignUp,
    profile,
    onResendConfirmation,
    sucessResend,
  };
}
