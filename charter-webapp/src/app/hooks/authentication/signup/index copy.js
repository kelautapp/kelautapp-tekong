import { useState } from "react";
import { createProfile } from "../../../../graphql/mutations";

import { getProfileByEmail } from "../../../../graphql/queries";

import {
  AWSCognitoSignup,
  confirmSignUp,
  resendConfirmSignup,
  signUp,
} from "../../../../services/aws/authentication";
import { KelautGraphql } from "../../../../services/aws/kelaut-graphql";

export default function useSignUp() {
  const [errors, setErrors] = useState(null);
  const [loading, setLoading] = useState(null);
  const [waitingConfirm, setWaitingConfirm] = useState(false);
  const [completeSignUp, setCompleteSignUp] = useState(false);
  const [profile, setProfile] = useState(false);
  const [sucessResend, setSuccessResend] = useState(null);

  const onAsyncSignup = async (data) => {
    return await signUp(data);
  };

  const onConvertToCognitoFormat = (data) => {
    return new AWSCognitoSignup(
      data.username,
      data.password,
      data.email,
      data.phoneNumber
    );
  };

  const onAsynConfirmSignUp = async (data) => {
    return await confirmSignUp(data.username, data.code);
  };

  const onAsyncResendConfirmation = async (username) => {
    return await resendConfirmSignup(username);
  };

  const onPreRegistrationProcess = async (data) => {
    setErrors(null);
    setLoading(true);
    try {
      const cognitoDataFormat = onConvertToCognitoFormat(data);
      const result = await onAsyncSignup(cognitoDataFormat);
      setWaitingConfirm(true);
    } catch (error) {
      if (error.code === "UsernameExistsException") {
        setErrors("Account already exist! Please use other email account");
      } else {
        setErrors(error.message);
      }
    }
    setLoading(false);
  };

  const onConfirmRegistrationProcess = async (confirmData, userData) => {
    setErrors(null);
    setLoading(true);
    try {
      const existing = await KelautGraphql(getProfileByEmail, {
        email: confirmData.username,
      });
      if (existing.data.getProfileByEmail == null) {
        const profile = await KelautGraphql(createProfile, {
          input: { ...userData },
        });
        setProfile({ ...profile });
      }

      await onAsynConfirmSignUp(confirmData);

      setWaitingConfirm(false);
      setLoading(false);
      setCompleteSignUp(true);
    } catch (error) {
      console.log(error);
      setLoading(false);
      setErrors("Invalid code");
    }

    setCompleteSignUp(false);
  };

  const onResendConfirmation = async (username) => {
    setSuccessResend(null);
    setLoading(true);
    try {
      await onAsyncResendConfirmation(username);
      setSuccessResend(true);
    } catch (error) {
      setErrors("Failed to resend confirmation code");
      setSuccessResend(false);
    }

    setLoading(false);
  };

  return {
    errors,
    loading,
    waitingConfirm,
    onPreRegistrationProcess,
    onConfirmRegistrationProcess,
    completeSignUp,
    profile,
    onResendConfirmation,
    sucessResend,
  };
}
