import React, { useState } from "react";
import { LoaderComponent } from "./components/loader";

export const LoaderContext = React.createContext();

export const LoaderContextState = (props) => {
  const [loading, setLoading] = useState(false);
  return (
    <LoaderContext.Provider value={{ loading, setLoading }}>
      {loading && <LoaderComponent />}
      {props.children}
    </LoaderContext.Provider>
  );
};
