import React from "react";

export const LoaderComponent = () => {
  return (
    <div className="loader-container">
      <div className="loader">
        <div className="loader__bar"></div>
        <div className="loader__bar"></div>
        <div className="loader__bar"></div>
        <div className="loader__bar"></div>
        <div className="loader__bar"></div>
        <div className="loader__ball"></div>
      </div>
      <p>Loading</p>
    </div>
  );
};
