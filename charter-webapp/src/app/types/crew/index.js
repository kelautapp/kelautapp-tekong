export class Crew {
  constructor(data) {
    this.name = data.name;
    this.idNumber = data.idNumber;
    this.dob = data.dob;
    this.photo = data.photo;
    this.profileId = null;
  }

  setProfileId(profileId) {
    this.profileId = profileId;
  }
}

export class UpdateCrew {
  constructor(data) {
    this.id = data.id;
    this.name = data.name;
    this.idNumber = data.idNumber;
    this.dob = data.dob;
    this.photo = data.photo;
  }
}

export class UpdateCrewStatus {
  constructor(data) {
    this.id = data.id;
    this.status = null;
    this.name = data.name;
  }
}
