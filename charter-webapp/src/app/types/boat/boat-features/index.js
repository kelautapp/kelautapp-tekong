export class CreateBoatFeatures {
  constructor(data) {
    this.featureName = data.featureName;
    this.boatId = null;
  }

  setBoatId(boatId) {
    this.boatId = boatId;
  }
}

export class UpdateBoatFeatures {
  constructor(data) {
    this.featureName = data.featureName;
    this.id = data.id;
  }
}
