export class CreateBoatPackage {
  constructor(data) {
    this.packageName = data.packageName;
    this.tripHour = data.tripHour;
    this.startTime = data.startTime;
    this.description = data.description;
    this.price = data.price;
    this.boatId = null;
  }

  setBoatId(boatId) {
    this.boatId = boatId;
  }
}

export class UpdateBoatPackage {
  constructor(data) {
    this.packageName = data.packageName;
    this.tripHour = data.tripHour;
    this.startTime = data.startTime;
    this.description = data.description;
    this.price = data.price;
    this.id = data.id;
  }
}
