export class UpdateBoatSpecification {
  constructor(data) {
    this.id = data.id;
    this.horsePower = data.horsePower;
    this.maxSpeed = data.maxSpeed;
    this.length = data.length;
    this.capacity = data.capacity;
    this.year = data.year;
  }
}
