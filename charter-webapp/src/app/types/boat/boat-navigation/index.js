export class CreateBoatNavigation {
  constructor(data) {
    this.deviceName = data.deviceName;
    this.boatId = null;
  }

  setBoatId(boatId) {
    this.boatId = boatId;
  }
}

export class UpdateBoatNavigation {
  constructor(data) {
    this.deviceName = data.deviceName;
    this.id = data.id;
  }
}
