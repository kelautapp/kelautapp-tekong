export class CreateBoatCrew {
  constructor(data) {
    this.name = data.name;
    this.idNumber = data.idNumber;
    this.dob = data.dob;
    this.photo = data.photo;
    this.crewId = data.id;
    this.role = "Crew";
    this.boatId = null;
    this.profileId = data.profileId;
  }
}
