export class CreateBoatEngine {
  constructor(data) {
    this.manufacturer = data.manufacturer;
    this.engineCount = data.engineCount;
    this.horsePower = data.engineCount;
    this.speed = data.speed;
    this.boatId = null;
  }

  setBoatId(boatId) {
    this.boatId = boatId;
  }
}

export class UpdateBoatEngine {
  constructor(data) {
    this.manufacturer = data.manufacturer;
    this.engineCount = data.engineCount;
    this.horsePower = data.engineCount;
    this.speed = data.speed;
    this.id = data.id;
  }
}
