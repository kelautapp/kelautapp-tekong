export class CreateBoat {
  constructor() {
    this.name = null;
    this.description = null;
    this.horsePower = null;
    this.maxSpeed = null;
    this.length = null;
    this.capacity = null;
    this.year = null;
    this.profileId = null;
  }

  setData(data) {
    this.name = data.name;
    this.description = data.description;
    this.horsePower = data.horsePower;
    this.maxSpeed = data.maxSpeed;
    this.length = data.length;
    this.capacity = data.capacity;
    this.year = data.year;
  }

  setProfileId(profileId) {
    this.profileId = profileId;
  }
}

export class UpdateBoatSummary {
  constructor() {
    this.id = null;
    this.name = null;
    this.description = null;
    this.lat = null;
    this.lng = null;
    this.zoom = null;
    this.location = null;
  }

  setBasicData(data) {
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
  }

  setMarkerData(data) {
    if (data) {
      this.lat = data.lat;
      this.lng = data.lng;
      this.zoom = data.zoom;
    }
  }

  setLocationData(location) {
    this.location = location;
  }
}
