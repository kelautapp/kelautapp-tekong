export class CreateBoatFishingType {
  constructor(data) {
    this.fishingType = data.fishingType;
    this.boatId = null;
  }

  setBoatId(boatId) {
    this.boatId = boatId;
  }
}

export class UpdateBoatFishingType {
  constructor(data) {
    this.fishingType = data.fishingType;
    this.id = data.id;
  }
}
