export class CreateBoatFacility {
  constructor(data) {
    this.facilityName = data.facilityName;
    this.boatId = null;
  }

  setBoatId(boatId) {
    this.boatId = boatId;
  }
}

export class UpdateBoatFacility {
  constructor(data) {
    this.facilityName = data.facilityName;
    this.id = data.id;
  }
}
