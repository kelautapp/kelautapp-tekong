import React, { useContext } from "react";
// import { Bar } from "react-chartjs-2";
import { useTranslation } from "react-i18next";
import KelautCard from "../components/kelaut-card";
import DashboardWeather from "../components/dashboard-weather";
import DashboardTodayBooking from "../components/dashboard-today-booking";
import UpcomingBooking from "../components/upcoming-booking";
import { SetupContext } from "../hooks";
import BaseVariantComp from "../kelaut/animations/framer-variants/components/base-variant-comp";
import {
  dashboarBoxVariants,
  dashboardPageVariants,
} from "../kelaut/animations/framer-variants/dashboard-page";
import { useHistory } from "react-router-dom";

export const Dashboard = () => {
  const history = useHistory();
  const [t] = useTranslation();
  const { setupState } = useContext(SetupContext);

  function getShortName(name) {
    let shortName = name.substr(0, name.indexOf(" "));
    return shortName;
  }

  function goToWeatherDetailPage() {
    history.push("/dashboard/weather");
  }

  return (
    <BaseVariantComp variants={dashboardPageVariants}>
      <div className="row">
        <div className="col-sm-12">
          {setupState && setupState.user && (
            <BaseVariantComp variants={dashboarBoxVariants}>
              <p className="greeting-text">
                {t("Greetings")}, {getShortName(setupState.user.name)}
              </p>
            </BaseVariantComp>
          )}
        </div>
      </div>
      <div className="row">
        <div className="col-sm-12">
          <BaseVariantComp variants={dashboarBoxVariants}>
            <KelautCard
              title={t("TitleTodayWeather")}
              onClick={goToWeatherDetailPage}
            >
              <DashboardWeather />
            </KelautCard>
          </BaseVariantComp>
        </div>
        <div className="col-sm-12">
          <BaseVariantComp variants={dashboarBoxVariants}>
            <KelautCard title={t("TitleTodayBooking")} enableOptions="true">
              <DashboardTodayBooking />
            </KelautCard>
          </BaseVariantComp>
        </div>
        <div className="col-sm-12">
          <BaseVariantComp variants={dashboarBoxVariants}>
            <KelautCard
              title={t("TitleUpcomingBooking")}
              enableOptions="true"
              bottomMenu={t("UpcomingBookingBtmMenu")}
            >
              <UpcomingBooking />
            </KelautCard>
          </BaseVariantComp>
        </div>
        {/* <div className="col-md-12 col-sm-12">
          <BaseVariantComp variants={dashboarBoxVariants}>
            <div className="row">
              <div className="col-md-12 grid-margin stretch-card">
                <div className="card">
                  <div className="card-body">
                    <div className="d-flex flex-row justify-content-between">
                      <h4 className="card-title mb-3">Monthly Booking</h4>
                      <p className="text-muted mb-1">
                        {" "}
                        <i className="mdi mdi-dots-horizontal"></i>
                      </p>
                    </div>
                    <div className="row">
                      <div className="col-md-12">
                        <Bar data={data} options={options} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </BaseVariantComp>
        </div> */}
      </div>
    </BaseVariantComp>
  );
};

export default Dashboard;
