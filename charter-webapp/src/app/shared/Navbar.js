import React, { useEffect, useState, useContext } from "react";
import { Dropdown } from "react-bootstrap";
import { useLocation, useHistory } from "react-router-dom";
import { Trans } from "react-i18next";
import { Auth } from "aws-amplify";
import { SetupContext } from "../hooks";
import { useTranslation } from "react-i18next";

const pathBarTitle = {
  "/dashboard": "Dashboard",
  "/dashboard/icons": "Icons",
  "/dashboard/bookings": "Bookings",
  "/dashboard/boats": "Boats",
  "/dashboard/boats/boatCreation": "Create Boat",
  "/dashboard/boats/manageBoat": "Manage Boat",
  "/dashboard/boats/boat-detail": "Boat Detail",
};

const Navbar = () => {
  const { dispatch, setupState } = useContext(SetupContext);
  // const [mylang, setMyLang] = useState(true);
  const { t, i18n } = useTranslation();

  const changeLanguageHandler = () => {
    i18n.changeLanguage(i18n.language === "my" ? "en" : "my");
  };

  const location = useLocation();
  const [, setTitle] = useState(null);
  let history = useHistory();

  function getAvatarName(name) {
    if (name) {
      let temp = name.split(" ");
      let newName = temp.reduce((name, item, i) => {
        if (i < 2) {
          return (name += item.charAt(0));
        }
        return name;
      }, "");
      return newName;
    }
    return "N/A";
  }

  useEffect(() => {
    let path = location.pathname;
    let title = pathBarTitle[path];
    if (title) setTitle(title);
    else {
      let tempPath = path.substring(0, path.lastIndexOf("/"));
      setTitle(pathBarTitle[tempPath]);
    }
  }, [location]);

  const toggleOffcanvas = () => {
    document.querySelector(".sidebar-offcanvas").classList.toggle("active");
    document.querySelector("#sidebar-bg").classList.toggle("active");
  };

  const onLogout = async () => {
    await Auth.signOut();
    dispatch({ type: "ADD_SETUP", payload: null });
    history.push("/signin");
  };

  const toProfile = () => {
    history.push("/dashboard/profile");
  };
  return (
    <nav className="navbar p-0 fixed-top d-flex flex-row">
      <div className="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
        {/* <Link className="navbar-brand brand-logo-mini" to="/">
          <img src={require("../../assets/images/logo-mini.svg")} alt="logo" />
        </Link> */}
        <button
          className="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
          type="button"
          onClick={toggleOffcanvas}
        >
          <span className="mdi mdi-menu"></span>
        </button>
      </div>
      <div className="navbar-menu-wrapper d-flex align-items-stretch">
        {/* <button
            className="navbar-toggler align-self-center"
            type="button"
            onClick={() => document.body.classList.toggle("sidebar-icon-only")}
          >
            <span className="mdi mdi-menu"></span>
          </button> */}
        <ul className="navbar-nav w-100">
          <li className="d-none d-lg-block nav-item w-100">
            <button
              className="navbar-toggler navbar-toggler-right align-self-center"
              type="button"
              onClick={toggleOffcanvas}
            >
              <span className="mdi mdi-menu"></span>
            </button>
          </li>
        </ul>
        <ul className="navbar-nav navbar-nav-right">
          <Dropdown alignRight as="li" className="nav-item d-none d-lg-block">
            {/* <Dropdown.Toggle className="nav-link btn btn-success create-new-button no-caret">
                + <Trans>Create New Project</Trans>
              </Dropdown.Toggle> */}

            <Dropdown.Menu className="navbar-dropdown preview-list create-new-dropdown-menu">
              <h6 className="p-3 mb-0">
                <Trans>Projects</Trans>
              </h6>
              <Dropdown.Divider />
              <Dropdown.Item
                href="!#"
                onClick={(evt) => evt.preventDefault()}
                className="preview-item"
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <i className="mdi mdi-file-outline text-primary"></i>
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject ellipsis mb-1">
                    <Trans>Software Development</Trans>
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                href="!#"
                onClick={(evt) => evt.preventDefault()}
                className="preview-item"
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <i className="mdi mdi-web text-info"></i>
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject ellipsis mb-1">
                    <Trans>UI Development</Trans>
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                href="!#"
                onClick={(evt) => evt.preventDefault()}
                className="preview-item"
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <i className="mdi mdi-layers text-danger"></i>
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject ellipsis mb-1">
                    <Trans>Software Testing</Trans>
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <p className="p-3 mb-0 text-center">
                <Trans>See all projects</Trans>
              </p>
            </Dropdown.Menu>
          </Dropdown>
          {/* <Dropdown alignRight as="li" className="nav-item">
            <Dropdown.Toggle
              as="a"
              className="nav-link count-indicator cursor-pointer circle-icon"
            >
              <i className="mdi mdi-email"></i>
              <span className="count bg-success"></span>
            </Dropdown.Toggle>
            <Dropdown.Menu className="navbar-dropdown preview-list">
              <h6 className="p-3 mb-0">
                <Trans>Messages</Trans>
              </h6>
              <Dropdown.Divider />
              <Dropdown.Item
                href="!#"
                onClick={(evt) => evt.preventDefault()}
                className="preview-item"
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <img
                      src={require("../../assets/images/faces/face4.jpg")}
                      alt="profile"
                      className="rounded-circle profile-pic"
                    />
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject ellipsis mb-1">
                    <Trans>Mark send you a message</Trans>
                  </p>
                  <p className="text-muted mb-0">
                    {" "}
                    1 <Trans>Minutes ago</Trans>{" "}
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                href="!#"
                onClick={(evt) => evt.preventDefault()}
                className="preview-item"
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <img
                      src={require("../../assets/images/faces/face2.jpg")}
                      alt="profile"
                      className="rounded-circle profile-pic"
                    />
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject ellipsis mb-1">
                    <Trans>Cregh send you a message</Trans>
                  </p>
                  <p className="text-muted mb-0">
                    {" "}
                    15 <Trans>Minutes ago</Trans>{" "}
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                href="!#"
                onClick={(evt) => evt.preventDefault()}
                className="preview-item"
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <img
                      src={require("../../assets/images/faces/face3.jpg")}
                      alt="profile"
                      className="rounded-circle profile-pic"
                    />
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject ellipsis mb-1">
                    <Trans>Profile picture updated</Trans>
                  </p>
                  <p className="text-muted mb-0">
                    {" "}
                    18 <Trans>Minutes ago</Trans>{" "}
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <p className="p-3 mb-0 text-center">
                4 <Trans>new messages</Trans>
              </p>
            </Dropdown.Menu>
          </Dropdown> */}
          {/* <Dropdown alignRight as="li" className="nav-item">
            <Dropdown.Toggle
              as="a"
              className="nav-link count-indicator cursor-pointer circle-icon"
            >
              <i className="mdi mdi-bell"></i>
              <span className="count bg-danger"></span>
            </Dropdown.Toggle>
            <Dropdown.Menu className="dropdown-menu navbar-dropdown preview-list">
              <h6 className="p-3 mb-0">
                <Trans>Notifications</Trans>
              </h6>
              <Dropdown.Divider />
              <Dropdown.Item
                className="dropdown-item preview-item"
                onClick={(evt) => evt.preventDefault()}
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <i className="mdi mdi-calendar text-success"></i>
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject mb-1">
                    <Trans>Event today</Trans>
                  </p>
                  <p className="text-muted ellipsis mb-0">
                    <Trans>Just a reminder that you have an event today</Trans>
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                className="dropdown-item preview-item"
                onClick={(evt) => evt.preventDefault()}
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <i className="mdi mdi-settings text-danger"></i>
                  </div>
                </div>
                <div className="preview-item-content">
                  <h6 className="preview-subject mb-1">
                    <Trans>Settings</Trans>
                  </h6>
                  <p className="text-muted ellipsis mb-0">
                    <Trans>Update dashboard</Trans>
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                className="dropdown-item preview-item"
                onClick={(evt) => evt.preventDefault()}
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <i className="mdi mdi-link-variant text-warning"></i>
                  </div>
                </div>
                <div className="preview-item-content">
                  <h6 className="preview-subject mb-1">
                    <Trans>Launch Admin</Trans>
                  </h6>
                  <p className="text-muted ellipsis mb-0">
                    <Trans>New admin wow</Trans>!
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <p className="p-3 mb-0 text-center">
                <Trans>See all notifications</Trans>
              </p>
            </Dropdown.Menu>
          </Dropdown> */}
          <Dropdown alignRight as="li" className="nav-item">
            <Dropdown.Toggle
              as="a"
              className="nav-link cursor-pointer no-caret"
            >
              <div className="navbar-profile">
                {setupState.user.profileDetails.photoUrl ? (
                  <img
                    className="img-xs rounded-circle"
                    src={setupState.user.profileDetails.photoUrl}
                    alt="profile"
                  />
                ) : (
                  <div className="avatar-sm">
                    {getAvatarName(setupState.user.profileDetails.name)}
                  </div>
                )}

                {/* <p className="mb-0 d-none d-sm-block navbar-profile-name">
                    <Trans>Henry Klein</Trans>
                  </p> */}
                <i className="mdi mdi-dots-vertical d-none d-sm-block"></i>
              </div>
            </Dropdown.Toggle>

            <Dropdown.Menu className="navbar-dropdown preview-list navbar-profile-dropdown-menu">
              <h6 className="p-3 mb-0">
                <Trans>{t("NavProfil")}</Trans>
              </h6>
              <Dropdown.Divider />
              <Dropdown.Item
                onClick={() => toProfile()}
                className="preview-item"
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <i className="mdi mdi-settings text-success"></i>
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject mb-1">
                    <Trans>{t("NavProfil")}</Trans>
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                onClick={() => onLogout()}
                className="preview-item"
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <i className="mdi mdi-logout text-danger"></i>
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject mb-1">
                    <Trans>{t("NavLogout")}</Trans>
                  </p>
                </div>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item
                onClick={() => changeLanguageHandler()}
                className="preview-item"
              >
                <div className="preview-thumbnail">
                  <div className="preview-icon bg-dark rounded-circle">
                    <i className="mdi mdi-translate text-warning"></i>
                  </div>
                </div>
                <div className="preview-item-content">
                  <p className="preview-subject mb-1">
                    <Trans>{t("LanguageSwitch")}</Trans>
                  </p>
                </div>
              </Dropdown.Item>
              {/* <Dropdown.Divider />
              <p className="p-3 mb-0 text-center">
                <Trans>Tukar Bahasa</Trans>
              </p> */}
            </Dropdown.Menu>
          </Dropdown>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
