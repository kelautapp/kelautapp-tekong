import React, { useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import { Trans } from "react-i18next";
import { useTranslation } from "react-i18next";

function Sidebar(props) {
  const [t] = useTranslation();

  function onRouteChanged() {
    document.querySelector("#sidebar").classList.remove("active");
    document.querySelector("#sidebar-bg").classList.remove("active");
  }

  function isPathActive(path) {
    return props.location.pathname === path;
  }

  function onCloseInBg() {
    document.querySelector("#sidebar-bg").classList.toggle("active");
    document.querySelector(".sidebar-offcanvas").classList.toggle("active");
  }

  useEffect(() => {
    const body = document.querySelector("body");
    document.querySelectorAll(".sidebar .nav-item").forEach((el) => {
      el.addEventListener("mouseover", function () {
        if (body.classList.contains("sidebar-icon-only")) {
          el.classList.add("hover-open");
        }
      });
      el.addEventListener("mouseout", function () {
        if (body.classList.contains("sidebar-icon-only")) {
          el.classList.remove("hover-open");
        }
      });
    });
  }, []);

  useEffect(() => {
    if (props.location) {
      onRouteChanged();
    }
  }, [props.location]);

  return (
    <div>
      <div className="sidebar-bg" id="sidebar-bg" onClick={onCloseInBg}></div>
      <nav className="sidebar sidebar-offcanvas" id="sidebar">
        <ul className="nav">
          <li className="nav-link">
            <div
              className="float-right"
              onClick={onCloseInBg}
              style={{ cursor: "pointer" }}
            >
              <i
                className="mdi mdi-close"
                style={{ color: "#fff", fontSize: "16px" }}
              ></i>
            </div>
          </li>
          <Link className="nav-link" to="/dashboard">
            <p className="app-title">
              <span className="main">Tekong</span> App
            </p>
          </Link>
          <li
            className={
              isPathActive("/dashboard")
                ? "nav-item menu-items active"
                : "nav-item menu-items"
            }
          >
            <Link className="nav-link" to="/dashboard">
              <span className="menu-title">
                <Trans>{t("SidebarDashboard")}</Trans>
              </span>
            </Link>
          </li>
          <li
            className={
              isPathActive("/dashboard/weather/weathermap")
                ? "nav-item menu-items active"
                : "nav-item menu-items"
            }
          >
            <Link className="nav-link" to="/dashboard/weather/weathermap">
              <span className="menu-title">
                <Trans>{t("SidebarWeatherMap")}</Trans>
              </span>
            </Link>
          </li>
          <li
            className={
              isPathActive("/dashboard/boats")
                ? "nav-item menu-items active"
                : "nav-item menu-items"
            }
          >
            <Link className="nav-link" to="/dashboard/boats">
              <span className="menu-title">
                <Trans>{t("SidebarBoatManagement")}</Trans>
              </span>
            </Link>
          </li>

          <li
            className={
              isPathActive("/dashboard/crew")
                ? "nav-item menu-items active"
                : "nav-item menu-items"
            }
          >
            <Link className="nav-link" to="/dashboard/crew">
              <span className="menu-title">
                <Trans>{t("SidebarCrewBoatManagement")}</Trans>
              </span>
            </Link>
          </li>
          <li
            className={
              isPathActive("/dashboard/bookings")
                ? "nav-item menu-items active"
                : "nav-item menu-items"
            }
          >
            <Link className="nav-link" to="/dashboard/bookings">
              <span className="menu-title">
                <Trans>{t("SidebarBookings")}</Trans>
              </span>
            </Link>
          </li>
          <li
            className={
              isPathActive("/dashboard/profile")
                ? "nav-item menu-items active"
                : "nav-item menu-items"
            }
          >
            <Link className="nav-link" to="/dashboard/profile">
              <span className="menu-title">
                <Trans>{t("SidebarProfileManagement")}</Trans>
              </span>
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default withRouter(Sidebar);
