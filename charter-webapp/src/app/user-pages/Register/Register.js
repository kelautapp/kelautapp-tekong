import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { InputField } from "../../components/forms-ui/Input";
import { Formik, Form } from "formik";
import { validate } from "./index";
import { LoaderContext } from "../../hooks";
import useSignUp from "../../hooks/authentication/signup";
import { toast } from "react-toastify";
import { Alert } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { ConfirmForm } from "../../components/confirmForm";
import AuthContainer from "../../kelaut/animations/framer-variants/components/auth-container";
import { useTranslation } from "react-i18next";

export function Register() {
  const { setLoading } = useContext(LoaderContext);
  const [initialized, setInitialized] = useState(false);
  const { loading, errors, waitingConfirm, onPreRegistrationProcess } =
    useSignUp();

  const [preRegisterData, setPreRegisterData] = useState({
    password: "",
    email: "",
    phoneNumber: "",
  });

  const [initalValues] = useState({
    password: "",
    email: "",
    phoneNumber: "",
  });

  const history = useHistory();
  const [t] = useTranslation();

  const handleFormSubmit = async (data) => {
    try {
      if (waitingConfirm) {
      } else {
        //set username with email
        data.username = data.email;
        if (!data.phoneNumber.startsWith("+60")) {
          let countrycode = "";
          if (data.phoneNumber[0] === "0") {
            countrycode = "+6";
          } else {
            countrycode = "+60";
          }
          data.phoneNumber = countrycode + data.phoneNumber;
        }
        setPreRegisterData({ ...data });
      }
    } catch (error) {
      console.log(error);
    }
  };

  // const setResendInterval = () =>

  const onCompleteSignup = () => {
    history.push("/signin");
  };

  useEffect(() => {
    const preRegister = async () => {
      setLoading(true);
      try {
        await onPreRegistrationProcess({ ...preRegisterData });
      } catch (error) {
        console.log(error, "error pre register");
      }
      setLoading(false);
    };
    if (initialized) preRegister();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [preRegisterData]);

  useEffect(() => {
    setInitialized(true);
  }, []);

  useEffect(() => {
    if (loading !== null) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (errors !== null) {
      toast(errors, { type: "error" });
    }
  }, [errors]);

  return (
    <div className="login-bg">
      <AuthContainer>
        <div className="row">
          <div className="col-lg-12">
            <div className="row">
              <div className="col-md-12" style={{ padding: 0 }}>
                <div className="left-side-container">
                  {/* <div className="brand-logo">
                      <img
                        src={require("../../assets/images/logo.svg")}
                        alt="logo"
                      />
                    </div> */}
                  <div className="mb-3">
                    <h4 className="thumbnail-title">{t("SignUpTitle")}</h4>
                    <p className="thumbnail-subtitle">{t("SignUpSubTitle")}</p>
                  </div>
                  {waitingConfirm === false ? (
                    <Formik
                      initialValues={initalValues}
                      enableReinitialize={true}
                      validationSchema={validate}
                      onSubmit={async (values) => {
                        await handleFormSubmit(values);
                      }}
                    >
                      <div>
                        <Form>
                          {/* <div> */}
                          <div className="text-left py-5 px-4 px-sm-5 form-place">
                            <>
                              <InputField
                                label={t("FormEmailLabel")}
                                type="email"
                                id="email"
                                name="email"
                                placeholder={t("FormEmailPlaceholder")}
                              />
                              <InputField
                                label={t("FormPasswordLabel")}
                                type="password"
                                id="password"
                                name="password"
                                placeholder={t("FormPasswordPlaceholder")}
                              />
                              <InputField
                                label={t("FormPhoneLabel")}
                                type="text"
                                id="phone-number"
                                name="phoneNumber"
                                placeholder={t("FormPhonePlaceholder")}
                              />
                            </>
                          </div>
                          <div className="text-center mt-2 font-weight-light">
                            <div className="text-left py-5 px-4 px-sm-5 form-place mb-4">
                              <button
                                type="submit"
                                className="btn btn-block btn-lg font-weight-medium auth-form-btn kelaut-primary-btn"
                              >
                                {t("SubmitButton")}
                              </button>
                              <p className="mt-3">
                                {t("PrivacyPolicyText")}
                                {/* <Link to="https://www.google.com">
                                  Terms of Use
                                </Link>{" "}
                                and{" "} */}
                                <a
                                  href="https://www.privacypolicies.com/live/9943b23e-6da8-49a9-bd3b-7b329a03f904"
                                  target="_blank"
                                  rel="noreferrer"
                                >
                                  {t("PrivacyPolicyLink")}
                                </a>
                                .
                              </p>
                            </div>
                            <p>
                              {" "}
                              {t("AlreadyHaveAccount")}
                              <Link
                                to="/signin"
                                className="ml-1 text-primary link"
                              >
                                {t("LoginLink")}
                              </Link>
                            </p>
                          </div>
                          {errors && <Alert variant="danger">{errors}</Alert>}
                        </Form>
                      </div>
                    </Formik>
                  ) : (
                    <ConfirmForm
                      username={preRegisterData.username}
                      onCompleteSignup={onCompleteSignup}
                    />
                  )}
                </div>
              </div>
            </div>
            {/* <div className="card text-left py-5 px-4 px-sm-5"> */}

            {/* </div> */}
          </div>
        </div>
      </AuthContainer>
    </div>
  );
}

export default Register;
