import * as Yup from "yup";
import i18n from "../../../i18n";
// const phoneRegExp =
// /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const phoneRegExp = /^(\+?6?01)[0-46-9]-*[0-9]{7,8}$/;
export const validate = Yup.object({
  //username: Yup.string().required("Username is required"),
  email: Yup.string()
    .email(i18n.t("FormEmailInvalid"))
    .required(i18n.t("FormEmailRequired")),
  password: Yup.string()
    .min(6, i18n.t("FormPasswordMinimum"))
    .required(i18n.t("FormPasswordRequired")),
  //   confirmPassword: Yup.string()
  //     .oneOf([Yup.ref("password"), null], "Password must match")
  //     .required("Confirm password is required"),
  phoneNumber: Yup.string()
    .required(i18n.t("FormPhoneRequired"))
    .matches(phoneRegExp, i18n.t("FormPhoneValidation")),
});
