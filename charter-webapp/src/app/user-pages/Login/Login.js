/* eslint-disable eqeqeq */
import React, { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { InputFieldWithSymbol } from "../../components/forms-ui/InputWithSymbol";
import { Formik, Form } from "formik";
import { validate } from "./index";
import { LoaderContext } from "../../hooks";
import useSign from "../../hooks/authentication/sigin";
import { Alert } from "react-bootstrap";
import { toast } from "react-toastify";
import { ConfirmForm } from "../../components/confirmForm";
import AuthContainer from "../../kelaut/animations/framer-variants/components/auth-container";
import { useTranslation } from "react-i18next";

export function Login() {
  const { loading, errors, onSignIn, confirm, userData, setConfirm } =
    useSign();
  const { setLoading } = useContext(LoaderContext);
  const [t] = useTranslation();

  const onCompleteSignup = () => {
    setConfirm(false);
  };

  useEffect(() => {
    if (loading !== null) {
      setLoading(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (errors) {
      toast(errors, { type: "error" });
    }
  }, [errors]);

  return (
    <div className="login-bg">
      <AuthContainer>
        <div className="row">
          <div className="col-lg-12">
            <div className="row">
              <div
                className="col-md-12"
                style={{ padding: 0, overflow: "auto" }}
              >
                <div className="right-side-container">
                  <div className="thumbnail-container">
                    <p className="thumbnail-title">{t("Welcome")}</p>
                    <p className="thumbnail-subtitle">{t("SignInToAccount")}</p>
                  </div>
                  <div className="text-left py-5 px-4 px-sm-5 form-place">
                    {confirm == false ? (
                      <Formik
                        initialValues={{
                          username: "",
                          password: "",
                        }}
                        validationSchema={validate}
                        onSubmit={async (values) => {
                          await onSignIn(values);
                        }}
                      >
                        <Form>
                          {/* <p className="error">{errors}</p> */}
                          <InputFieldWithSymbol
                            label={t("Username")}
                            type="email"
                            placeholder={t("UsernamePlaceholder")}
                            name="username"
                            size="lg"
                            className="h-auto"
                            icon="mdi mdi-account-outline"
                          />

                          <InputFieldWithSymbol
                            label={t("Password")}
                            type="password"
                            placeholder={t("PasswordPlaceholder")}
                            name="password"
                            size="lg"
                            className="h-auto"
                            icon="mdi mdi-lock-outline"
                          />
                          {errors && <Alert variant="danger">{errors}</Alert>}
                          <div className="my-2 d-flex justify-content-between align-items-center forgot-password">
                            <div className="form-check"></div>
                            <Link
                              // href="!#"
                              to="/reset"
                              className="auth-link text-muted link"
                            >
                              {t("ForgotPassword")}
                            </Link>
                          </div>
                          <div>
                            <button
                              type="submit"
                              className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn kelaut-primary-btn"
                            >
                              {t("Login")}
                            </button>
                          </div>
                        </Form>
                      </Formik>
                    ) : (
                      <ConfirmForm
                        username={userData ? userData.username : null}
                        onCompleteSignup={onCompleteSignup}
                      />
                    )}
                  </div>
                  <div className="text-center mt-4 font-weight-light sign-up-navigator">
                    {t("SignUp")}
                    <Link to="/signup" className="ml-1 text-primary link">
                      {t("SignUpNow")}
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </AuthContainer>
    </div>
  );
}

export default Login;
