import * as Yup from "yup";
import i18n from "../../../i18n";

export const validate = Yup.object({
  username: Yup.string()
    .email(i18n.t("InvalidEmail"))
    .required(i18n.t("RequireEmail")),
  password: Yup.string().required(i18n.t("RequirePassword")),
});
