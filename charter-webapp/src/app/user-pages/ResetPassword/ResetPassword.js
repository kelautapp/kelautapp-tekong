import React, { useEffect, useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { InputFieldWithSymbol } from "../../components/forms-ui/InputWithSymbol";
import { Formik, Form } from "formik";
import { validate } from "./index";
import { LoaderContext, useForgotPassword } from "../../hooks";
import { Alert } from "react-bootstrap";
import { toast } from "react-toastify";
import { validateConfirm } from "./validation";
import AuthContainer from "../../kelaut/animations/framer-variants/components/auth-container";
import { useTranslation } from "react-i18next";

export function ResetPassword() {
  const [t] = useTranslation();
  const history = useHistory();
  const { setLoading } = useContext(LoaderContext);
  const {
    loading,
    error,
    completeForgotPassword,
    completeSendCode,
    onSendPasswordCode,
    onConfirmPassword,
  } = useForgotPassword();
  const [selectedValidation] = useState(false);

  const handleSubmit = async (values) => {
    if (completeSendCode) {
      onConfirmPassword(values);
    } else {
      onSendPasswordCode(values.email);
    }
  };
  useEffect(() => {
    if (loading !== null) {
      setLoading(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (completeSendCode) {
      toast(t("CompleteSendCode"), { type: "success" });
    }
  }, [completeSendCode, t]);

  useEffect(() => {
    if (completeForgotPassword) {
      toast(t("CompleteForgotPassword"), { type: "success" });
      history.push("/signin");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeForgotPassword, t]);

  useEffect(() => {
    if (error) {
      toast(error, { type: "error" });
      // history.push("/signin");
      history.push("/signin");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error]);

  return (
    <div className="login-bg">
      <AuthContainer>
        <div className="row">
          <div className="col-lg-12">
            <div className="row">
              <div className="col-md-12" style={{ padding: 0 }}>
                <div className="right-side-container">
                  <div className="thumbnail-container">
                    <p className="thumbnail-title">{t("ResetPasswordTitle")}</p>
                    <p className="thumbnail-subtitle">
                      {t("ResetPasswordSubTitle")}
                    </p>
                  </div>
                  <div className="text-left py-5 px-4 px-sm-5 form-place">
                    <Formik
                      initialValues={{
                        email: "",
                        code: "",
                        password: "",
                      }}
                      validationSchema={
                        selectedValidation ? validateConfirm : validate
                      }
                      onSubmit={async (values) => {
                        await handleSubmit(values);
                        // changeSelectedValidation(true);
                      }}
                    >
                      <Form className="pt-3">
                        {/* <p className="error">{errors}</p> */}
                        <InputFieldWithSymbol
                          label={t("FormEmailLabel")}
                          type="email"
                          placeholder={t("FormEmailPlaceholder")}
                          name="email"
                          size="lg"
                          className="h-auto"
                          icon="mdi mdi-face-profile"
                        />
                        {completeSendCode && (
                          <>
                            <InputFieldWithSymbol
                              label={t("VerificationCode")}
                              type="text"
                              placeholder={t("VerificationCodePlaceholder")}
                              name="code"
                              size="lg"
                              className="h-auto"
                              icon="mdi mdi-lock"
                            />
                            <InputFieldWithSymbol
                              label={t("FormPasswordLabel")}
                              type="password"
                              placeholder={t("FormPasswordPlaceholder")}
                              name="password"
                              size="lg"
                              className="h-auto"
                              icon="mdi mdi-lock"
                            />
                          </>
                        )}

                        {error && <Alert variant="danger">{error}</Alert>}

                        <div className="mt-3">
                          <button
                            type="submit"
                            className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn kelaut-primary-btn"
                          >
                            {completeSendCode
                              ? t("ResetPasswordButton")
                              : t("SendCodeButton")}
                          </button>
                        </div>
                        <div className="text-center mt-4 font-weight-light sign-up-navigator">
                          {t("SignUp")}
                          <Link to="/signup" className="ml-1 text-primary link">
                            {t("SignUpNow")}
                          </Link>
                        </div>
                        <div className="text-center mt-4 font-weight-light sign-up-navigator">
                          <p>
                            {" "}
                            {t("AlreadyHaveAccount")}
                            <Link
                              to="/signin"
                              className="ml-1 text-primary link"
                            >
                              {t("LoginLink")}
                            </Link>
                          </p>
                        </div>
                      </Form>
                    </Formik>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </AuthContainer>
    </div>
  );
}

export default ResetPassword;
