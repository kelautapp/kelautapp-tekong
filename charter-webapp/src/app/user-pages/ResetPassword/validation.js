import * as Yup from "yup";
import i18n from "../../../i18n";

export const validate = Yup.object({
  email: Yup.string()
    .email(i18n.t("FormEmailInvalid"))
    .required(i18n.t("FormEmailRequired")),
});

export const validateConfirm = Yup.object({
  email: Yup.string()
    .email(i18n.t("FormEmailInvalid"))
    .required(i18n.t("FormEmailRequired")),
  code: Yup.string().required(i18n.t("VerificationCodeRequire")),
  password: Yup.string().required(i18n.t("FormPasswordRequired")),
});
