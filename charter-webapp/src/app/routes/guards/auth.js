import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import { SetupContext } from "../../hooks";

export const ProtectedRoute = ({ component, ...rest }) => {
  const { setupState } = useContext(SetupContext);
  return setupState.load == null ? (
    <div>Checking</div>
  ) : setupState.user != null ? (
    <Route {...rest} component={component} />
  ) : (
    <Redirect to="/signin" />
  );
};
