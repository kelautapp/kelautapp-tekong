import React, { Component, Suspense, lazy } from "react";
import { Switch, Route } from "react-router-dom";

import Spinner from "../app/shared/Spinner";
import { BoatCreationPage } from "./kelaut/pages/boats/boat-creation";
import { ManageBoat } from "./kelaut/pages/boats/manage-boat";
import ProfileRoutes from "./kelaut/pages/profile";
const ChangePasswordPage = lazy(() =>
  import("./kelaut/pages/profile/change-password")
);

const Dashboard = lazy(() => import("./dashboard/Dashboard"));
// const Dashboard = lazy(() => import("./kelaut/pages/dashboard"));

const Buttons = lazy(() => import("./basic-ui/Buttons"));
const Dropdowns = lazy(() => import("./basic-ui/Dropdowns"));
const Typography = lazy(() => import("./basic-ui/Typography"));

const BasicElements = lazy(() => import("./form-elements/BasicElements"));

const BasicTable = lazy(() => import("./tables/BasicTable"));

const Mdi = lazy(() => import("./icons/Mdi"));

const ChartJs = lazy(() => import("./charts/ChartJs"));

const Error404 = lazy(() => import("./error-pages/Error404"));
const Error500 = lazy(() => import("./error-pages/Error500"));

const Register1 = lazy(() => import("./user-pages/Register"));
const Boats = lazy(() => import("./kelaut/pages/boats"));
const BoatDetail = lazy(() => import("./kelaut/pages/boats/boat-detail"));

const Crew = lazy(() => import("./kelaut/pages/crew"));

const Bookings = lazy(() => import("./kelaut/pages/bookings"));
const BookingDetail = lazy(() =>
  import("./kelaut/pages/bookings/booking-detail")
);

const WeatherDetail = lazy(() =>
  import("./kelaut/pages/weather/weather-detail")
);

const WeatherMap = lazy(() => import("./kelaut/pages/weather/weather-map"));
const ProfileDetail = lazy(() => import("./kelaut/pages/profile/detail"));

class AppRoutes extends Component {
  render() {
    return (
      <Suspense fallback={<Spinner />}>
        <Switch>
          <Route exact path="/dashboard" component={Dashboard} />
          {/* weather page */}
          <Route path="/dashboard/weather/weathermap" component={WeatherMap} />
          <Route path="/dashboard/weather" component={WeatherDetail} />

          {/* end weather page */}

          {/* bookings */}
          <Route
            path="/dashboard/bookings/booking-detail/:id"
            component={BookingDetail}
          />

          <Route path="/dashboard/bookings" component={Bookings} />
          <Route exact path="/dashboard/boats" component={Boats} />
          <Route
            path="/dashboard/boats/boatCreation"
            component={BoatCreationPage}
          />
          <Route
            path="/dashboard/boats/boat-detail/:id"
            component={BoatDetail}
          />
          <Route
            path="/dashboard/boats/manageBoat/:id"
            component={ManageBoat}
          />
          {/* <Route exact path="/dashboard/boats" component={Boats} /> */}
          <Route path="/basic-ui/buttons" component={Buttons} />
          <Route
            path="/dashboard/profile/verifyProfile"
            component={ProfileRoutes}
          />
          <Route
            path="/dashboard/profile/changePassword"
            component={ChangePasswordPage}
          />
          <Route path="/dashboard/profile" component={ProfileDetail} />
          <Route path="/dashboard/crew" component={Crew} />

          <Route path="/basic-ui/dropdowns" component={Dropdowns} />
          <Route path="/basic-ui/typography" component={Typography} />

          <Route
            path="/form-Elements/basic-elements"
            component={BasicElements}
          />

          <Route path="/tables/basic-table" component={BasicTable} />

          <Route path="/dashboard/icons" component={Mdi} />

          <Route path="/charts/chart-js" component={ChartJs} />

          {/* <Route path="/signin" component={Login} /> */}
          <Route path="/user-pages/register-1" component={Register1} />

          <Route path="/error-pages/error-404" component={Error404} />
          <Route path="/error-pages/error-500" component={Error500} />
        </Switch>
      </Suspense>
    );
  }
}

export default AppRoutes;
