import React from "react";
import { motion } from "framer-motion";
import { authVariants } from "../../auth-page";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ children }) {
  return (
    <motion.div
      className="container login-container"
      variants={authVariants}
      initial="initial"
      animate="animate"
      exit="exit"
    >
      {children}
    </motion.div>
  );
}
