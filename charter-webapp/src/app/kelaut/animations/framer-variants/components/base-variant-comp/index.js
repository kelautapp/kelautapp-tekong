import React from "react";
import { motion } from "framer-motion";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ children, variants, ...otherProps }) {
  return (
    <motion.div
      variants={variants}
      initial="initial"
      animate="animate"
      exit="exit"
      {...otherProps}
    >
      {children}
    </motion.div>
  );
}
