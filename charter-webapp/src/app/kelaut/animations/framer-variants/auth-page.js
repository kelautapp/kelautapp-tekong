export const authVariants = {
  initial: {
    y: "100vh",
    transition: {
      staggerChildren: 1,
    },
  },
  animate: {
    y: 0,
    transition: {
      type: "spring",
      delay: 1,
      duration: 2,
      mass: 0.4,
      damping: 8,
      when: "beforeChildren",
      staggerChildren: 1,
    },
  },
  exit: {
    y: "100vh",
    transition: {
      mass: 0.4,
      damping: 8,
    },
  },
};
