export const generalPageMainContainerVariants = {
  initial: {
    x: "100vw",
    transition: {
      staggerChildren: 1,
    },
  },
  animate: {
    x: 0,
    transition: {
      type: "spring",
      mass: 0.4,
      damping: 8,
      when: "beforeChildren",
      staggerChildren: 1,
    },
  },
  exit: {
    x: "100vw",
    transition: {
      mass: 0.4,
      damping: 8,
    },
  },
};

export const generalCardVariants = {
  initial: {
    scale: 0,
  },
  animate: {
    scale: 1,
    transition: {
      type: "spring",
      delay: 0.5,
      duration: 2,
      mass: 0.4,
      damping: 8,
      when: "beforeChildren",
    },
  },
};
