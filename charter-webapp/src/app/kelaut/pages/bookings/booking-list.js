import React, { useState } from "react";
import * as moment from "moment";
import BookingDate from "../../../components/booking-date";
import Badge from "react-bootstrap/Badge";
import { useHistory } from "react-router-dom";
import _ from "lodash";
import { useTranslation } from "react-i18next";

export const Booking = () => {
  const history = useHistory();

  function goToDetailPage(id) {
    history.push(`/dashboard/bookings/booking-detail/${id}`);
  }

  const [bookings] = useState([]);
  // const [bookings] = useState([
  //   {
  //     id: 1,
  //     bookingNumber: "BNF2002 KL-2",
  //     location: "Mersing",
  //     state: "Johor",
  //     date: "2022-02-10 09:30:00",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 2,
  //     bookingNumber: "BNF2003 KL-2",
  //     location: "Endau",
  //     state: "Pahang",
  //     date: "2022-02-11 10:00:00",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 3,
  //     bookingNumber: "BNF2004 KL-2",
  //     location: "Rompin",
  //     state: "Pahang",
  //     date: "2022-02-12 08:30:00",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 4,
  //     bookingNumber: "BNF2004 KL-2",
  //     location: "Mersing",
  //     state: "Johor",
  //     date: "2022-03-20 08:00:00",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 5,
  //     bookingNumber: "BNF2004 KL-2",
  //     location: "Mersing",
  //     state: "Kedah",
  //     date: "2022-04-15 08:00:00",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 6,
  //     bookingNumber: "BNF2004 KL-2",
  //     location: "Mersing",
  //     state: "Johor",
  //     date: "2022-05-16 10:00:00",
  //     //   image: Boat,
  //   },
  // ]);

  function getTime(date) {
    return moment(date).format("HH:mma");
  }

  function isCritical(date) {
    var today = moment();
    var tripdate = moment(date);
    var different = tripdate.diff(today, "days");
    return different;
  }

  function CriticalBooking({ booking }) {
    return (
      <tr key={booking.id}>
        <td colSpan={3}>
          <div
            className="critical-booking"
            onClick={() => goToDetailPage(booking.id)}
          >
            <BookingDate date={booking.date} />
            <div className="critical-loc">
              <div style={{ marginBottom: 5, fontSize: 14 }}>
                {booking.location}
              </div>
              <div style={{ fontWeight: 400 }}>{booking.bookingNumber}</div>
            </div>
            <div className="critical-time">
              <Badge pill variant="light">
                {getTime(booking.date)}
              </Badge>
            </div>
          </div>
        </td>
      </tr>
    );
  }

  function NormalBooking({ booking }) {
    return (
      <tr key={booking.id}>
        <td colSpan={3}>
          <div
            className="normal-booking"
            onClick={() => goToDetailPage(booking.id)}
          >
            <BookingDate date={booking.date} />
            <div className="normal-loc">
              <div
                className="loc-name"
                style={{ marginBottom: 5, fontSize: 14 }}
              >
                {booking.location}
              </div>
              <div style={{ fontWeight: 400 }}>{booking.bookingNumber}</div>
            </div>
            <div className="normal-time">
              <Badge pill variant="secondary">
                {getTime(booking.date)}
              </Badge>
            </div>
          </div>
        </td>
      </tr>
    );
  }

  const [t] = useTranslation();

  return (
    <div className="table-responsive">
      {!_.isEmpty(bookings) && (
        <table className="table table-booking">
          <thead>
            <tr>
              <th>
                <div className="header">
                  <div>{t("ColumnNameDate")}</div>
                  <div>{t("ColumnNameTrip")}</div>
                  <div>{t("ColumnNameTime")}</div>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            {bookings.map((booking) => {
              if (isCritical(booking.date) < 8)
                return <CriticalBooking key={booking.id} booking={booking} />;
              else return <NormalBooking key={booking.id} booking={booking} />;
            })}
          </tbody>
        </table>
      )}
      {_.isEmpty(bookings) && <p>{t("NoBookingMsg")}</p>}
    </div>
  );
};

export default Booking;
