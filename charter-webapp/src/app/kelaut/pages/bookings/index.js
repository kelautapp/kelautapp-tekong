import React from "react";
import KelautCard from "../../../components/kelaut-card";
import BaseVariantComp from "../../animations/framer-variants/components/base-variant-comp";
import {
  generalPageMainContainerVariants,
  generalCardVariants,
} from "../../animations/framer-variants/general-page";
import KelautPage from "../../../components/kelaut-page";
import BookingList from "./booking-list";
import { useTranslation } from "react-i18next";

export const Bookings = () => {
  const [t] = useTranslation();
  return (
    <KelautPage enableBackHeaderBtn>
      <BaseVariantComp variants={generalPageMainContainerVariants}>
        <div className="row">
          <div className="col-sm-12">
            <KelautCard
              title={t("BookingsCardTitle")}
              variants={generalCardVariants}
              enableOptions={true}
            >
              <BookingList />
            </KelautCard>
          </div>
        </div>
      </BaseVariantComp>
    </KelautPage>
  );
};

export default Bookings;
