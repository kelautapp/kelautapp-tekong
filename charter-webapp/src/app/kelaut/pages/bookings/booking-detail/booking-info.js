import React from "react";
import KelautCard from "../../../../components/kelaut-card";
import * as moment from "moment";

export const BookingInfo = ({ booking }) => {
  function getDateFormat(date) {
    return moment(date).format("DD MMMM yyyy, H:mma");
  }
  function toCommas(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  return (
    <KelautCard title="Booking Information">
      <div className="bd-booking-info">
        <div className="row">
          <p className="label">Anglers</p>
          <p className="value">{booking.anglers}</p>
        </div>
        <div className="row">
          <p className="label">Date & Time</p>
          <p className="value">{getDateFormat(booking.date)}</p>
        </div>
        <div className="row">
          <p className="label">Price</p>
          <p className="value money">RM {toCommas(booking.price)}</p>
        </div>
      </div>
    </KelautCard>
  );
};

export default BookingInfo;
