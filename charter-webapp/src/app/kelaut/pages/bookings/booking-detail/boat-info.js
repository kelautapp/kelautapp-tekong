import React from "react";
import KelautCard from "../../../../components/kelaut-card";
import Boat from "../../../../../assets/images/boats/boat2.jpg";

export const BoatInfo = ({ boat }) => {
  return (
    <KelautCard title="Boat Information">
      <div className="boat-info">
        <img src={Boat} alt="" />
        <div className="boat-info-detail">
          <p className="boat-name">{boat.name}</p>
          <ul className="boat-crews">
            {boat.crews &&
              boat.crews.map((crew, key) => <li key={key}>{crew.name}</li>)}
          </ul>
        </div>
      </div>
    </KelautCard>
  );
};

export default BoatInfo;
