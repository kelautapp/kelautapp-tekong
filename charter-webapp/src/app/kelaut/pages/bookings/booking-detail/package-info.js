import React from "react";
import KelautCard from "../../../../components/kelaut-card";

export const PackageInformation = ({ bpackage }) => {
  return (
    <KelautCard title="Package Information">
      <div className="bd-package-info">
        {bpackage &&
          bpackage.map((pkg, key) => (
            <div className="row" key={key}>
              <p className="label">{pkg.label}</p>
              {pkg.value &&
                pkg.value.map((vlue, key) => (
                  <p key={key} className="value">
                    {vlue}
                  </p>
                ))}
            </div>
          ))}
      </div>
    </KelautCard>
  );
};

export default PackageInformation;
