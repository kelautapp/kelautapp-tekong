import React from "react";
import KelautCard from "../../../../components/kelaut-card";
import CallBtn from "../../../../../assets/images/kelaut/callbtn.svg";

export const CustomerInfo = ({ customer }) => {
  return (
    <KelautCard title="Customer Information">
      {customer && (
        <div className="b-cust-info">
          <div className="cust-detail">
            <div className="row">
              <p className="label">Name</p>
              <p className="value">{customer.name}</p>
            </div>
            <div className="row">
              <p className="label">Phone Number</p>
              <p className="value">{customer.phone}</p>
            </div>
            <div className="row">
              <p className="label">E-mail address</p>
              <p className="value">{customer.email}</p>
            </div>
          </div>
          <div className="call-btn">
            <img className="call-btn" src={CallBtn} alt="" />
          </div>
        </div>
      )}
    </KelautCard>
  );
};

export default CustomerInfo;
