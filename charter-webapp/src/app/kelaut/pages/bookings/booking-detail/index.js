import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import KelautCard from "../../../../components/kelaut-card";
import KelautPage from "../../../../components/kelaut-page";
import PrintImage from "../../../../../assets/images/kelaut/print.svg";
import CustomerInfo from "./customer-info";
import _ from "lodash";
import BoatInfo from "./boat-info";
import BookingInfo from "./booking-info";
import PackageInformation from "./package-info";
// import { m } from "framer-motion";
import Button from "react-bootstrap/Button";

export const BookingDetail = (props) => {
  const [bookings] = useState([
    {
      id: 1,
      bookingNumber: "BNF2002 KL-2",
      location: "Mersing",
      state: "Johor",
      date: "2022-02-10 09:30:00",
      customer: {
        name: "Ilham Bin Ahmad",
        phone: "+60137889220",
        email: "ilham@gmail.com",
      },
      boat: {
        name: "Seroja D'Hati",
        crews: [
          {
            name: "Zarif Bin Leman",
            role: "Driver",
          },
          {
            name: "Danial Arif Bin Othman",
            role: "Crew",
          },
          {
            name: "Roslan Bin Ahmad",
            role: "Crew",
          },
        ],
      },
      booking: {
        anglers: 10,
        date: "2022-02-10 09:30:00",
        price: 1200.0,
        package: [
          {
            label: "Food & Snacks",
            value: [
              "Breakfast for 10pax",
              "Lunch for 10pax",
              "Dinner for 10pax",
            ],
          },
          {
            label: "Baits",
            value: ["100 pax"],
          },
          {
            label: "Ice",
            value: ["5 Bag"],
          },
          {
            label: "Storage box",
            value: ["2 box"],
          },
        ],
      },
      //   image: Boat,
    },
    {
      id: 2,
      bookingNumber: "BNF2003 KL-2",
      location: "Endau",
      state: "Pahang",
      date: "2022-02-11 10:00:00",
      customer: {
        name: "Bukhari Bin Faisal",
        phone: "+60137991222",
        email: "bukhari@gmail.com",
      },
      boat: {
        name: "Harimau Laut",
        crews: [
          {
            name: "Muthu a/l Mariapan",
            role: "Driver",
          },
          {
            name: "Mohd Rais Bin Mohd Saiful",
            role: "Crew",
          },
          {
            name: "Rosli Bin Ali",
            role: "Crew",
          },
        ],
      },
      booking: {
        anglers: 10,
        date: "2022-02-11 10:00:00",
        price: 800.0,
        package: [
          {
            label: "Food & Snacks",
            value: [
              "Breakfast for 10pax",
              "Lunch for 10pax",
              "Dinner for 10pax",
            ],
          },
          {
            label: "Baits",
            value: ["100 pax"],
          },
          {
            label: "Ice",
            value: ["5 Bag"],
          },
          {
            label: "Storage box",
            value: ["2 box"],
          },
        ],
      },
      //   image: Boat,
    },
    {
      id: 3,
      bookingNumber: "BNF2004 KL-2",
      location: "Rompin",
      state: "Pahang",
      date: "2022-02-12 08:30:00",
      customer: {
        name: "Zufar Bin Sazali",
        phone: "+60137999922",
        email: "zufar.sazali@gmail.com",
      },
      boat: {
        name: "Melor",
        crews: [
          {
            name: "Yusri Bin Yusof",
            role: "Driver",
          },
          {
            name: "Rais Atan Bin Haikal",
            role: "Crew",
          },
        ],
      },
      booking: {
        anglers: 10,
        date: "2022-02-12 08:30:00",
        price: 4800.0,
        package: [
          {
            label: "Food & Snacks",
            value: [
              "Breakfast for 10pax",
              "Lunch for 10pax",
              "Dinner for 10pax",
            ],
          },
          {
            label: "Baits",
            value: ["100 pax"],
          },
          {
            label: "Ice",
            value: ["5 Bag"],
          },
          {
            label: "Storage box",
            value: ["2 box"],
          },
        ],
      },
      //   image: Boat,
    },
    {
      id: 4,
      bookingNumber: "BNF2004 KL-2",
      location: "Mersing",
      state: "Johor",
      date: "2022-03-20 08:00:00",
      customer: {
        name: "Zufar Bin Sazali",
        phone: "+60137999922",
        email: "zufar.sazali@gmail.com",
      },
      boat: {
        name: "Melor",
        crews: [
          {
            name: "Yusri Bin Yusof",
            role: "Driver",
          },
          {
            name: "Rais Atan Bin Haikal",
            role: "Crew",
          },
        ],
      },
      booking: {
        anglers: 10,
        date: "2022-02-12 08:30:00",
        price: 4800.0,
        package: [
          {
            label: "Food & Snacks",
            value: [
              "Breakfast for 10pax",
              "Lunch for 10pax",
              "Dinner for 10pax",
            ],
          },
          {
            label: "Baits",
            value: ["100 pax"],
          },
          {
            label: "Ice",
            value: ["5 Bag"],
          },
          {
            label: "Storage box",
            value: ["2 box"],
          },
        ],
      },
      //   image: Boat,
    },
    {
      id: 5,
      bookingNumber: "BNF2004 KL-2",
      location: "Mersing",
      state: "Kedah",
      date: "2022-04-15 08:00:00",
      customer: {
        name: "Zufar Bin Sazali",
        phone: "+60137999922",
        email: "zufar.sazali@gmail.com",
      },
      boat: {
        name: "Melor",
        crews: [
          {
            name: "Yusri Bin Yusof",
            role: "Driver",
          },
          {
            name: "Rais Atan Bin Haikal",
            role: "Crew",
          },
        ],
      },
      booking: {
        anglers: 10,
        date: "2022-02-12 08:30:00",
        price: 4800.0,
        package: [
          {
            label: "Food & Snacks",
            value: [
              "Breakfast for 10pax",
              "Lunch for 10pax",
              "Dinner for 10pax",
            ],
          },
          {
            label: "Baits",
            value: ["100 pax"],
          },
          {
            label: "Ice",
            value: ["5 Bag"],
          },
          {
            label: "Storage box",
            value: ["2 box"],
          },
        ],
      },
      //   image: Boat,
    },
    {
      id: 6,
      bookingNumber: "BNF2004 KL-2",
      location: "Mersing",
      state: "Johor",
      date: "2022-05-16 10:00:00",
      customer: {
        name: "Zufar Bin Sazali",
        phone: "+60137999922",
        email: "zufar.sazali@gmail.com",
      },
      boat: {
        name: "Melor",
        crews: [
          {
            name: "Yusri Bin Yusof",
            role: "Driver",
          },
          {
            name: "Rais Atan Bin Haikal",
            role: "Crew",
          },
        ],
      },
      booking: {
        anglers: 10,
        date: "2022-02-12 08:30:00",
        price: 4800.0,
        package: [
          {
            label: "Food & Snacks",
            value: [
              "Breakfast for 10pax",
              "Lunch for 10pax",
              "Dinner for 10pax",
            ],
          },
          {
            label: "Baits",
            value: ["100 pax"],
          },
          {
            label: "Ice",
            value: ["5 Bag"],
          },
          {
            label: "Storage box",
            value: ["2 box"],
          },
        ],
      },
      //   image: Boat,
    },
  ]);

  const { id } = useParams();
  // const par = useParams();
  const [booking, setBooking] = useState(null);
  useEffect(() => {
    let bid = _.toInteger(id);
    setBooking(bookings.find((x) => x.id === bid));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, booking]);

  return (
    <KelautPage enableBackHeaderBtn>
      {booking && (
        <div className="b-detail">
          <KelautCard>
            <div className="b-detail-1">
              <div className="b-info">
                <div className="label">Booking No.</div>
                <div className="number">
                  #{booking && booking.bookingNumber}
                </div>
              </div>
              <img className="b-print-img" alt="print" src={PrintImage} />
            </div>
          </KelautCard>
          <CustomerInfo customer={booking.customer} />
          <BoatInfo boat={booking.boat} />
          <BookingInfo booking={booking.booking} />
          <PackageInformation bpackage={booking.booking.package} />
          <Button variant="light" className="b-postpone btn">
            Postpone Booking
          </Button>
          <Button variant="light" className="b-cancel btn">
            Cancel Booking
          </Button>
        </div>
      )}
    </KelautPage>
  );
};

export default BookingDetail;
