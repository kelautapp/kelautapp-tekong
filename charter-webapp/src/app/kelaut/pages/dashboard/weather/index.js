import React from "react";
import KelautCard from "../../../../components/kelaut-card";
import { useTranslation } from "react-i18next";

const Weather = (props) => {
  const [t] = useTranslation();
  return (
    <>
      <KelautCard title={t("TitleTodayWeather")}>
        <div className="row">
          <div className="col-sm-6"></div>
        </div>
      </KelautCard>
    </>
  );
};

export default Weather;
