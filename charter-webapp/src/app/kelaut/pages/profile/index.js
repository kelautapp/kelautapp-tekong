import React, { Suspense } from "react";
import { Route, Switch } from "react-router";
import Spinner from "../../../shared/Spinner";
import UserPage from "./user";

export const ProfileRoutes = () => {
  return (
    <Suspense fallback={<Spinner />}>
      <Switch>
        <Route path="/dashboard/profile" component={UserPage} />
      </Switch>
    </Suspense>
  );
};

export default ProfileRoutes;
