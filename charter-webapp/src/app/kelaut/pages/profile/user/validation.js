import * as Yup from "yup";

export const validate = Yup.object({
  birthDate: Yup.date().required("This field is required!"),
  username: Yup.string().required("This field is required!"),
  email: Yup.string().required("This field is required!"),
  name: Yup.string().required("This field is required!"),
  companyName: Yup.string().required("This field is required!"),
  phone: Yup.string().required("This field is required!"),
  language: Yup.string(),
  startCareer: Yup.date().required("This field is required!"),
  // icFront: Yup.mixed().required("Front of IC is required!"),
  // icBack: Yup.mixed().required("Back of IC is required!"),
  // icBack: Yup.object().shape({
  //   name: Yup.string().required("File is required"),
  // }),
});
