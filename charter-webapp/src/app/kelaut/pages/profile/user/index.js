import { Form, Formik } from "formik";
import React, { useContext, useState, useEffect } from "react";
import { InputField } from "../../../../components/forms-ui/Input";
import DatePicker from "react-datepicker";
import { LoaderContext } from "../../../../hooks";
import moment from "moment";
import useUserProfileVerification from "../../../../hooks/authentication/verifyProfile";
import useGetUserProfile from "../../../../hooks/authentication/getUserProfile";
import { validate } from "./validation";
import { toast } from "react-toastify";

export const UserPage = () => {
  const {
    loading: getUserLoading,
    userProfile,
    errors: getUserProfileErrors,
    runInitialOperation,
  } = useGetUserProfile();

  const { setLoading } = useContext(LoaderContext);

  const [initialValues, setInitialValues] = useState({
    birthDate: new Date(),
    username: "",
    email: "",
    name: "",
    companyName: "",
    phone: "",
    language: "MY",
    icFront: null,
    icBack: null,
    startCareer: new Date(),
  });

  const { loading, runOperation, setRawData, profile } =
    useUserProfileVerification(null);

  const onVerifyUser = async (values) => {
    setRawData({ ...values, id: userProfile.id });
    runOperation();
  };

  useEffect(() => {
    if (userProfile) {
      setInitialValues({
        birthDate:
          userProfile.birthDate && userProfile.birthDate !== "N/A"
            ? moment(userProfile.birthDate, "YYYY-MM-DD").toDate()
            : new Date(),
        username: userProfile.email,
        email: userProfile.email,
        name: userProfile.name,
        companyName: userProfile.companyName,
        phone: userProfile.phone,
        language: "MY",
        startCareer:
          userProfile.startCareer && userProfile.startCareer !== "N/A"
            ? moment(userProfile.startCareer, "YYYY-MM-DD").toDate()
            : new Date(),
        icFront: null,
        icBack: null,
      });
    }
  }, [userProfile]);

  useEffect(() => {
    if (getUserLoading !== null) {
      setLoading(getUserLoading);
    }

    if (loading !== null) {
      setLoading(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getUserLoading, loading]);

  useEffect(() => {
    if (getUserProfileErrors) {
      toast(getUserProfileErrors, { type: "error" });
      setLoading(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getUserProfileErrors]);

  useEffect(() => {
    if (profile) {
      // toast(getUserProfileErrors, { type: "error" });
      // setLoading(false);
      runInitialOperation();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [profile]);

  return (
    <div className="row kelaut-form">
      <div className="col-sm-12">
        <div className="card">
          <div className="card-body">
            <div className="d-flex flex-row justify-content-between">
              <h4 className="card-title mb-1">User Profile</h4>
              <p className="text-muted mb-1">
                {" "}
                <i className="mdi mdi-dots-horizontal"></i>
              </p>
            </div>
            {/* <div className="float-right">
              <Badge variant="warning">
                {userProfile && ProfileStatus[userProfile.status]}
              </Badge>
            </div>
            <div style={{ clear: "both" }}></div> */}
            <Formik
              initialValues={initialValues}
              enableReinitialize={true}
              validationSchema={validate}
              onSubmit={(values) => onVerifyUser(values)}
            >
              {({ values, setFieldValue }) => (
                <Form>
                  <InputField
                    label="Username"
                    type="email"
                    className="form-control form-control-lg input-border"
                    id="username"
                    name="username"
                    placeholder="Username"
                    disabled
                  />
                  <InputField
                    label="Email"
                    type="email"
                    className="form-control form-control-lg input-border"
                    id="email"
                    name="email"
                    placeholder="Email"
                    disabled
                  />
                  <InputField
                    label="Full Name"
                    type="text"
                    className="form-control form-control-lg input-border"
                    id="name"
                    name="name"
                    placeholder="Full Name"
                  />
                  <InputField
                    label="Company Name"
                    type="text"
                    className="form-control form-control-lg input-border"
                    id="companyName"
                    name="companyName"
                    placeholder="Company Name"
                  />
                  <InputField
                    label="Phone"
                    type="text"
                    className="form-control form-control-lg input-border"
                    id="phone"
                    name="phone"
                    placeholder="Phone"
                    disabled
                  />
                  <InputField
                    label="Language"
                    type="text"
                    className="form-control form-control-lg input-border"
                    id="language"
                    name="language"
                    placeholder="Language"
                  />
                  <div className="form-group search-field">
                    <label>Birth Date</label>
                    <DatePicker
                      selected={values.birthDate}
                      dateFormat="MMMM d, yyyy"
                      className="form-control"
                      name="birthDate"
                      onChange={(date) => setFieldValue("birthDate", date)}
                    />
                  </div>
                  <div className="form-group search-field">
                    <label>Start Career Date</label>
                    <DatePicker
                      selected={values.startCareer}
                      dateFormat="MMMM d, yyyy"
                      className="form-control"
                      name="startCareer"
                      onChange={(date) => setFieldValue("startCareer", date)}
                    />
                  </div>
                  {/* {(verification == null ||
                    verification.status !== "PENDING") && (
                    <>
                      <InputFileField
                        name="icFront"
                        label="Front Of Ic"
                        id="icFront"
                      />

                      <InputFileField
                        name="icBack"
                        label="Back Of Ic"
                        id="icBack"
                      />
                    </>
                  )} */}
                  <hr />
                  {userProfile && getUserProfileErrors === null && (
                    <div className="mt-3">
                      <div className="float-right">
                        <button
                          type="submit"
                          className="btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn"
                        >
                          Update Profile
                        </button>
                      </div>
                      <div style={{ clear: "both" }}></div>
                    </div>
                  )}
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserPage;
