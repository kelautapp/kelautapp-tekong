import { Form, Formik } from "formik";
import React, { useContext, useEffect } from "react";
import { InputField } from "../../../../components/forms-ui/Input";
import { LoaderContext } from "../../../../hooks";

import { validate } from "./validation";
import { toast } from "react-toastify";
import { useChangePassword } from "../../../../hooks/authentication/change-password";
import { Alert } from "react-bootstrap";

export const ChangePasswordPage = () => {
  const { setLoading } = useContext(LoaderContext);
  const { loading, error, completeChangePassword, onChangePassword } =
    useChangePassword();

  const handleSubmit = async (values) => {
    await onChangePassword(values);
  };

  useEffect(() => {
    if (loading !== null) setLoading(loading);
  }, [loading, setLoading]);

  useEffect(() => {
    if (error) {
      toast(error, { type: "error" });
    }
  }, [error]);

  useEffect(() => {
    if (completeChangePassword) {
      toast("Change password successfully", { type: "success" });
    }
  }, [completeChangePassword]);

  return (
    <div className="row kelaut-form">
      <div className="col-sm-12">
        <div className="card">
          <div className="card-body">
            <div className="d-flex flex-row justify-content-between">
              <h4 className="card-title mb-1">Change Password</h4>
              <p className="text-muted mb-1">
                {" "}
                <i className="mdi mdi-dots-horizontal"></i>
              </p>
            </div>

            <Formik
              initialValues={{
                oldPassword: "",
                newPassword: "",
              }}
              enableReinitialize={true}
              validationSchema={validate}
              onSubmit={(values) => handleSubmit(values)}
            >
              {({ values, setFieldValue }) => (
                <Form>
                  <InputField
                    label="Current Password"
                    type="password"
                    className="form-control form-control-lg input-border"
                    id="oldPassword"
                    name="oldPassword"
                    placeholder="Current Password"
                  />
                  <InputField
                    label="New Password"
                    type="password"
                    className="form-control form-control-lg input-border"
                    id="newPassword"
                    name="newPassword"
                    placeholder="New Password"
                  />

                  <hr />
                  {error && <Alert variant="danger">{error}</Alert>}
                  <div className="mt-3">
                    <div className="float-right">
                      <button
                        type="submit"
                        className="btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn"
                      >
                        Change Password
                      </button>
                    </div>
                    <div style={{ clear: "both" }}></div>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ChangePasswordPage;
