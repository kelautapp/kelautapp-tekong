import * as Yup from "yup";

export const validate = Yup.object({
  oldPassword: Yup.string().required("This field is required!"),
  newPassword: Yup.string().required("This field is required!"),
});
