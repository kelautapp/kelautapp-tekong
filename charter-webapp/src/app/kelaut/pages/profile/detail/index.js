import React, { useState, useContext, useEffect } from "react";
import DisplayInfo from "../../../../components/display-info";
import KelautCard from "../../../../components/kelaut-card";
import { LoaderContext, SetupContext } from "../../../../hooks";
import BaseVariantComp from "../../../animations/framer-variants/components/base-variant-comp";
import { generalPageMainContainerVariants } from "../../../animations/framer-variants/general-page";
import * as moment from "moment";
import ProfileWithAvatar from "../../../../components/profile-with-avatar";
import Modal from "../../../../components/modal";
import UpdateProfileForm from "../../../../components/update-profile-form";
import useUserProfileVerification from "../../../../hooks/authentication/verifyProfile";
import { Storage } from "aws-amplify";
import ChangePasswordForm from "../../../../components/change-password-form";
import { useChangePassword } from "../../../../hooks/authentication/change-password";
import { toast } from "react-toastify";
import ToastrService from "../../../../../services/toastr";
import KelautPage from "../../../../components/kelaut-page";
import { useTranslation } from "react-i18next";

const SummaryKey = Object.freeze({
  email: "Email",
  name: "Name",
  phone: "Mobile Phone",
  companyName: "Company Name",
  nationality: "Nationality",
  birthDate: "Date Of Birth",
});

export default function ProfileDetail() {
  const [t] = useTranslation();
  const { setupState, dispatch } = useContext(SetupContext);
  const { setLoading } = useContext(LoaderContext);
  const [profileData, setProfileData] = useState(null);
  const [showUpdateProfile, setShowUpdateProfile] = useState(false);
  const [showChgPwd, setShowChgPwd] = useState(false);
  const { loading, runOperation, setRawData, profile, complete } =
    useUserProfileVerification(null);

  const {
    loading: passswordLoading,
    error,
    completeChangePassword,
    onChangePassword,
  } = useChangePassword();

  async function generateUrl(key) {
    return await Storage.get(key, {
      level: "public", // defaults to `public`
    });
  }

  function handleSubmit(data) {
    setRawData(data);
    runOperation();
  }

  async function combineData(profile) {
    let temp = { ...setupState.user };
    const photoUrl =
      profile.photo && profile.photo.toLowerCase() !== "null"
        ? await generateUrl(profile.photo)
        : null;
    const backdropUrl =
      profile.backdrop && profile.backdrop.toLowerCase() !== "null"
        ? await generateUrl(profile.backdrop)
        : null;
    temp.profileDetails = { ...profile, photoUrl, backdropUrl };
    dispatch({
      type: "ADD_SETUP",
      payload: temp,
    });
  }

  useEffect(() => {
    if (setupState.load) {
      if (setupState.user && setupState.user.profileDetails) {
        setProfileData({
          ...setupState.user.profileDetails,
          birthDate: moment(setupState.user.profileDetails.birthDate).format(
            "DD-MM-YYYY"
          ),
        });
      }
    }
  }, [setupState]);

  useEffect(() => {
    if (loading !== null) {
      if (loading === false) setShowUpdateProfile(false);
      setLoading(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (passswordLoading !== null) {
      if (passswordLoading === false) setShowChgPwd(false);
      setLoading(passswordLoading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [passswordLoading]);

  useEffect(() => {
    if (profile) {
      combineData(profile);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [profile]);

  useEffect(() => {
    if (error) {
      toast(error, { type: "error" });
    }
  }, [error]);

  useEffect(() => {
    if (completeChangePassword) {
      toast(t("SuccessChangePassword"), { type: "success" });
    }
  }, [completeChangePassword, t]);

  useEffect(() => {
    if (complete) {
      ToastrService.success(t("SuccessEditProfile"));
    }
  }, [complete, t]);

  return (
    <KelautPage enableBackHeaderBtn>
      <BaseVariantComp variants={generalPageMainContainerVariants}>
        {setupState.user && setupState.user.profileDetails && (
          <ProfileWithAvatar
            name={setupState.user.profileDetails.name}
            email={setupState.user.profileDetails.email}
            phone={setupState.user.profileDetails.phone}
            photo={setupState.user.profileDetails.photoUrl}
            backdrop={setupState.user.profileDetails.backdropUrl}
            onEdit={() => setShowUpdateProfile(true)}
            onChgPwd={() => setShowChgPwd(true)}
          />
        )}
      </BaseVariantComp>
      <BaseVariantComp variants={generalPageMainContainerVariants}>
        <KelautCard title={t("ProfileInformation")}>
          {profileData &&
            // eslint-disable-next-line array-callback-return
            Object.keys(profileData).map((k) => {
              if (SummaryKey[k]) {
                return (
                  <DisplayInfo
                    key={k}
                    title={t(SummaryKey[k].replace(/\s/g, ""))}
                    detail={profileData[k]}
                  />
                );
              }
            })}
        </KelautCard>
        {showUpdateProfile && (
          <Modal
            handleClose={() => setShowUpdateProfile(false)}
            title={t("UpdateProfile")}
            show={showUpdateProfile}
          >
            <UpdateProfileForm
              profileData={profileData}
              handleSubmit={handleSubmit}
            />
          </Modal>
        )}

        {showChgPwd && (
          <Modal
            handleClose={() => setShowChgPwd(false)}
            title={t("ResetPasswordButton")}
            show={showChgPwd}
          >
            <ChangePasswordForm onHandleSubmit={onChangePassword} />
          </Modal>
        )}
      </BaseVariantComp>
    </KelautPage>
  );
}
