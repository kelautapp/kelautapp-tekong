import React from "react";
import DashboardWeather from "../../../../components/dashboard-weather";
import KelautCard from "../../../../components/kelaut-card";
import PageBackNavigator from "../../../../components/page-back-navigator";
import BaseVariantComp from "../../../animations/framer-variants/components/base-variant-comp";
import {
  generalCardVariants,
  generalPageMainContainerVariants,
} from "../../../animations/framer-variants/general-page";
import { useHistory } from "react-router-dom";
import WeatherForecast from "../../../../components/weather-forecast";
import { useTranslation } from "react-i18next";
// eslint-disable-next-line import/no-anonymous-default-export
export default function () {
  const [t] = useTranslation();
  const history = useHistory();

  function backToDashboard() {
    history.push("/dashboard");
  }

  function goToMap() {
    history.push("/dashboard/weather/weathermap");
  }

  return (
    <BaseVariantComp variants={generalPageMainContainerVariants}>
      {/* back page container */}
      <div className="row">
        <div className="col-sm-12">
          <BaseVariantComp variants={generalCardVariants}>
            <PageBackNavigator
              title={t("BackToDashboard")}
              onClick={backToDashboard}
            />
          </BaseVariantComp>
        </div>
      </div>
      {/* today weather */}
      <div className="row">
        <div className="col-sm-12">
          <BaseVariantComp variants={generalCardVariants}>
            <KelautCard title={t("TitleTodayWeather")}>
              <DashboardWeather />
            </KelautCard>
          </BaseVariantComp>
        </div>
      </div>

      {/* weather forecast 1 weeks */}
      <div className="row">
        <div className="col-sm-12">
          <BaseVariantComp variants={generalCardVariants}>
            <KelautCard
              title={t("WeeklyForecastTitle")}
              bottomMenu={t("WeeklyForecastBtmMenu")}
              bottomMenuObject={goToMap}
            >
              <WeatherForecast />
            </KelautCard>
          </BaseVariantComp>
        </div>
      </div>
    </BaseVariantComp>
  );
}
