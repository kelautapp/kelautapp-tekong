import React, { useEffect, useState } from "react";

export const WeatherMap = () => {
  const [height, setHeight] = useState(0);
  const [width, setWidth] = useState(0);
  const [lat, setLat] = useState(1.164);
  const [lng, setLng] = useState(101.898);

  function setPosition(position) {
    setLat(position.coords.latitude);
    setLng(position.coords.longitude);
  }

  useEffect(() => {
    // const height = window.innerHeight - 70;
    // setWidth(document.getElementsByClassName("weather-map")[0].clientWidth);
    // setHeight(height);
    function handleResize() {
      const height = window.innerHeight - 70;
      setWidth(document.getElementsByClassName("weather-map")[0].clientWidth);
      setHeight(height);
    }

    handleResize();
    navigator.geolocation.getCurrentPosition(setPosition);

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const iframe = `<iframe width="${width}" height="${height}" src="https://embed.windy.com/embed2.html?lat=${lat}&lon=${lng}&detailLat=${lat}&detailLon=${lng}&width=375&height=800&zoom=6&level=surface&overlay=wind&product=ecmwf&menu=&message=true&marker=true&calendar=now&pressure=true&type=map&location=coordinates&detail=true&metricWind=default&metricTemp=default&radarRange=-1" frameborder="0"></iframe>`;
  return (
    <div className="weather-map">
      <Iframe iframe={iframe} />
    </div>
  );
};

function Iframe(props) {
  return (
    <div
      dangerouslySetInnerHTML={{ __html: props.iframe ? props.iframe : "" }}
    />
  );
}

export default WeatherMap;
