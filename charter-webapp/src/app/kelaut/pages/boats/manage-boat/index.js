import React, { useState, useEffect, useContext } from "react";
import { validate } from "./validation";
import { FormBuilderComponent } from "../../../../components";
import { Button, Tab, Tabs } from "react-bootstrap";
import { BOAT_TABS } from "./config";
import { listOfYear } from "../boat-creation/data";
import useBoat from "../../../../hooks/boat-management/boat";
import { LoaderContext } from "../../../../hooks";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";

// render(progressInstance);

export function ManageBoat(props) {
  const { setLoading } = useContext(LoaderContext);
  let { id } = useParams();
  // const [id, setId] = useState(null);
  const { loading, onGetBoat, boat, onUpdateBoat, completeUpdateBoat } =
    useBoat();
  const [initialValues, setinitialValues] = useState({
    typeOfBoat: "",
    manufacturer: "",
    length: "",
    year: "",
    condition: "",
    capacity: "",
  });

  const years = listOfYear().map((year) => ({ label: year, value: year }));

  const BoatCreationFormStructure = {
    formSplit: "two",
    forms: [
      {
        type: "input-text",
        formStructure: {
          label: "Type Of Boat",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "type",
          name: "type",
          placeholder: "Type of boats",
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: "Manufacturer",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "manufacturer",
          name: "manufacturer",
          placeholder: "Manufacturer",
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: "Length",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "length",
          name: "length",
          placeholder: "Length",
        },
      },
      {
        type: "input-select",
        formStructure: {
          label: "Year",
          // type: "text",
          // className: "form-control form-control-lg input-border",
          id: "modelYear",
          name: "modelYear",
          placeholder: "Year",
          options: [...years],
          isSearchable: true,
        },
      },
      {
        type: "input-select",
        formStructure: {
          label: "Condition",
          // type: "text",
          // className: "form-control form-control-lg input-border",
          id: "boatCondition",
          name: "boatCondition",
          placeholder: "Condition",
          options: [
            { value: "NEW", label: "New" },
            { value: "USED", label: "Used" },
          ],
          isSearchable: true,
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: "Capacity",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "capacity",
          name: "capacity",
          placeholder: "Capacity",
        },
      },
    ],
    formButton: [
      {
        type: "submit",
        name: "Update Boat",
        className:
          "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
      },
    ],
  };

  function onHandleSubmit(values) {
    let temp = { ...values, id: id };
    delete temp["number"];
    delete temp["size"];
    delete temp["profileId"];
    delete temp["lat"];
    delete temp["lng"];
    delete temp["zoom"];
    onUpdateBoat(temp);
  }

  useEffect(() => {
    onGetBoat(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (boat) {
      setinitialValues({ ...boat });
    }
  }, [boat]);

  useEffect(() => {
    if (props.initialized) {
      setinitialValues({ ...props.initialValues });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.initialized]);

  useEffect(() => {
    if (loading !== null) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (completeUpdateBoat) {
      toast.success("Successfully Update boat details");
      // setLoading(false);
    }
  }, [completeUpdateBoat]);

  return (
    <div className="row kelaut-form">
      {/* <div className="row"> */}
      <div className="col-sm-12 mb-3">
        {/* <div className="row">
          <div className="col-sm-12">
            <h4>
              <b>B-00-1</b>
            </h4>
          </div>
        </div> */}
        <div className="row">
          {/* <div className="col-md-11 col-sm-12">{progressInstance}</div> */}
          <div className="col-sm-6">
            <h4>{boat && <b>{boat.number}</b>}</h4>
          </div>
          <div className="col-sm-6">
            <div className="float-right">
              <Button>Publish</Button>
            </div>
          </div>
        </div>
      </div>
      {/* </div> */}
      <div className="col-sm-12">
        <div className="card">
          <div className="card-body">
            <div className="d-flex flex-row justify-content-between">
              <h4 className="card-title mb-1">Manage Boat</h4>
              <p className="text-muted mb-1">
                {" "}
                <i className="mdi mdi-dots-horizontal"></i>
              </p>
            </div>
            <FormBuilderComponent
              initialValue={initialValues}
              validation={validate}
              structures={BoatCreationFormStructure}
              onHandleSubmit={(values) => onHandleSubmit(values)}
            />
            <div className="row mt-3">
              <div className="col-sm-12">
                <Tabs className="mb-2 mt-2">
                  {/* <Tab eventKey="media" title="Media">
                    Media Tab
                  </Tab>
                  <Tab eventKey="location" title="Location">
                    Location Tab
                  </Tab> */}
                  {BOAT_TABS.map((boat) => (
                    <Tab {...boat} key={boat.eventKey}>
                      {boat.hasOwnProperty("component")
                        ? boat.component()
                        : boat.title + "tabs"}
                    </Tab>
                  ))}
                </Tabs>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
