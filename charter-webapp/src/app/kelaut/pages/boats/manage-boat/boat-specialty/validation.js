import * as Yup from "yup";

export const validate = Yup.object({
  manufacturer: Yup.string().required("Please specify the manufacturer"),
  engineCount: Yup.string().required("Please specify the engine count"),
  horsePower: Yup.string().required("Please specify the horse power"),
  speed: Yup.string().required("Please specify the speed"),
});
