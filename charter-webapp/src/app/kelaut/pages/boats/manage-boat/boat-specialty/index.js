import React, { useState, useEffect } from "react";
import { validate } from "./validation";
import { FormBuilderComponent } from "../../../../../components";
import { Button } from "react-bootstrap";

export function BoatSpecialty(props) {
  const [initialValues, setinitialValues] = useState([
    {
      gearCrewName: "",
    },
  ]);

  const BoatCreationFormStructure = {
    formSplit: "two",
    forms: [
      {
        type: "input-text",
        formStructure: {
          label: "Gear Crew Name",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "gearCrewName",
          name: "gearCrewName",
          placeholder: "Gear Crew Name",
        },
      },
    ],
    formButton: [
      {
        type: "submit",
        name: "Save",
        className:
          "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
      },
    ],
  };

  function addBoat() {
    let tempBoat = [...initialValues];
    tempBoat.push({
      manufacturer: "",
      engineCount: "",
      horsePower: "",
      speed: "",
    });

    setinitialValues([...tempBoat]);
  }

  useEffect(() => {
    if (props.initialized) {
      setinitialValues({ ...props.initialValues });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.initialized]);

  return (
    <div className="row kelaut-form">
      <div className="col-sm-12">
        <div className="card">
          <div className="card-body">
            <div className="d-flex flex-row justify-content-between">
              <h4 className="card-title mb-1">Boat Crews</h4>
              <p className="text-muted mb-1">
                {" "}
                <i className="mdi mdi-dots-horizontal"></i>
              </p>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div className="float-right">
                  <Button onClick={addBoat}>Add Crew</Button>
                </div>
              </div>
            </div>
            {initialValues.map((val, i) => (
              <div key={i}>
                <h5>Crew {i + 1}</h5>
                <FormBuilderComponent
                  key={i}
                  initialValue={val}
                  validation={validate}
                  structures={BoatCreationFormStructure}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
