import React, { useState, useEffect, useContext } from "react";
import { validate } from "./validation";
import { FormBuilderComponent } from "../../../../../components";
import { Button } from "react-bootstrap";
import { LoaderContext } from "../../../../../hooks";
import { useBoatEngine } from "../../../../../hooks/boat-management/boat-engine";
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";
import {
  CreateBoatEngine,
  UpdateBoatEngine,
} from "../../../../../types/boat/boat-engine";

export function BoatEngine(props) {
  let { id } = useParams();
  const { setLoading } = useContext(LoaderContext);
  const [activeSaveIndex, setActiveSaveIndex] = useState(null);
  const {
    loading,
    boatEngine,
    onCreateBoatEngine,
    completeBoatEngine,
    onGetBoatEngine,
    boatEngines,
    onUpdateBoatEngine,
    completeUpdateBoatEngine,
  } = useBoatEngine();

  const [initialValues, setinitialValues] = useState([
    {
      id: null,
      manufacturer: "",
      engineCount: "",
      horsePower: "",
      speed: "",
    },
  ]);

  const BoatCreationFormStructure = {
    formSplit: "two",
    forms: [
      {
        type: "input-text",
        formStructure: {
          label: "Manufacturer",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "manufacturer",
          name: "manufacturer",
          placeholder: "Manufacturer",
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: "Engine Count",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "engineCount",
          name: "engineCount",
          placeholder: "Engine Count",
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: "Horse Power",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "horsePower",
          name: "horsePower",
          placeholder: "Horse Power",
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: "Speed",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "speed",
          name: "speed",
          placeholder: "Speed",
        },
      },
    ],
    formButton: [
      {
        type: "submit",
        name: "Save Engine",
        className:
          "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
      },
    ],
  };

  function addBoat() {
    let tempBoat = [...initialValues];
    tempBoat.push({
      manufacturer: "",
      engineCount: "",
      horsePower: "",
      speed: "",
    });

    setinitialValues([...tempBoat]);
  }

  function onHandleSubmit(values, index) {
    if (values.id) {
      let data = new UpdateBoatEngine(values);
      onUpdateBoatEngine(data);
    } else {
      setActiveSaveIndex(index);
      let data = new CreateBoatEngine(values);
      data.setBoatId(id);
      onCreateBoatEngine(data);
    }
  }

  useEffect(() => {
    if (props.initialized) {
      setinitialValues({ ...props.initialValues });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.initialized]);

  useEffect(() => {
    if (loading !== null) {
      setLoading(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (completeBoatEngine) {
      toast.success("Successfully create boat engine");
    }

    if (completeUpdateBoatEngine) {
      toast.success("Successfully update boat engine");
    }
  }, [completeBoatEngine, completeUpdateBoatEngine]);

  useEffect(() => {
    if (boatEngine) {
      let tempValue = [...initialValues];

      tempValue[activeSaveIndex] = { ...boatEngine };
      setinitialValues(tempValue);
      setActiveSaveIndex(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [boatEngine]);

  useEffect(() => {
    if (boatEngines.length > 0) {
      setinitialValues([...boatEngines]);
    }
  }, [boatEngines]);

  useEffect(() => {
    if (id) onGetBoatEngine(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  return (
    <div className="row kelaut-form">
      <div className="col-sm-12">
        <div className="card">
          <div className="card-body">
            <div className="d-flex flex-row justify-content-between">
              <h4 className="card-title mb-1">Boat Creation</h4>
              <p className="text-muted mb-1">
                {" "}
                <i className="mdi mdi-dots-horizontal"></i>
              </p>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div className="float-right">
                  <Button onClick={addBoat}>Add Engine</Button>
                </div>
              </div>
            </div>
            {initialValues.map((val, i) => (
              <div key={i}>
                <h5>Boat {i + 1}</h5>
                <FormBuilderComponent
                  key={val}
                  initialValue={val}
                  validation={validate}
                  structures={BoatCreationFormStructure}
                  onHandleSubmit={(values) => onHandleSubmit(values, i)}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
