import * as Yup from "yup";

export const validate = Yup.object({
  facilityName: Yup.string().required("Please specify the facility"),
});
