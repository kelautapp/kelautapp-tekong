import { BoatEngine } from "./boat-engine";
import React from "react";
import { BoatNavigation } from "./boat-navigation";
import { BoatFacility } from "./boat-facility";
import { BoatFeature } from "./boat-feature";
import { BoatSpecialty } from "./boat-specialty";
import { BoatFishingType } from "./boat-fishing-type";
import { BoatPackage } from "../boat-package";
import { BoatLocation } from "./boat-location";
export const BOAT_TABS = [
  {
    eventKey: "media",
    title: "Media",
  },
  {
    eventKey: "location",
    title: "Location",
    component: () => <BoatLocation />,
  },
  {
    eventKey: "engine",
    title: "Engine",
    component: () => <BoatEngine />,
  },
  {
    eventKey: "navigation",
    title: "Navigation",
    component: () => <BoatNavigation />,
  },
  {
    eventKey: "facility",
    title: "Facility",
    component: () => <BoatFacility />,
  },
  {
    eventKey: "features",
    title: "Features",
    component: () => <BoatFeature />,
  },
  {
    eventKey: "crew",
    title: "Crews",
    component: () => <BoatSpecialty />,
  },
  {
    eventKey: "fishingType",
    title: "Fishing Type",
    component: () => <BoatFishingType />,
  },
  {
    eventKey: "package",
    title: "Package",
    component: () => <BoatPackage />,
  },
];
