import * as Yup from "yup";

export const validate = Yup.object({
  fishingType: Yup.string().required("Please specify the fishing type"),
});
