import React, { useState, useEffect, useContext } from "react";
import { validate } from "./validation";
import { FormBuilderComponent } from "../../../../../components";
import { Button } from "react-bootstrap";
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";
import { LoaderContext } from "../../../../../hooks";
import { useBoatNavigation } from "../../../../../hooks/boat-management/boat-navigation";
import {
  CreateBoatNavigation,
  UpdateBoatNavigation,
} from "../../../../../types/boat/boat-navigation";

export function BoatNavigation(props) {
  let { id } = useParams();
  const { setLoading } = useContext(LoaderContext);
  const [activeSaveIndex, setActiveSaveIndex] = useState(null);
  const {
    loading,
    data,
    onCreate,
    complete,
    onGet,
    datas,
    onUpdate,
    completeUpdate,
  } = useBoatNavigation();

  const [initialValues, setinitialValues] = useState([
    {
      deviceName: "",
    },
  ]);

  const BoatCreationFormStructure = {
    formSplit: "two",
    forms: [
      {
        type: "input-text",
        formStructure: {
          label: "Device Name",
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "deviceName",
          name: "deviceName",
          placeholder: "Device Name",
        },
      },
    ],
    formButton: [
      {
        type: "submit",
        name: "Save Navigation",
        className:
          "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
      },
    ],
  };

  function addBoat() {
    let tempBoat = [...initialValues];
    tempBoat.push({
      deviceName: "",
    });

    setinitialValues([...tempBoat]);
  }

  function onHandleSubmit(values, index) {
    if (values.id) {
      let data = new UpdateBoatNavigation(values);
      onUpdate(data);
    } else {
      setActiveSaveIndex(index);
      let data = new CreateBoatNavigation(values);
      data.setBoatId(id);
      onCreate(data);
    }
  }

  useEffect(() => {
    if (props.initialized) {
      setinitialValues({ ...props.initialValues });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.initialized]);

  useEffect(() => {
    if (loading !== null) {
      setLoading(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (complete) {
      toast.success("Successfully create boat navigation");
    }

    if (completeUpdate) {
      toast.success("Successfully update boat navigation");
    }
  }, [complete, completeUpdate]);

  useEffect(() => {
    if (data) {
      let tempValue = [...initialValues];

      tempValue[activeSaveIndex] = { ...data };
      setinitialValues(tempValue);
      setActiveSaveIndex(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  useEffect(() => {
    if (datas.length > 0) {
      setinitialValues([...datas]);
    }
  }, [datas]);

  useEffect(() => {
    if (id) onGet(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  return (
    <div className="row kelaut-form">
      <div className="col-sm-12">
        <div className="card">
          <div className="card-body">
            <div className="d-flex flex-row justify-content-between">
              <h4 className="card-title mb-1">Boat Navigation</h4>
              <p className="text-muted mb-1">
                {" "}
                <i className="mdi mdi-dots-horizontal"></i>
              </p>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <div className="float-right">
                  <Button onClick={addBoat}>Add Navigation</Button>
                </div>
              </div>
            </div>
            {initialValues.map((val, i) => (
              <div key={i}>
                <h5>Navigation Device {i + 1}</h5>
                <FormBuilderComponent
                  key={i}
                  initialValue={val}
                  validation={validate}
                  structures={BoatCreationFormStructure}
                  onHandleSubmit={(values) => onHandleSubmit(values, i)}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
