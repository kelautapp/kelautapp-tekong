import * as Yup from "yup";

export const validate = Yup.object({
  deviceName: Yup.string().required("Please specify the device name"),
});
