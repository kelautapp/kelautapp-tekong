import * as Yup from "yup";

export const validate = Yup.object({
  featureName: Yup.string().required("Please specify the feature name"),
});
