import * as Yup from "yup";

export const validate = Yup.object({
  type: Yup.string().required("Please specity the type of boats"),
  manufacturer: Yup.string().required(
    "Please specify the manufacturer of the boats!"
  ),
  length: Yup.string(),
  modelYear: Yup.string().required(
    "Please specify the year of boat manufactured!"
  ),
  boatCondition: Yup.string().required("Please specify the condition of boat"),
  capacity: Yup.number().required(
    "Please specify the maximum capacity of the boat!"
  ),
});
