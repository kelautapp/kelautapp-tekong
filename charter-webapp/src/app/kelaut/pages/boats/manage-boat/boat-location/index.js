import React, { useCallback, useEffect } from "react";
import { GoogleMap, useJsApiLoader, Marker } from "@react-google-maps/api";
import { Button } from "react-bootstrap";
import { toast } from "react-toastify";
import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from "use-places-autocomplete";
import mapStyles from "./style";
import useBoat from "../../../../../hooks/boat-management/boat";
import { useParams } from "react-router-dom";

const libraries = ["places"];
const mapContainerStyle = {
  height: "80vh",
  width: "100%",
};
const options = {
  styles: mapStyles,
  disableDefaultUI: true,
  zoomControl: true,
};
const center = {
  lat: 3.672859376882748,
  lng: 105.4674620305061,
};

export function BoatLocation(props) {
  const { onGetBoat, onUpdateBoatLocation, boat } = useBoat();
  let { id } = useParams();
  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: `AIzaSyCpTdn2CnvMZZ4vJ0JgYyzSMsPm6B0dt9I`,
    libraries,
  });

  const [marker, setMarker] = React.useState(null);
  const mapRef = React.useRef();

  const onMapClick = useCallback((e) => {
    setMarker({
      lat: e.latLng.lat(),
      lng: e.latLng.lng(),
      zoom: mapRef.current.zoom,
    });
  }, []);

  const onMapLoad = React.useCallback((map) => {
    mapRef.current = map;
  }, []);

  const panTo = React.useCallback(({ lat, lng }) => {
    mapRef.current.panTo({ lat, lng });
    mapRef.current.setZoom(16);
  }, []);

  const onSavePinLocation = async () => {
    if (marker) onUpdateBoatLocation({ ...marker, id });
    else toast.error("Please select location!!");
  };

  useEffect(() => {
    if (id && isLoaded) onGetBoat(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, isLoaded]);

  useEffect(() => {
    if (boat && isLoaded) {
      const { lat, lng, zoom } = boat;
      if (lat && lng && zoom) {
        setMarker({ lat, lng, zoom });
        mapRef.current.panTo({ lat, lng });
        mapRef.current.setZoom(zoom);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [boat]);

  return (
    <>
      <div>
        {isLoaded ? (
          <div style={{ position: "relative" }}>
            <Search panTo={panTo} />
            <GoogleMap
              id="map"
              mapContainerStyle={mapContainerStyle}
              zoom={5}
              center={center}
              options={options}
              onClick={onMapClick}
              onLoad={onMapLoad}
            >
              {marker && (
                <Marker
                  key={`${marker.lat}-${marker.lng}`}
                  position={{ lat: marker.lat, lng: marker.lng }}
                  // onClick={() => {
                  //   setSelected(marker);
                  // }}
                  // icon={{
                  //   url: `/bear.svg`,
                  //   origin: new window.google.maps.Point(0, 0),
                  //   anchor: new window.google.maps.Point(15, 15),
                  //   scaledSize: new window.google.maps.Size(30, 30),
                  // }}
                />
              )}
            </GoogleMap>
          </div>
        ) : (
          <div>Loading...</div>
        )}
      </div>
      <div className="mt-3">
        <div className="float-right">
          <Button onClick={onSavePinLocation}>Save</Button>
        </div>
      </div>
    </>
  );
}

function Search({ panTo }) {
  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions,
  } = usePlacesAutocomplete({
    requestOptions: {
      location: { lat: () => center.lat, lng: () => center.lng },
      radius: 100 * 1000,
    },
  });

  const handleInput = (e) => {
    setValue(e.target.value);
  };

  const handleSelect = async (address) => {
    setValue(address, false);
    clearSuggestions();

    try {
      const results = await getGeocode({ address });
      const { lat, lng } = await getLatLng(results[0]);
      panTo({ lat, lng });
    } catch (error) {
      console.log("😱 Error: ", error);
    }
  };
  return (
    <div className="search">
      <input
        value={value}
        onChange={handleInput}
        disabled={!ready}
        placeholder="Search your location"
      />
      {status === "OK" && (
        <div className="suggestions">
          {data.map((d) => (
            <div key={d.id} onClick={() => handleSelect(d.description)}>
              {d.description}
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
