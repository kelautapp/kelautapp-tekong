import * as Yup from "yup";
import i18n from "../../../../../i18n";

export const validate = Yup.object({
  name: Yup.string().required(i18n.t("RequireBoatName")),
  maxSpeed: Yup.number()
    .required(i18n.t("RequireBoatSpeed"))
    .typeError(i18n.t("TypeErrorNumberOnly")),
  horsePower: Yup.number()
    .required(i18n.t("RequireBoatHP"))
    .typeError(i18n.t("TypeErrorNumberOnly")),
  length: Yup.number(),
  year: Yup.number()
    .required(i18n.t("RequireBoatYear"))
    .typeError(i18n.t("TypeErrorNumberOnly")),
  capacity: Yup.number()
    .required(i18n.t("RequireBoatCapacity"))
    .typeError(i18n.t("TypeErrorNumberOnly")),
  description: Yup.string().required(i18n.t("RequireBoatDesc")),
});
