import React, { useState, useEffect, useContext } from "react";
import { validate } from "./validation";
import { FormBuilderComponent } from "../../../../components";
import useBoat from "../../../../hooks/boat-management/boat";
import { LoaderContext, SetupContext } from "../../../../hooks";
import { listOfYear } from "./data";
import { useHistory } from "react-router-dom";
import { CreateBoat } from "../../../../types/boat/boat";
import ToastrService from "../../../../../services/toastr";
import { useTranslation } from "react-i18next";

export function BoatCreationPage(props) {
  const history = useHistory();
  const [t] = useTranslation();
  const { setupState } = useContext(SetupContext);
  const { setLoading } = useContext(LoaderContext);
  const { loading, onCreateBoat, boat, completeCreateBoat } = useBoat();
  const [initialValues, setinitialValues] = useState({
    name: "",
    description: "",
    horsePower: "",
    maxSpeed: "",
    length: "",
    capacity: "",
    year: "",
  });

  const years = listOfYear().map((year) => ({ label: year, value: year }));

  const BoatCreationFormStructure = {
    formSplit: "one",
    forms: [
      {
        type: "input-text",
        formStructure: {
          label: t("FormLabelBoatName"),
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "name",
          name: "name",
          placeholder: t("FormPlaceholderBoatName"),
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: t("FormLabelBoatHP"),
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "horsePower",
          name: "horsePower",
          placeholder: t("FormPlaceHolderBoatHP"),
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: t("FormLabelBoatSpeed"),
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "maxSpeed",
          name: "maxSpeed",
          placeholder: t("FormPlaceholderBoatSpeed"),
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: t("FormLabelBoatLength"),
          type: "text",
          className: "form-control form-control-lg input-border",
          id: "length",
          name: "length",
          placeholder: t("FormPlaceholderBoatLength"),
        },
      },
      {
        type: "input-text",
        formStructure: {
          label: t("FormLabelBoatCapacity"),
          type: "number",
          className: "form-control form-control-lg input-border",
          id: "capacity",
          name: "capacity",
          placeholder: t("FormPlaceholderBoatCapacity"),
        },
      },
      {
        type: "input-select",
        formStructure: {
          label: t("FormLabelBoatYear"),
          id: "year",
          name: "year",
          placeholder: t("FormPlaceholderBoatYear"),
          options: [...years],
          isSearchable: true,
        },
      },

      {
        type: "textarea",
        formStructure: {
          label: t("FormLabelBoatDesc"),
          className:
            "form-control form-control-lg input-border kelaut-textarea-ui",
          id: "description",
          name: "description",
          placeholder: t("FormPlaceholderBoatDesc"),
          rows: 5,
          maxLength: 200,
        },
      },
    ],
    formButton: [
      {
        type: "submit",
        name: t("FormSubmitAddBoat"),
        className:
          "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
      },
    ],
  };

  function onHandleSubmit(values) {
    let createBoat = new CreateBoat();
    createBoat.setData(values);
    createBoat.setProfileId(setupState.user.profileId);
    onCreateBoat(createBoat);
  }

  useEffect(() => {
    if (props.initialized) {
      setinitialValues({ ...props.initialValues });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.initialized]);

  useEffect(() => {
    if (loading) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (completeCreateBoat) {
      ToastrService.success("Successfully create boat");
      setLoading(false);
      history.push(`/dashboard/boats/boat-detail/${boat.id}`);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeCreateBoat]);

  return (
    <FormBuilderComponent
      initialValue={initialValues}
      validation={validate}
      structures={BoatCreationFormStructure}
      onHandleSubmit={(values) => onHandleSubmit(values)}
    />
  );
}
