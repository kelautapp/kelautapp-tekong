import React, { useContext, useEffect, useState } from "react";
import { Doughnut } from "react-chartjs-2";
import KelautCard from "../../../components/kelaut-card";
import KelautSimpleTable from "../../../components/kelaut-simple-table";
import PlusButton from "../../../../assets/images/kelaut/plusbtnblue.svg";
import Modal from "../../../components/modal";
import { BoatCreationPage } from "./boat-creation";
import { useHistory } from "react-router-dom";
import { LoaderContext, SetupContext } from "../../../hooks";
import useBoat from "../../../hooks/boat-management/boat";
import SubMenu from "../../../components/sub-menu";
import KelautPage from "../../../components/kelaut-page";
import i18n from "../../../../i18n";

const constructTheBoatReportChart = (active = 0, inactive = 0) => {
  return {
    datasets: [
      {
        data: [active, inactive],
        backgroundColor: [
          "rgba(255, 99, 132, 0.5)",
          "rgba(54, 162, 235, 0.5)",
          "rgba(255, 206, 86, 0.5)",
          "rgba(75, 192, 192, 0.5)",
          "rgba(153, 102, 255, 0.5)",
          "rgba(255, 159, 64, 0.5)",
        ],
        borderColor: [
          "rgba(255,99,132,1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
          "rgba(75, 192, 192, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(255, 159, 64, 1)",
        ],
      },
    ],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [i18n.t("Activate"), i18n.t("Deactivate")],
  };
};

const DoughnutPieOptions = {
  responsive: true,
  animation: {
    animateScale: true,
    animateRotate: true,
  },
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function () {
  const { setLoading } = useContext(LoaderContext);
  const { setupState } = useContext(SetupContext);
  const history = useHistory();

  const { onGetBoats, boats, loading } = useBoat();

  const [show, setShow] = useState(false);

  const [active, setActive] = useState([]);
  const [inactive, setInActive] = useState([]);
  const [report, setReport] = useState(() => constructTheBoatReportChart());

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const AddButton = () => {
    return (
      <img
        src={PlusButton}
        alt=""
        className="kelaut-card-custom-action"
        onClick={handleShow}
      />
    );
  };

  function goToBotDetail(id) {
    history.push(`/dashboard/boats/boat-detail/${id}`);
  }

  function getListOfActions(data) {
    return [
      {
        title: "Edit",
        onClick: () => goToBotDetail(data.id),
      },
      // {
      //   title: "Deactivate",
      //   onClick: () => deactivateCrew(data),
      // },
    ];
  }

  useEffect(() => {
    if (loading !== null) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (setupState) {
      onGetBoats(setupState.user.profileId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setupState]);

  useEffect(() => {
    if (boats.length > 0) {
      var tempData = {
        active: [],
        inactive: [],
      };
      boats.map((boat) => {
        if (boat.status.toLowerCase() === "active") tempData.active.push(boat);
        else tempData.inactive.push(boat);
        return boat;
      });

      setActive(tempData.active);
      setInActive(tempData.inactive);
    }
  }, [boats]);

  useEffect(() => {
    setReport(constructTheBoatReportChart(active.length, inactive.length));
  }, [active, inactive]);

  return (
    <KelautPage enableBackHeaderBtn>
      <KelautCard
        title={i18n.t("ActiveBoatListTitle")}
        enableOptions
        customOptions={AddButton}
      >
        {active && active.length > 0 && (
          <KelautSimpleTable
            rows={active}
            active
            // clickAction={goToBotDetail}
            actionId="id"
            customOptions={SubMenu}
            customAction={getListOfActions}
          />
        )}
        {!active || (active.length < 1 && <p>{i18n.t("NoActiveBoatMsg")}</p>)}
      </KelautCard>
      <KelautCard title={i18n.t("InactiveBoatListTitle")}>
        {inactive && inactive.length > 0 && (
          <KelautSimpleTable
            rows={inactive}
            // clickAction={goToBotDetail}
            actionId="id"
            customOptions={SubMenu}
            customAction={getListOfActions}
          />
        )}
        {!inactive ||
          (inactive.length < 1 && <p>{i18n.t("NoInactiveBoatMsg")}</p>)}
      </KelautCard>
      <KelautCard title={i18n.t("BoatReportTitle")} enableOptions>
        <Doughnut data={report} options={DoughnutPieOptions} />
      </KelautCard>
      <Modal
        title={i18n.t("AddNewBoatModalTitle")}
        handleClose={handleClose}
        show={show}
      >
        <BoatCreationPage />
      </Modal>
    </KelautPage>
  );
}
