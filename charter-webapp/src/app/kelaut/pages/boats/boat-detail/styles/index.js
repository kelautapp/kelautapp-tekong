import styled from "styled-components";

export const KelautContainer = styled.div`
  padding: 10px;
  background: #fff;
`;

export const KelautImgBox = styled.div`
  padding: 10px;
`;

export const KelautImages = styled.img`
  height: 100%;
  width: 100%;
  margin: 0;
  padding: 0;
`;

export const BoatSummaryTitle = styled.p`
  color: #5855d6;
  font-size: 24px;
  font-weight: bold;
`;

export const BoatSummaryDescription = styled.p`
  font-size: 16px;
`;
