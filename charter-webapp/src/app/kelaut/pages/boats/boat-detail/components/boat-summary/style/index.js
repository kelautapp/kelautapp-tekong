import styled from "styled-components";

export const RatingContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: flex-start;
`;

export const RatingNo = styled.span`
  font-weight: bold;
  font-size: 14px;
  margin: 5px;
  color: #9592a6;
`;

export const RatingAmount = styled.span`
  font-size: 14px;
  margin: 5px;
  color: #9592a6; ;
`;

export const BoatPlaceText = styled.p`
  font-size: 14px;
  color: #4ed964;
`;
