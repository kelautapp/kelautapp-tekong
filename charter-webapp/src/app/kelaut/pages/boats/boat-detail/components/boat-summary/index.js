import React, { useEffect, useState } from "react";
import {
  KelautImages,
  KelautImgBox,
  BoatSummaryTitle,
  BoatSummaryDescription,
} from "../../styles";
import { BOAT_LIST_OF_IMAGES } from "./data";
import "react-loading-skeleton/dist/skeleton.css";
import ReactStars from "react-rating-stars-component";
import {
  BoatPlaceText,
  RatingAmount,
  RatingContainer,
  RatingNo,
} from "./style";
// import { Placeholder } from "react-bootstrap";

const BoatImage = ({ image }) => {
  return (
    <KelautImgBox>
      <KelautImages src={image.url} />
    </KelautImgBox>
  );
};

export function BoatSummary() {
  const [images, setImages] = useState([]);

  useEffect(() => {
    setImages([...BOAT_LIST_OF_IMAGES]);
  }, []);

  return (
    <div className="row">
      <div className="col-sm-12">
        <div className="row">
          {images.length > 0 && (
            <div key={images[0].id} className="col-md-6">
              <BoatImage image={images[0]} />
            </div>
          )}
          {images.length > 0 && (
            <div className="col-md-6">
              <div className="row">
                <div key={images[1].id} className="col-sm-12 col-md-6">
                  <BoatImage image={images[1]} />
                </div>
                <div key={images[2].id} className="col-sm-12 col-md-6">
                  <BoatImage image={images[2]} />
                </div>
              </div>
              <div className="row">
                <div key={images[3].id} className="col-sm-12 col-md-6">
                  <BoatImage image={images[3]} />
                </div>
                <div key={images[4].id} className="col-sm-12 col-md-6">
                  <BoatImage image={images[4]} />
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
      <div className="col-sm-12">
        <div className="row">
          <div className="col-md-6">
            <BoatSummaryTitle>Thousand Sunny</BoatSummaryTitle>
          </div>
          <div className="col-md-6">
            <RatingContainer>
              <RatingNo>4.9</RatingNo>
              <ReactStars count={5} size={20} />{" "}
              <RatingAmount>(37)</RatingAmount>
            </RatingContainer>
          </div>
          <div className="col-sm-12">
            <BoatPlaceText>Persisiran Mersing, Johor, Malaysia</BoatPlaceText>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            {/* <Skeleton />
            <Skeleton /> */}
            <BoatSummaryDescription>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur.
            </BoatSummaryDescription>
          </div>
        </div>
      </div>
    </div>
  );
}
