import React, { useState, useContext, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, useParams } from "react-router-dom";
import BoatCrew from "../../../../components/boat-crew";
import BoatDetailMain from "../../../../components/boat-detail-main";
import BoatPackage from "../../../../components/boat-package";
import BoatPhoto from "../../../../components/boat-photo";
import PageBackNavigator from "../../../../components/page-back-navigator";
import { LoaderContext } from "../../../../hooks";
import useBoat from "../../../../hooks/boat-management/boat";
import BaseVariantComp from "../../../animations/framer-variants/components/base-variant-comp";
import {
  generalCardVariants,
  generalPageMainContainerVariants,
} from "../../../animations/framer-variants/general-page";

// eslint-disable-next-line import/no-anonymous-default-export
export default function () {
  const { id } = useParams();
  const history = useHistory();
  const { loading, boat, onUpdateBoatStatus } = useBoat();
  const { setLoading } = useContext(LoaderContext);
  const [status, setStatus] = useState(null);
  const [t] = useTranslation();

  function backToBoatListing() {
    history.push("/dashboard/boats");
  }

  useEffect(() => {
    if (loading !== null) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (boat !== null) setStatus(boat.status);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [boat]);

  return (
    <BaseVariantComp variants={generalPageMainContainerVariants}>
      <div className="row">
        <div className="col-sm-12">
          <BaseVariantComp variants={generalCardVariants}>
            <PageBackNavigator
              title={t("BackToBoatMenu")}
              onClick={backToBoatListing}
            />
          </BaseVariantComp>
        </div>
      </div>

      {/* boat details content */}
      <div className="row">
        <div className="col-sm-12">
          <BoatPhoto />
        </div>
      </div>

      <BoatDetailMain setStatus={setStatus} />

      <div className="row">
        <div className="col-sm-12">
          <BoatCrew />
        </div>
      </div>

      <div className="row">
        <div className="col-sm-12">
          <BaseVariantComp variants={generalCardVariants}>
            <BoatPackage />
          </BaseVariantComp>
        </div>
      </div>
      <div className="row">
        <div className="col-sm-12">
          <button
            className={`btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn ${
              status === "ACTIVE" && "kelaut-danger-btn"
            }`}
            style={{ width: "100%" }}
            onClick={() =>
              onUpdateBoatStatus({
                id,
                status: status === "ACTIVE" ? "INACTIVE" : "ACTIVE",
              })
            }
          >
            {status === "ACTIVE" ? t("DeactivateBotBtn") : t("ActivateBotBtn")}{" "}
            <i className="mdi mdi-delete-forever" />
          </button>
        </div>
      </div>
    </BaseVariantComp>
  );
}
