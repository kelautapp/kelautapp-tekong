import * as Yup from "yup";

export const validate = Yup.object({
  title: Yup.string().required("Please specity the boat package title"),
  description: Yup.string().required("Please specify the package description!"),
  price: Yup.number().required("Please specify the package price"),
});
