import * as Yup from "yup";
import i18n from "../../../../i18n";

export const validate = Yup.object({
  name: Yup.string().required(i18n.t("ThisFieldIsRequired")),
  idNumber: Yup.string().required(i18n.t("ThisFieldIsRequired")),
  dob: Yup.string(),
  photo: Yup.string()
    // .required("Photo is required")
    .transform((currentValue, originalValue) => {
      return originalValue === null ? "" : currentValue;
    }),
});
