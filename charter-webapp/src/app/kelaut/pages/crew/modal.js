import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import { FormBuilderComponent } from "../../../components";
import { Crew, UpdateCrew } from "../../../types/crew";
import { validate } from "./validate";
import i18n from "../../../../i18n";

const BoatCreationFormStructure = {
  formSplit: "one",
  forms: [
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("FullName"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "name",
        name: "name",
        placeholder: "Ellie Smith",
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("ICNumberLabel"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "idNumber",
        name: "idNumber",
        placeholder: "91xxxxxxxxxx",
      },
    },
    {
      type: "input-date",
      formStructure: {
        label: i18n.t("DateOfBirth"),
        type: "date",
        className: "form-control form-control-lg input-border",
        id: "dob",
        name: "dob",
        // placeholder: "Ellie Smith",
      },
    },
    {
      type: "image",
      formStructure: {
        label: i18n.t("CrewPhotoLabel"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "photo",
        name: "photo",
        placeholder: i18n.t("CrewPhotoPlaceholder"),
      },
    },
  ],
  formButton: [
    {
      type: "submit",
      name: i18n.t("RegisterButton"),
      className:
        "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
    },
  ],
};

export default function CrewModal({
  handleClose,
  show,
  onCreate = () => {},
  profileId,
  crewData = null,
  onUpdate = () => {},
}) {
  const [initialValue, setInitialValue] = useState({
    id: null,
    name: "",
    idNumber: "",
    dob: "",
    photo: "",
  });

  function onHandleSubmit(values) {
    if (values.id) {
      const crew = new UpdateCrew(values);
      onUpdate(crew);
    } else {
      const crew = new Crew(values);
      crew.setProfileId(profileId);
      onCreate(crew);
    }
  }

  useEffect(() => {
    if (crewData) {
      setInitialValue(crewData);
    } else {
      setInitialValue({
        id: null,
        name: "",
        idNumber: "",
        dob: "",
        photo: "",
      });
    }
  }, [crewData]);

  return (
    <Modal
      show={show}
      onHide={handleClose}
      className="crew-input-modal"
      backdrop="static"
      centered
    >
      <Modal.Header closeButton className="crew-modal-header">
        <Modal.Title>{i18n.t("AddNewCrewTitle")}</Modal.Title>
      </Modal.Header>

      <Modal.Body className="crew-modal-body">
        <FormBuilderComponent
          initialValue={initialValue}
          validation={validate}
          structures={BoatCreationFormStructure}
          onHandleSubmit={(values) => onHandleSubmit(values)}
        />
      </Modal.Body>
    </Modal>
  );
}
