import React, { useState, useEffect, useContext } from "react";
import { Doughnut } from "react-chartjs-2";
import KelautCard from "../../../components/kelaut-card";
import KelautSimpleTable from "../../../components/kelaut-simple-table";
import PlusButton from "../../../../assets/images/kelaut/plusbtnblue.svg";
import Modal from "./modal";
import KelautPage from "../../../components/kelaut-page";
import { useCrew } from "../../../hooks/crew";
import { LoaderContext, SetupContext } from "../../../hooks";
import SubMenu from "../../../components/sub-menu";
import CustomModal from "../../../components/modal";
import { UpdateCrewStatus } from "../../../types/crew";
import ToastrService from "../../../../services/toastr";
import { useTranslation } from "react-i18next";

const constructChart = (active = 0, inactive = 0) => {
  return {
    datasets: [
      {
        data: [active, inactive],
        backgroundColor: [
          "rgba(255, 99, 132, 0.5)",
          "rgba(54, 162, 235, 0.5)",
          "rgba(255, 206, 86, 0.5)",
          "rgba(75, 192, 192, 0.5)",
          "rgba(153, 102, 255, 0.5)",
          "rgba(255, 159, 64, 0.5)",
        ],
        borderColor: [
          "rgba(255,99,132,1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
          "rgba(75, 192, 192, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(255, 159, 64, 1)",
        ],
      },
    ],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: ["Active", "Inactive"],
  };
};

export const Index = () => {
  const [t] = useTranslation();
  const [show, setShow] = useState(false);
  const [showStatus, setShowStatus] = useState(false);
  const [changeData, setChangeData] = useState(null);
  const [changeAction, setChangeAction] = useState(null);

  const [active, setActive] = useState([]);
  const [inactive, setInActive] = useState([]);
  const [report, setReport] = useState(() => constructChart());
  const [editData, setEditData] = useState(null);

  const handleClose = () => {
    setShow(false);
  };

  const handleStatusClose = () => {
    setChangeData(null);
    setShowStatus(false);
  };

  const handleShow = () => {
    setEditData(null);
    setShow(true);
  };

  const { setupState } = useContext(SetupContext);
  const { setLoading } = useContext(LoaderContext);

  const {
    loading,
    onCreate,
    onGet,
    datas,
    onUpdate,
    onUpdateStatus,
    completeChangeStatus,
    complete,
    completeUpdate,
    error,
  } = useCrew();

  const AddButton = () => {
    return (
      <img
        src={PlusButton}
        alt=""
        className="kelaut-card-custom-action"
        onClick={handleShow}
      />
    );
  };

  function deactivateCrew(data) {
    setShowStatus(true);
    let crew = new UpdateCrewStatus(data);
    crew.status = "inactive";
    setChangeData(crew);
    setChangeAction("inactive");
  }

  function activateCrew(data) {
    setShowStatus(true);
    let crew = new UpdateCrewStatus(data);
    crew.status = "active";
    setChangeData(crew);
    setChangeAction("active");
  }

  function onConfirmChangeCrewStatus() {
    delete changeData["name"];
    onUpdateStatus(changeData);
  }

  function getListOfActions(data) {
    return [
      {
        title: "Edit",
        onClick: () => setEditData(data),
      },
      {
        title: "Deactivate",
        onClick: () => deactivateCrew(data),
      },
    ];
  }

  function getListOfActionsActivation(data) {
    return [
      {
        title: "Edit",
        onClick: () => setEditData(data),
      },
      {
        title: "Activate",
        onClick: () => activateCrew(data),
      },
    ];
  }

  useEffect(() => {
    if (loading !== null) {
      setLoading(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (datas.length > 0) {
      var tempData = {
        active: [],
        inactive: [],
      };
      datas.map((d) => {
        if (d.status.toLowerCase() === "active") tempData.active.push(d);
        else tempData.inactive.push(d);
        return d;
      });

      setActive(tempData.active);
      setInActive(tempData.inactive);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [datas]);

  useEffect(() => {
    if (setupState && setupState.user && setupState.user.profileId) {
      onGet(setupState.user.profileId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setupState]);

  useEffect(() => {
    setReport(constructChart(active.length, inactive.length));
  }, [active, inactive]);

  useEffect(() => {
    if (editData) {
      setShow(true);
    }
  }, [editData]);

  useEffect(() => {
    if (complete) {
      handleClose();
      ToastrService.success(t("SuccessCreateCrew"));
    }
  }, [complete, t]);

  useEffect(() => {
    if (completeUpdate) {
      handleClose();
      ToastrService.success(t("SuccessUpdateCrew"));
    }
  }, [completeUpdate, t]);

  useEffect(() => {
    if (completeChangeStatus) {
      handleStatusClose();
      ToastrService.success(t("SuccessChangeCrewStatus"));
    }
  }, [completeChangeStatus, t]);

  useEffect(() => {
    if (error) {
      ToastrService.error(t("UnexpectedError"));
    }
  }, [error, t]);

  const GetChangeCrewMessage = ({ name, status }) => {
    let str = t("ChangeCrewStatusQuestion")
      .replace(
        "{status}",
        status === "active" ? t("Activate") : t("Deactivate")
      )
      .replace("{crewname}", `<span class="name">${name.name}</span>?`);
    return <span dangerouslySetInnerHTML={{ __html: str }}></span>;
  };

  return (
    <KelautPage enableBackHeaderBtn>
      <KelautCard
        title={t("ActiveCrewTitle")}
        enableOptions
        customOptions={AddButton}
      >
        {active && active.length > 0 && (
          <KelautSimpleTable
            rows={active}
            active
            actionId="id"
            customOptions={SubMenu}
            customAction={getListOfActions}
          />
        )}
        {!active || (active.length < 1 && <p>{t("NoActiveCrewMsg")}</p>)}
      </KelautCard>
      <KelautCard title={t("InactiveCrewTitle")}>
        {inactive && inactive.length > 0 && (
          <KelautSimpleTable
            rows={inactive}
            actionId="id"
            customOptions={SubMenu}
            customAction={getListOfActionsActivation}
          />
        )}
        {!inactive || (inactive.length < 1 && <p>{t("NoInactiveCrewMsg")}</p>)}
      </KelautCard>
      <KelautCard title={t("CrewReportTitle")} enableOptions>
        <Doughnut data={report} options={DoughnutPieOptions} />
      </KelautCard>
      {setupState && setupState.user && setupState.user.profileId && (
        <Modal
          handleClose={handleClose}
          show={show}
          onCreate={onCreate}
          onUpdate={onUpdate}
          profileId={setupState.user.profileId}
          crewData={editData}
        />
      )}

      <CustomModal
        title={t("ChangeCrewStatusTitle")}
        handleClose={() => handleStatusClose()}
        show={showStatus}
      >
        {changeData && (
          <div className="delete-modal">
            <p className="message">
              <GetChangeCrewMessage name={changeData} status={changeAction} />
            </p>
            <div className="d-flex flex-row justify-content-between">
              <button
                className="btn font-weight-medium auth-form-btn btn-default"
                onClick={() => handleStatusClose()}
              >
                Cancel
              </button>
              <button
                className="btn font-weight-medium auth-form-btn kelaut-primary-btn"
                onClick={() => onConfirmChangeCrewStatus()}
              >
                Confirm
              </button>
            </div>
          </div>
        )}
      </CustomModal>
    </KelautPage>
  );
};

const DoughnutPieOptions = {
  responsive: true,
  animation: {
    animateScale: true,
    animateRotate: true,
  },
};

export default Index;
