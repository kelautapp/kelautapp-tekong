import React, { Suspense, lazy, useContext, useEffect } from "react";
import { Redirect, Switch, useLocation } from "react-router-dom";
import App from "./App";
import { SetupContext } from "./hooks";
import Spinner from "./shared/Spinner";
import { Auth } from "aws-amplify";
import { ProtectedRoute } from "./routes/guards";
import { IsAuthRoute } from "./routes/guards/isLogin";
import ResetPassword from "./user-pages/ResetPassword";
import { KelautGraphql } from "../services/aws/kelaut-graphql";
import { getProfileByEmail } from "../graphql/queries";
import { AnimatePresence } from "framer-motion";
import { Storage } from "aws-amplify";

const Login = lazy(() => import("./user-pages/Login"));
const Register = lazy(() => import("./user-pages/Register"));

async function generateUrl(key) {
  return await Storage.get(key, {
    level: "public", // defaults to `public`
  });
}

export const FullApp = () => {
  const location = useLocation();
  const { dispatch } = useContext(SetupContext);

  useEffect(() => {
    const checkAuthenticatedUser = async () => {
      try {
        const result = await Auth.currentAuthenticatedUser();
        const userProfile = await KelautGraphql(getProfileByEmail, {
          email: result.attributes.email,
        });

        const photoUrl =
          userProfile.data.getProfileByEmail.photo &&
          userProfile.data.getProfileByEmail.photo.toLowerCase() !== "null"
            ? await generateUrl(userProfile.data.getProfileByEmail.photo)
            : null;
        const backdropUrl =
          userProfile.data.getProfileByEmail.backdrop &&
          userProfile.data.getProfileByEmail.backdrop.toLowerCase() !== "null"
            ? await generateUrl(userProfile.data.getProfileByEmail.backdrop)
            : null;
        dispatch({
          type: "ADD_SETUP",
          payload: {
            ...result.attributes,
            profileId: userProfile.data.getProfileByEmail.id,
            name: userProfile.data.getProfileByEmail.name,
            profileDetails: {
              ...userProfile.data.getProfileByEmail,
              photoUrl,
              backdropUrl,
            },
          },
        });
      } catch (error) {
        dispatch({ type: "ADD_SETUP", payload: null });
      }
    };

    checkAuthenticatedUser();
  }, [dispatch]);
  return (
    <Suspense fallback={<Spinner />}>
      <AnimatePresence>
        <Switch location={location} key={location.key}>
          <ProtectedRoute path="/dashboard" component={App} />
          <IsAuthRoute path="/signin" component={Login} />
          <IsAuthRoute path="/signup" component={Register} />
          <IsAuthRoute path="/reset" component={ResetPassword} />
          <Redirect to="/dashboard" />
        </Switch>
      </AnimatePresence>
    </Suspense>
  );
};
