import * as Yup from "yup";
import i18n from "../../../i18n";

export const validate = Yup.object({
  name: Yup.string().required(i18n.t("RequireBoatName")),
  description: Yup.string().required(i18n.t("RequireBoatDesc")),
});
