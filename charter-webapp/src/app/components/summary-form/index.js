import React, { useState, useEffect, useCallback } from "react";
import { Formik, Form } from "formik";
import { validate } from "./config";
import { InputField } from "..";
import TextArea from "../forms-ui/TextArea";
import { GoogleMap, useJsApiLoader, Marker } from "@react-google-maps/api";
import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from "use-places-autocomplete";
import mapStyles from "./style";
import { UpdateBoatSummary } from "../../types/boat/boat";
import { useTranslation } from "react-i18next";
const libraries = ["places"];
const mapContainerStyle = {
  height: "80vh",
  width: "100%",
  borderRadius: 20,
};
const options = {
  styles: mapStyles,
  disableDefaultUI: true,
  zoomControl: true,
};
const center = {
  lat: 3.672859376882748,
  lng: 105.4674620305061,
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ initial, ...props }) {
  const [initialValue, setInitialValue] = useState({
    id: "",
    name: "",
    description: "",
  });

  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: `AIzaSyCpTdn2CnvMZZ4vJ0JgYyzSMsPm6B0dt9I`,
    libraries,
  });

  const [t] = useTranslation();

  const [marker, setMarker] = React.useState(null);
  const [location, setLocation] = React.useState(null);
  const mapRef = React.useRef();

  const onMapClick = useCallback(async (e) => {
    const place = await getGeocode({
      location: { lat: e.latLng.lat(), lng: e.latLng.lng() },
      //   types: ["administrative_area_level_4", "political"],
    });

    // eslint-disable-next-line array-callback-return
    const finalPlace = place.filter((p) => {
      let temp = p.types.join(", ");
      if (
        temp.indexOf("administrative_area_level") >= 0 &&
        temp.indexOf("political")
      ) {
        return p;
      }
    });
    if (finalPlace.length > 0) setLocation(finalPlace[0].formatted_address);
    setMarker({
      lat: e.latLng.lat(),
      lng: e.latLng.lng(),
      zoom: mapRef.current.zoom,
    });
  }, []);

  const onMapLoad = React.useCallback((map) => {
    mapRef.current = map;
  }, []);

  const panTo = React.useCallback(({ lat, lng }) => {
    mapRef.current.panTo({ lat, lng });
    mapRef.current.setZoom(16);
  }, []);

  function handleBoatSummit(values) {
    let boatSummary = new UpdateBoatSummary();
    boatSummary.setBasicData(values);
    boatSummary.setMarkerData(marker);
    boatSummary.setLocationData(location);
    if (props.hasOwnProperty("onHandleSubmit")) {
      props.onHandleSubmit(boatSummary);
    }
  }

  async function onSetMarker(lat, lng) {
    const place = await getGeocode({
      location: { lat: lat, lng: lng },
    });

    // eslint-disable-next-line array-callback-return
    const finalPlace = place.filter((p) => {
      let temp = p.types.join(", ");
      if (
        temp.indexOf("administrative_area_level") >= 0 &&
        temp.indexOf("political")
      ) {
        return p;
      }
    });
    if (finalPlace.length > 0) setLocation(finalPlace[0].formatted_address);
    setMarker({
      lat,
      lng,
      zoom: mapRef.current.zoom,
    });
  }

  useEffect(() => {
    if (isLoaded) {
      if (initial) {
        setInitialValue(initial);
        const { lat, lng, zoom, location } = initial;
        if (lat && lng && zoom) {
          setMarker({
            lat,
            lng,
            zoom,
          });
          //   mapRef.current.panTo({ lat, lng });
          //   mapRef.current.setZoom(zoom);
        }
        if (location) setLocation(location);
      } else {
        setInitialValue({
          id: "",
          name: "",
          description: "",
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoaded]);

  useEffect(() => {
    if (marker) {
      if (marker.lat && marker.lng && marker.zoom) {
        mapRef.current.panTo({ lat: marker.lat, lng: marker.lng });
        mapRef.current.setZoom(marker.zoom);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mapRef.current, marker]);
  return (
    <div>
      <Formik
        initialValues={initialValue}
        enableReinitialize={true}
        validationSchema={validate}
        onSubmit={(values) => {
          handleBoatSummit(values);
          //   if (props.hasOwnProperty("onHandleSubmit")) {
          //     props.onHandleSubmit(values);
          //   }
        }}
      >
        {({ values, setFieldValue }) => (
          <>
            <Form>
              <InputField
                label={t("FormLabelBoatName")}
                type="text"
                id="name"
                name="name"
                placeholder={t("FormPlaceholderBoatName")}
              />

              <TextArea
                label={t("FormLabelBoatDesc")}
                className="form-control form-control-lg input-border kelaut-textarea-ui"
                id="description"
                name="description"
                placeholder={t("FormPlaceholderBoatDesc")}
                rows={5}
                maxLength={200}
              />
              <div style={{ marginBottom: 10, marginTop: 10 }}>
                {isLoaded ? (
                  <div style={{ position: "relative" }}>
                    <Search panTo={panTo} onSetMarker={onSetMarker} />
                    <GoogleMap
                      id="map"
                      mapContainerStyle={mapContainerStyle}
                      zoom={5}
                      center={center}
                      options={options}
                      onClick={onMapClick}
                      onLoad={onMapLoad}
                    >
                      {marker && (
                        <Marker
                          key={`${marker.lat}-${marker.lng}`}
                          position={{ lat: marker.lat, lng: marker.lng }}
                        />
                      )}
                    </GoogleMap>
                  </div>
                ) : (
                  <div>Loading...</div>
                )}
              </div>
              <div className="d-flex justify-content-center">
                <button
                  type="submit"
                  className="btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn"
                >
                  {t("UpdateBoatSummaryBtn")}
                </button>
              </div>
            </Form>
          </>
        )}
      </Formik>
    </div>
  );
}

function Search({ panTo, onSetMarker = () => {} }) {
  const {
    ready,
    value,
    suggestions: { status, data },
    setValue,
    clearSuggestions,
  } = usePlacesAutocomplete({
    requestOptions: {
      location: { lat: () => center.lat, lng: () => center.lng },
      radius: 100 * 1000,
    },
  });

  const handleInput = (e) => {
    setValue(e.target.value);
  };

  const handleSelect = async (address) => {
    setValue(address, false);
    clearSuggestions();

    try {
      const results = await getGeocode({ address });
      const { lat, lng } = await getLatLng(results[0]);
      onSetMarker(lat, lng);
      panTo({ lat, lng });
    } catch (error) {
      console.log("😱 Error: ", error);
    }
  };
  const [t] = useTranslation();

  return (
    <div className="search">
      <input
        value={value}
        onChange={handleInput}
        disabled={!ready}
        placeholder={t("BoatDetailBoatSummaryModalFormLabelSearch")}
      />
      {status === "OK" && (
        <div className="suggestions">
          {data.map((d, i) => (
            <div key={i} onClick={() => handleSelect(d.description)}>
              {d.description}
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
