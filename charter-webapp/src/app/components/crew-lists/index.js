import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

export default function CrewList({ crewData = [] }) {
  const [crews, setCrews] = useState([]);
  const [t] = useTranslation();

  function getAvatarName(name) {
    if (name) {
      let temp = name.split(" ");
      let newName = temp.reduce((name, item, i) => {
        if (i < 2) {
          return (name += item.charAt(0));
        }
        return name;
      }, "");
      return newName;
    }
    return "N/A";
  }

  useEffect(() => {
    setCrews([...crewData]);
  }, [crewData]);

  return (
    <div className="kelaut-boat-crew-read">
      {crews.map((crew, i) => {
        return (
          <div className="border" key={i}>
            <div className="crew-profile">
              <div className="detail-container">
                {crew.photo === null && (
                  <div className="avatar">{getAvatarName(crew.name)}</div>
                )}
                {crew.photo && (
                  <img className="crew-image" src={crew.photo} alt="temp" />
                )}
                <div className="crew-detail">
                  <p className="name">{crew.name}</p>
                  <p className="crew-role">{t(crew.role)}</p>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}
