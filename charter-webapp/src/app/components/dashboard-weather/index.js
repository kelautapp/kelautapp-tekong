import React, { useState, useEffect } from "react";
import { useWeatherMap } from "../../hooks/weather";
import WeatherBox from "../weather-box";
import Skeleton from "react-loading-skeleton";

// eslint-disable-next-line import/no-anonymous-default-export
export default function () {
  const [lat, setLat] = useState(1.164);
  const [lng, setLng] = useState(101.898);
  const [am, setAM] = useState(null);
  const [pm, setPM] = useState(null);
  const { loading, datas, getCurrentWeatherByCoordinate } = useWeatherMap();

  function setPosition(position) {
    setLat(position.coords.latitude);
    setLng(position.coords.longitude);
  }

  useEffect(() => {
    // getCurrentWeatherByCoordinate
    navigator.geolocation.getCurrentPosition(setPosition);
  }, []);

  useEffect(() => {
    // getCurrentWeatherByCoordinate
    getCurrentWeatherByCoordinate(lat, lng, 8);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lat, lng]);

  useEffect(() => {
    if (datas) {
      if (datas.length > 0) {
        // eslint-disable-next-line array-callback-return
        datas.forEach((d) => {
          if (d.dt_txt.indexOf("09:00:00") >= 0) {
            setAM(d);
          } else if (d.dt_txt.indexOf("15:00:00") >= 0) {
            setPM(d);
          }
        });
      }
    }
  }, [datas]);

  return (
    <div className="weather-box-container ">
      {loading ? (
        <Skeleton height={200} />
      ) : (
        <>
          {am && (
            <WeatherBox
              time="AM"
              temperature={Math.round(am.main.temp, 2)}
              status={am.weather[0].main.toLowerCase()}
            />
          )}
          {pm && (
            <WeatherBox
              time="PM"
              temperature={Math.round(pm.main.temp, 2)}
              status={pm.weather[0].main.toLowerCase()}
            />
          )}
        </>
      )}

      {/* <WeatherBox time="PM" temperature={29} status="raining" /> */}
    </div>
  );
}
