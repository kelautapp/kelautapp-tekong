import React, { useState, useEffect, useContext } from "react";
import Modal from "../modal";
import PhotoUpload from "../photo-upload";
import SimpleCard from "../simple-card";
import { useParams } from "react-router-dom";
import { useBoatPhoto } from "../../hooks/boat-management/boat-photo";
import ImageGalleryComponent from "../image-gallery";
import Skeleton from "react-loading-skeleton";
import { LoaderContext } from "../../hooks";
import ToastrService from "../../../services/toastr";
import { useTranslation } from "react-i18next";

const AddButton = ({ onClick }) => {
  return <i className="mdi mdi-plus-circle add-button" onClick={onClick} />;
};

export default function BoatPhoto() {
  let { id } = useParams();
  const { setLoading } = useContext(LoaderContext);
  const [show, setShow] = useState(false);
  const [showGallery, setShowGallery] = useState(false);
  const [formattedImageData, setFormattedImageData] = useState([]);
  const {
    loading,
    onGet,
    imageData,
    onDelete,
    onUpload,
    completeUpload,
    completeDelete,
  } = useBoatPhoto();
  const [loadSkeleton, setLoadSkeleton] = useState(null);
  const [t] = useTranslation();

  useEffect(() => {
    if (id) {
      setLoadSkeleton(true);
      setTimeout(() => {
        onGet(id);
      }, 2000);
      // onGet(id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (imageData.length > 0) {
      let tempImageData = imageData.reduce((newImage, img) => {
        newImage.push({
          original: img.url,
          thumbnail: img.url,
        });
        return newImage;
      }, []);

      setFormattedImageData(tempImageData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [imageData]);

  useEffect(() => {
    if (loading !== null) {
      if (show) {
        setLoading(loading);
      }
      setLoadSkeleton(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (completeUpload) {
      ToastrService.success(t("SuccessUpdateBotPhotoMsg"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeUpload, t]);

  useEffect(() => {
    if (completeDelete) {
      ToastrService.success(t("SuccessDeleteBotPhotoMsg"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeDelete, t]);

  return (
    <SimpleCard
      title={t("BoatPhotoTitle")}
      enableOptions
      customOptions={() => <AddButton onClick={() => setShow(true)} />}
    >
      {loadSkeleton ? (
        <Skeleton height={200} />
      ) : (
        <div className="boat-photo-list" onClick={() => setShowGallery(true)}>
          <div className="container">
            {imageData.length === 0 && <p>{t("NoBoatPhotoMsg")}</p>}
            {imageData.length > 0 && (
              <div className="first" key={imageData[0].id}>
                <img src={imageData[0].url} alt={imageData[0].name} />
              </div>
            )}
            {imageData.length > 1 && (
              <div className="another">
                {/* eslint-disable-next-line array-callback-return */}
                {imageData.map((image, i) => {
                  if (i > 0 && i <= 3) {
                    return (
                      <div className="image-container" key={image.id}>
                        <img src={image.url} alt={imageData.name} />
                        {i === 3 && imageData.length - (i + 1) !== 0 && (
                          <div className="indicator">
                            {imageData.length - (i + 1)}+
                          </div>
                        )}
                      </div>
                      // </div>
                    );
                  }
                })}
              </div>
            )}
          </div>
        </div>
      )}

      <Modal
        title={t("BoatGalleryTitle")}
        handleClose={() => setShowGallery(false)}
        show={showGallery}
      >
        <ImageGalleryComponent boatPhoto={formattedImageData} />
      </Modal>
      <Modal
        title={t("AddBoatPhoto")}
        handleClose={() => setShow(false)}
        show={show}
      >
        <PhotoUpload
          boatId={id}
          boatPhoto={imageData}
          handleUpload={onUpload}
          handleDelete={onDelete}
        />
      </Modal>
    </SimpleCard>
  );
}
