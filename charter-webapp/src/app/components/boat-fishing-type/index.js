import React, { useContext, useState, useEffect } from "react";
import BaseVariantComp from "../../kelaut/animations/framer-variants/components/base-variant-comp";
import { generalCardVariants } from "../../kelaut/animations/framer-variants/general-page";
import BulletList from "../bullet-list";
import KelautCard from "../kelaut-card";
import Modal from "../modal";
// import data from "./data";
import SubMenu from "../sub-menu";
import { LoaderContext } from "../../hooks";
import { useBoatFishingType } from "../../hooks/boat-management/boat-fishing-type";
import FishingTypeForm from "../fishing-type-form";
import ToastrService from "../../../services/toastr";
import { useTranslation } from "react-i18next";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ id, fishingType = null }) {
  const [show, setShow] = useState(false);
  const [data, setData] = useState({ fishingType: [] });
  const { setLoading } = useContext(LoaderContext);
  const [t] = useTranslation();

  const {
    loading,
    data: fishingTypeData,
    complete,
    onCreate,
    completeUpdate,
  } = useBoatFishingType();

  function openModal() {
    setShow(true);
  }

  function handleCloseModal() {
    setShow(false);
  }

  function handleAction(mode) {
    if (mode === "edit") {
      openModal(true);
    }
  }

  function getListOfActions() {
    return [
      {
        title: "Edit",
        onClick: () => handleAction("edit"),
      },
    ];
  }

  function onHandleSubmit(fishingType) {
    let temp = {
      id: id,
      fishingType: JSON.stringify(fishingType),
    };
    onCreate(temp);
  }

  useEffect(() => {
    if (loading !== null) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (complete !== null || completeUpdate !== null) {
      setShow(false);
      ToastrService.success(t("SuccessUpdateFT"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [complete, completeUpdate, t]);

  useEffect(() => {
    if (fishingType) {
      let parseAmenities = JSON.parse(fishingType);
      setData(parseAmenities);
    }
  }, [fishingType]);

  useEffect(() => {
    if (fishingTypeData) {
      if (fishingTypeData.fishingType) {
        setData(JSON.parse(fishingTypeData.fishingType));
      }
    }
  }, [fishingTypeData]);

  return (
    <BaseVariantComp variants={generalCardVariants}>
      <KelautCard
        title={t("FTTitle")}
        enableOptions
        customOptions={() => <SubMenu lists={getListOfActions()} />}
      >
        {data.fishingType.length === 0 && <p>{t("NoavailableFT")}</p>}
        {data.fishingType.map((d, i) => (
          <BulletList key={i} detail={d.name} />
        ))}
        <Modal
          title={t("EditFTModal")}
          handleClose={() => handleCloseModal()}
          show={show}
        >
          <FishingTypeForm onHandleSubmit={onHandleSubmit} initial={data} />
        </Modal>
      </KelautCard>
    </BaseVariantComp>
  );
}
