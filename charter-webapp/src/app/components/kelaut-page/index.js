import React from "react";
import BaseVariantComp from "../../kelaut/animations/framer-variants/components/base-variant-comp";
import PageBackNavigator from "../../components/page-back-navigator";
import { generalCardVariants } from "../../kelaut/animations/framer-variants/general-page";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

export const Index = (props) => {
  const [t] = useTranslation();
  const { enableBackHeaderBtn } = props;
  const history = useHistory();
  function backToDashboard() {
    history.push("/dashboard");
  }
  return (
    <>
      {enableBackHeaderBtn && (
        <div className="row">
          <div className="col-sm-12">
            <BaseVariantComp variants={generalCardVariants}>
              <PageBackNavigator
                title={t("BackToDashboard")}
                onClick={backToDashboard}
              />
            </BaseVariantComp>
          </div>
        </div>
      )}
      {props.children}
    </>
  );
};

export default Index;
