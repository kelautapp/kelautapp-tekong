import React, { useState, useEffect } from "react";
import { Formik, Form, FieldArray } from "formik";
import * as Yup from "yup";
import { InputField } from "..";
import PlusButton from "../../../assets/images/kelaut/plusbtnblue.svg";
import { useTranslation } from "react-i18next";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ initial, ...props }) {
  const [initialValue, setInitialValue] = useState({
    fishingType: [
      {
        name: "",
      },
    ],
  });

  const [t] = useTranslation();

  const validationSchema = Yup.object().shape({
    fishingType: Yup.array()
      .of(
        Yup.object().shape({
          name: Yup.string().required(t("ThisFieldIsRequired")),
        })
      )
      .min(1, "at least 1"),
  });

  function handleAddItem(values) {
    let temp = { ...values };
    temp.fishingType = [...temp.fishingType, { name: "" }];
    setInitialValue(temp);
  }

  function removeItem(index, values) {
    let temp = { ...values };
    temp.fishingType.splice(index, 1);
    setInitialValue(temp);
  }

  const AddButton = ({ values }) => {
    return (
      <img
        src={PlusButton}
        alt=""
        className="kelaut-card-custom-action"
        onClick={() => handleAddItem(values)}
      />
    );
  };

  useEffect(() => {
    if (initial) {
      setInitialValue(initial);
    }
  }, [initial]);

  return (
    <Formik
      initialValues={initialValue}
      enableReinitialize={true}
      validationSchema={validationSchema}
      onSubmit={(values) => {
        if (props.hasOwnProperty("onHandleSubmit")) {
          props.onHandleSubmit(values);
        }
      }}
    >
      {({ values, setFieldValue }) => (
        <>
          <Form>
            <div className="d-flex justify-content-end">
              <AddButton type="button" values={values} />
            </div>
            <div className="row">
              <FieldArray name="fishingType">
                {() =>
                  values.fishingType.map((fishingType, i) => {
                    return (
                      <div className="col-sm-12" key={i}>
                        <InputField
                          label={t("Name")}
                          type="text"
                          id={`fishingType.${i}.name`}
                          name={`fishingType.${i}.name`}
                          // placeholder="Fishing Technique Name"
                          value={fishingType.name}
                        />
                        {values.fishingType.length > 1 && (
                          <div className="d-flex justify-content-end">
                            <p
                              className="remove-item"
                              onClick={() => removeItem(i, values)}
                            >
                              {t("Remove")}
                            </p>
                          </div>
                        )}
                      </div>
                    );
                  })
                }
              </FieldArray>
              <div className="col-sm-12 kelaut-form-div">
                <div className="mt-3">
                  <div className="float-right">
                    <button
                      type="submit"
                      className="btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn"
                    >
                      {t("UpdateFTBtn")}
                    </button>
                  </div>
                  <div style={{ clear: "both" }}></div>
                </div>
              </div>
            </div>
          </Form>
        </>
      )}
    </Formik>
  );
}
