import React, { useState, useEffect } from "react";
import { FormBuilderComponent } from "..";
import * as moment from "moment";
import { validate } from "./validate";
import i18n from "../../../i18n";
const ProfileFormStructure = {
  formSplit: "one",
  forms: [
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("FullName"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "name",
        name: "name",
        placeholder: "Ellie Smith",
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("CompanyName"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "companyName",
        name: "companyName",
        placeholder: i18n.t("CompanyName"),
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("MobilePhone"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "phone",
        name: "phone",
        // placeholder: "Ellie Smith",
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("Nationality"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "nationality",
        name: "nationality",
        // placeholder: "Ellie Smith",
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("Language"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "language",
        name: "language",
        // placeholder: "Ellie Smith",
      },
    },
    {
      type: "input-date",
      formStructure: {
        label: i18n.t("DateOfBirth"),
        type: "date",
        className: "form-control form-control-lg input-border",
        id: "birthDate",
        name: "birthDate",
        // placeholder: "Ellie Smith",
      },
    },
    {
      type: "input-date",
      formStructure: {
        label: i18n.t("StartCareerDate"),
        type: "date",
        className: "form-control form-control-lg input-border",
        id: "startCareer",
        name: "startCareer",
        // placeholder: "Ellie Smith",
      },
    },
    {
      type: "image",
      formStructure: {
        label: i18n.t("ProfilePhoto"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "photo",
        name: "photo",
        // field: ""
        placeholder: "Upload Profile Photo",
      },
    },
    {
      type: "image",
      formStructure: {
        label: i18n.t("BackdropPhoto"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "backdrop",
        name: "backdrop",
        placeholder: "Upload Backdrop Photo",
        full: true,
        // placeholder: "Ellie Smith",
      },
    },
  ],
  formButton: [
    {
      type: "submit",
      name: i18n.t("UpdateProfile"),
      className:
        "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
    },
  ],
};

const defaultValue = {
  id: null,
  name: "",
  companyName: "",
  phone: "",
  nationality: "",
  language: "",
  startCareer: "",
  photo: "",
  backdrop: "",
  birthDate: "",
};

export default function UpdateProfileForm({
  profileData = null,
  handleSubmit = () => {},
}) {
  const [initialValue, setInitialValue] = useState(defaultValue);

  function onHandleSubmit(data) {
    handleSubmit(data);
  }

  useEffect(() => {
    if (profileData) {
      setInitialValue({
        ...profileData,
        birthDate: moment(profileData.birthDate, "DD-MM-YYYY").format(
          "YYYY-MM-DD"
        ),
        startCareer: moment(profileData.startCareer).format("YYYY-MM-DD"),
        photo: profileData.photo,
        backdrop: profileData.backdrop,
      });
    }
  }, [profileData]);
  return (
    <FormBuilderComponent
      initialValue={initialValue}
      validation={validate}
      structures={ProfileFormStructure}
      onHandleSubmit={(data) => onHandleSubmit(data)}
    />
  );
}
