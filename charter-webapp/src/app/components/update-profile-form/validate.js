import * as Yup from "yup";

const phoneRegExp = /^(\+?6?01)[0-46-9]-*[0-9]{7,8}$/;
export const validate = Yup.object({
  //username: Yup.string().required("Username is required"),
  name: Yup.string().required("Name is required!"),
  companyName: Yup.string().required("Company Name is required!"),
  phone: Yup.string()
    .required("This field is required!")
    .matches(phoneRegExp, "Phone number is not valid"),
  nationality: Yup.string().required("Nationality is required!"),
  language: Yup.string().required("Language is required!"),
  birthDate: Yup.string().required("Birth Date is required!"),
});
