import React from "react";
import SuccessIcon from "../../../assets/images/kelaut/available.svg";
import StopIcon from "../../../assets/images/kelaut/notavailable.svg";

function CustomOptions(props) {
  if (props.CustomOption) {
    return <props.CustomOption lists={props.action} />;
  }
  return (
    <p className="text-muted mb-1">
      {" "}
      <i className="mdi mdi-dots-horizontal"></i>
    </p>
  );
}

export const index = ({
  columns,
  rows,
  active,
  clickAction = () => {},
  actionId = null,
  customOptions = null,
  customAction = () => {},
  maxLength = 30,
}) => {
  function cutLongName(name) {
    if (name.length > maxLength) {
      return name.substr(0, maxLength) + "..";
    }
    return name;
  }
  return (
    <div className="table-responsive table-simple-kelaut">
      <table className="table">
        {columns && (
          <thead>
            <tr>
              {columns.map((col, key) => {
                return <td key={key}>{col}</td>;
              })}
            </tr>
          </thead>
        )}
        <tbody>
          {rows &&
            rows.map((row, key) => {
              return (
                <tr
                  className="simple-tr"
                  key={key}
                  onClick={() =>
                    clickAction(actionId ? row[actionId] : row.name)
                  }
                >
                  <td className="status-col">
                    {active && (
                      <img
                        src={SuccessIcon}
                        className="avilable-icon"
                        alt="success icon"
                      />
                    )}
                    {!active && (
                      <img
                        src={StopIcon}
                        className="avilable-icon"
                        alt="available icon"
                      />
                    )}
                  </td>
                  <td className="boat-name">{cutLongName(row.name)}</td>
                  <td className="action-col">
                    {/* <i className="mdi mdi-dots-horizontal"></'i>
                     */}
                    {customOptions && (
                      <ul className="custom-action">
                        <li>
                          <CustomOptions
                            CustomOption={customOptions}
                            action={customAction(row)}
                          />
                        </li>
                      </ul>
                    )}
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
};
export default index;
