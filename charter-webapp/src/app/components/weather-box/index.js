import React from "react";
import WeatherIcon from "../weather-icon";
import CloudDay from "../../../assets/images/weather/cloudy.png";
// import Raining from "../../svgs/weather/raining";
import Raining from "../../../assets/images/weather/rain.png";
import Thunderstorm from "../../../assets/images/weather/heavy-rain.png";
import Clear from "../../../assets/images/weather/sunny.png";

const WeatherStatus = {
  clouds: { comp: <WeatherIcon path={CloudDay} />, desc: "Cold & Sunshine" },
  rain: { comp: <WeatherIcon path={Raining} />, desc: "Raining" },
  thunderstorm: {
    comp: <WeatherIcon path={Thunderstorm} />,
    desc: "Heavy Rain",
  },
  clear: { comp: <WeatherIcon path={Clear} />, desc: "Sunny Day" },
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ time, temperature, status }) {
  const className =
    status === "raining" ? "weather-box raining" : "weather-box";

  function formattedStatusToKelautStatus(status) {
    switch (status) {
      case "thunderstorm":
        return "thunderstorm";
      case "drizzle":
      case "rain":
        return "rain";
      case "clear":
        return "clear";
      default:
        return "clouds";
    }
  }

  return (
    <div className={className}>
      <p className="weather-time">{time}</p>
      <div className="weather-content">
        {WeatherStatus[formattedStatusToKelautStatus(status)].comp}
        <div className="weather-status-box">
          <div className="weather-cel">{temperature} &#8451;</div>
          <p className="weather-status">
            {WeatherStatus[formattedStatusToKelautStatus(status)].desc}
          </p>
        </div>
      </div>
    </div>
  );
}
