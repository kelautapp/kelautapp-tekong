import React, { useState, useContext, useEffect } from "react";
import { LoaderContext } from "../../hooks";
import useSignUp from "../../hooks/authentication/signup";
import { toast } from "react-toastify";
import { Alert } from "react-bootstrap";
import { Formik, Form } from "formik";
import { validate } from "./validation";
import { InputField } from "../forms-ui/Input";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

export function ConfirmForm({ username, onCompleteSignup }) {
  const [t] = useTranslation();
  const { setLoading } = useContext(LoaderContext);
  const intervalDuration = 60;
  const [duration, setCurrentDuration] = useState(intervalDuration);

  const [runInterval, setRunInterval] = useState(null);
  const [initalValues, setInitialValues] = useState({
    username: "",
    code: "",
  });

  const {
    loading,
    completeSignUp,
    errors,
    onConfirmRegistrationProcess,
    onResendConfirmation,
    sucessResend,
  } = useSignUp();

  const runResendConfirmation = () => {
    setCurrentDuration((prevState) => prevState - 1);
  };

  const handleResend = async (evt) => {
    evt.preventDefault();
    await onResendConfirmation(username);
  };

  const confirmRegistration = async (values) => {
    try {
      await onConfirmRegistrationProcess({ ...values });
    } catch (error) {
      // setErrorMessage("Invalid code!");
    }
  };

  useEffect(() => {
    if (sucessResend) {
      setRunInterval(true);
      toast("Successfully resend code", { type: "success" });
    }
  }, [sucessResend]);

  useEffect(() => {
    setRunInterval(true);
  }, []);

  useEffect(() => {
    let intervalHolder = null;
    if (runInterval) {
      intervalHolder = setInterval(() => {
        runResendConfirmation();
      }, 1000);
    } else if (runInterval === false) {
      clearInterval(intervalHolder);
      setCurrentDuration(intervalDuration);
    }

    return () => clearInterval(intervalHolder);
  }, [runInterval]);

  useEffect(() => {
    if (duration <= 0) {
      setRunInterval(false);
    }
  }, [duration]);

  useEffect(() => {
    if (username) {
      setInitialValues({
        username: username,
        code: "",
      });
    }
  }, [username]);

  useEffect(() => {
    if (loading !== null) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (completeSignUp) {
      toast("Successfully register account", { type: "success" });
      // history.push("/signin");
      onCompleteSignup();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeSignUp]);

  useEffect(() => {
    if (errors !== null) {
      toast(errors, { type: "error" });
    }
  }, [errors]);
  return (
    <Formik
      initialValues={initalValues}
      enableReinitialize={true}
      validationSchema={validate}
      onSubmit={async (values) => {
        await confirmRegistration(values);
      }}
    >
      <Form className="confirmation-form">
        {/* <div> */}
        <div className="pt-3 m-4">
          <InputField
            label={t("Username")}
            type="email"
            className="form-control form-control-lg input-border"
            id="username"
            name="username"
            placeholder="Username"
            disabled
          />

          <InputField
            label={t("VerificationCode")}
            type="text"
            className="form-control form-control-lg input-border"
            id="code"
            name="code"
            placeholder={t("VerificationCodePlaceholder")}
          />
        </div>
        {errors && <Alert variant="danger">{errors}</Alert>}

        <div className="text-center mt-4 font-weight-light m-4">
          <p>
            {runInterval === false ? (
              <Link onClick={handleResend} className="text-primary">
                {t("ResendConfirmCode")}
              </Link>
            ) : (
              t("WaitResendCode").replace("{duration}", duration)
            )}
          </p>
        </div>
        <div className="mt-3">
          <center>
            <button
              type="submit"
              className="btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn"
            >
              {t("VerifyCode")}
            </button>
          </center>
        </div>
      </Form>
    </Formik>
  );
}
