import * as Yup from "yup";
import i18n from "../../../i18n";

export const validate = Yup.object({
  username: Yup.string().required("Username is required"),
  code: Yup.string().required(i18n.t("VerificationCodeRequire")),
});
