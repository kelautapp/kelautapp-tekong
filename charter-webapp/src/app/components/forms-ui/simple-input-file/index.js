import React, { useRef, useState } from "react";
import { ErrorMessage, useField } from "formik";
import { Link } from "react-router-dom";

export const InputFileField = ({ label, ...props }) => {
  const [field, meta, helpers] = useField(props);

  const fileRef = useRef();
  const [filePreview, setFilePreview] = useState(null);

  const clearFile = () => {
    fileRef.current.value = null;
    setFilePreview(null);
    helpers.setValue(null);
  };

  const onChangeFile = (event) => {
    let file = fileRef.current.files[0];
    helpers.setValue(fileRef.current.files[0]);
    let reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onloadend = () => {
      setFilePreview({
        imgSrc: [reader.result],
      });
    };
  };

  return (
    <div className="form-group search-field">
      <label htmlFor={field.name}>{label}</label>
      <input
        type="file"
        className={`form-control form-control-lg input-border ${
          meta.touched && meta.error && "invalid"
        }`}
        {...field}
        name={props.name}
        value={props.value}
        ref={fileRef}
        onChange={onChangeFile}
        //   props.onChange();
        //   onChangeFile();
        // }}
      />
      {filePreview && filePreview.imgSrc && (
        <img src={filePreview.imgSrc} alt="Uploaded img preview" />
      )}
      {fileRef &&
        fileRef.current &&
        fileRef.current.value &&
        fileRef.current.value !== "" && (
          <Link onClick={() => clearFile()}>Remove</Link>
        )}
      <ErrorMessage component="div" name={field.name} className="error" />
    </div>
  );
};
