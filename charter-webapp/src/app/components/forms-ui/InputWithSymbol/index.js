import React from "react";
import { ErrorMessage, useField } from "formik";
import { Form } from "react-bootstrap";

export const InputFieldWithSymbol = ({ label, ...props }) => {
  const [field] = useField(props);
  return (
    <Form.Group className="d-flex search-field with-symbol">
      <Form.Label>{label}</Form.Label>
      <div className="custom-input">
        {/* <input
        className={`form-control form-control-lg input-border h-auto ${
          meta.touched && meta.error && "invalid"
        }`}
        {...field}
        {...props}
        autoComplete="off"
      /> */}
        <span>
          <i className={props.icon} />
        </span>
        <Form.Control {...field} {...props} autoComplete="off" />
      </div>

      <ErrorMessage component="div" name={field.name} className="error" />
    </Form.Group>
  );
};
