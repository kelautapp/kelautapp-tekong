import React from "react";
import Select from "react-select";
import { ErrorMessage, useField } from "formik";

function SelectField({ label, ...props }) {
  const [field, meta, helpers] = useField(props);
  function onHandleSelectChange(event) {
    helpers.setTouched();
    helpers.setValue(event.value);
  }

  function onHandleBlur() {
    helpers.setTouched();
  }

  return (
    <div>
      <label className="kelaut-select" htmlFor={field.name}>
        {label}
      </label>
      <Select
        className={`kelaut-select-ui ${
          meta.touched && meta.error && "invalid"
        }`}
        {...field}
        {...props}
        onChange={onHandleSelectChange}
        onBlur={onHandleBlur}
        value={
          props.options
            ? // eslint-disable-next-line eqeqeq
              props.options.find((option) => option.value == field.value)
            : ""
        }
      />
      <ErrorMessage component="div" name={field.name} className="error" />
    </div>
  );
}

export default SelectField;
