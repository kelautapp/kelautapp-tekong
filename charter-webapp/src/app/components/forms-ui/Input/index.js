import React from "react";
import { ErrorMessage, useField } from "formik";

export const InputField = ({ label, grey = null, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <div className={"form-group search-field"}>
      <label htmlFor={field.name}>{label}</label>
      <div className="default-custom-input">
        <input
          className={`form-control form-control-lg ${
            meta.touched && meta.error && "invalid"
          } ${grey ? "kelaut-grey" : ""}`}
          {...field}
          {...props}
          autoComplete="off"
        />
      </div>
      <ErrorMessage component="div" name={field.name} className="error" />
    </div>
  );
};
