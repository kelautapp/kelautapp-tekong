import React, { useState, useEffect, useRef } from "react";
import { Calendar } from "react-date-range";
import { ErrorMessage, useField } from "formik";
import * as moment from "moment";
function useOutsideAlerter(ref) {
  const [count, setCount] = useState(0);
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        // alert("You clicked outside of me!");
        setCount((prevCount) => prevCount + 1);
      }
    }

    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);

  return { count };
}

function DatePicker({ label, ...props }) {
  const [field, meta, helpers] = useField(props);
  const [date, setDate] = useState(moment().toDate());
  const [display, setDisplay] = useState(null);
  const [showCalendar, setShowCalendar] = useState(false);
  const wrapperRef = useRef(null);
  const { count } = useOutsideAlerter(wrapperRef);

  function handleChange(event) {
    helpers.setValue(moment(event).format("YYYY-MM-DD"));
    setDate(event);
    setShowCalendar(false);
  }

  function handleDummyInput() {
    helpers.setTouched();
    setShowCalendar((prevState) => !prevState);
  }

  useEffect(() => {
    if (meta.value) {
      let convertoMoment = moment(meta.value, "YYYY-MM-DD");
      if (convertoMoment.isValid()) {
        setDisplay(convertoMoment.format("DD-MM-YYYY"));
        setDate(convertoMoment.toDate());
      }
    }
  }, [meta.value]);

  useEffect(() => {
    if (count > 0) {
      setShowCalendar(false);
    }
  }, [count]);

  return (
    <div className="kelaut-date-picker">
      <label className="kelaut-select" htmlFor={field.name}>
        {label}
      </label>
      <div className="dummy-input" onClick={handleDummyInput}>
        {display}
        <i className="mdi mdi-calendar-blank" />
      </div>
      <div className="calendar">
        <div className="calendar-container" ref={wrapperRef}>
          {showCalendar && <Calendar date={date} onChange={handleChange} />}
        </div>
      </div>
      <ErrorMessage component="div" name={field.name} className="error" />
    </div>
  );
}

export default DatePicker;
