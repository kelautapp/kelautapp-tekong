import { ErrorMessage, useField } from "formik";
import React, { useState, useEffect } from "react";
import ImageUploading from "react-images-uploading";
import { uploadFile } from "../../../../services/aws/s3/upload";
import { v4 as uuidv4 } from "uuid";
import { Storage } from "aws-amplify";

export default function InputFile({
  label,
  placeholder,
  full = false,
  ...props
}) {
  const [field, meta, helpers] = useField(props);
  const [imageUpload, setImageUpload] = useState([]);
  const [imageLoading, setImageLoading] = useState(false);

  function generateFileName(name) {
    return `${uuidv4()}-${name}`;
  }
  async function uploadImageToS3(data) {
    const result = await uploadFile(data, generateFileName(data.name));
    return result;
  }

  async function generateUrl(key) {
    return await Storage.get(key, {
      level: "public", // defaults to `public`
    });
  }

  const onChange = async (imageList, addUpdateIndex) => {
    // data for submit
    setImageLoading(true);
    const result = await uploadImageToS3(imageList[0].file);
    let file = await generateUrl(result.key);
    setImageUpload([{ data_url: file, key: result.key }]);
    setImageLoading(false);
  };

  async function init() {
    setImageLoading(true);
    if (meta.value && meta.value.toLowerCase() !== "null") {
      let file = await generateUrl(meta.value);
      setImageUpload([{ data_url: file, key: meta.value }]);
    }

    setImageLoading(false);
  }

  useEffect(() => {
    if (imageUpload.length > 0) {
      helpers.setValue(imageUpload[0]["key"]);
    } else {
      helpers.setValue(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [imageUpload]);

  useEffect(() => {
    if (meta && meta.value && meta.value !== "") {
      init();
    } else {
      setImageUpload([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [meta.value]);

  return (
    <>
      <ImageUploading
        value={imageUpload}
        onChange={onChange}
        dataURLKey="data_url"
        acceptType={["jpg", "png", "jpeg"]}
      >
        {({
          imageList,
          onImageUpload,
          onImageRemoveAll,
          onImageUpdate,
          onImageRemove,
          isDragging,
          dragProps,
        }) => (
          // write your building UI

          <div>
            <p className="form-label">{label}</p>
            {imageLoading ? (
              <p>Loading....</p>
            ) : (
              imageList.map((image, index) => (
                <div
                  key={index}
                  className={`image-item ${full === false ? "wrap" : null}`}
                  onClick={() => onImageUpdate(index)}
                >
                  <div className="image-wrap">
                    <img
                      className={full ? "full-width-img" : null}
                      src={image["data_url"]}
                      alt=""
                    />
                    <div className="image-change-btn">
                      <button type="button">
                        <i className="mdi mdi-camera" />
                      </button>
                      {/* <button onClick={() => onImageRemove(index)}>Remove</button> */}
                    </div>
                  </div>
                </div>
              ))
            )}
            {imageList.length <= 0 && (
              <div className="kelaut-upload" onClick={onImageUpload}>
                <p>
                  <i className="mdi mdi-plus" />
                  {placeholder}
                </p>
              </div>
            )}
          </div>
        )}
      </ImageUploading>
      <ErrorMessage component="div" name={field.name} className="error" />
    </>
  );
}
