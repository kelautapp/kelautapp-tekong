import React from "react";
import { ErrorMessage, useField } from "formik";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ label, ...props }) {
  const [field, meta, helpers] = useField(props);
  function onHandleSelectChange(event) {
    event.persist();
    helpers.setTouched();
    helpers.setValue(event.target.value);
  }

  function onHandleBlur() {
    helpers.setTouched();
  }

  function checkDifferentWithMax() {
    if (props && meta) {
      let diff = props.maxLength - meta.value.length;
      let sentence = `${diff} ${diff === 1 ? "character" : "characters"} left`;

      return sentence;
    }
  }
  return (
    <div>
      <label className="kelaut-textarea" htmlFor={field.name}>
        {label}
      </label>
      <textarea
        className={`kelaut-textarea-ui ${
          meta.touched && meta.error && "invalid"
        }`}
        {...field}
        {...props}
        onChange={onHandleSelectChange}
        onBlur={onHandleBlur}
        value={field.value}
      />
      {props.hasOwnProperty("maxLength") && (
        <p className="characters-left">{checkDifferentWithMax()}</p>
      )}
      <ErrorMessage component="div" name={field.name} className="error" />
    </div>
  );
}
