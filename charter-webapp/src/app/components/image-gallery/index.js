import React from "react";
import ImageGallery from "react-image-gallery";

export default function ImageGalleryComponent({ boatPhoto }) {
  return <ImageGallery items={boatPhoto} />;
}
