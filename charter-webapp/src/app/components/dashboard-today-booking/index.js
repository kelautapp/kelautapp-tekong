import React, { useState } from "react";
import BookingDetails from "../booking-details";
import _ from "lodash";
import { useTranslation } from "react-i18next";
// eslint-disable-next-line import/no-anonymous-default-export
export default function () {
  // const [bookings] = useState([
  //   {
  //     id: 1,
  //     bookingNumber: "BNF2002 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     image: Boat,
  //   },
  //   {
  //     id: 2,
  //     bookingNumber: "BNF2003 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     image: Boat,
  //   },
  //   {
  //     id: 3,
  //     bookingNumber: "BNF2004 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     image: Boat,
  //   },
  // ]);
  const [t] = useTranslation();
  const [bookings] = useState([]);
  return (
    <div>
      {bookings.map((booking) => (
        <BookingDetails key={booking.id} booking={booking} />
      ))}
      {_.isEmpty(bookings) && <p>{t("TodayBookingNoTripMessage")}</p>}
    </div>
  );
}
