import React from "react";
import { Formik, Form } from "formik";
import { InputField } from "../forms-ui/Input";
import { InputFieldWithSymbol } from "../forms-ui/InputWithSymbol";
import SelectField from "../forms-ui/Select";
import TextArea from "../forms-ui/TextArea";
import InputImage from "../forms-ui/input-image";
import DatePicker from "../forms-ui/date";

const formSplit = Object.freeze({
  one: "col-sm-12",
  two: "col-sm-12 col-md-6",
  four: "col-sm-12 col-md-3",
});

export function FormBuilderComponent({
  initialValue,
  validation,
  structures,
  ...others
}) {
  return (
    <Formik
      initialValues={initialValue}
      enableReinitialize={true}
      validationSchema={validation}
      onSubmit={(values) => {
        if (others.hasOwnProperty("onHandleSubmit")) {
          others.onHandleSubmit(values);
        }
      }}
      {...others}
    >
      {({ values, setFieldValue }) => (
        <Form>
          <div className="row">
            {structures.forms.map((structure, i) => {
              return (
                <div className={formSplit[structures.formSplit]} key={i}>
                  {structure.type === "input-text" && (
                    <InputField {...structure.formStructure} grey={true} />
                  )}

                  {structure.type === "input-date" && (
                    <DatePicker {...structure.formStructure} grey={true} />
                  )}
                  {structure.type === "input-text-with-symbol" && (
                    <InputFieldWithSymbol
                      {...structure.formStructure}
                      grey={true}
                    />
                  )}

                  {structure.type === "input-select" && (
                    <SelectField {...structure.formStructure} grey={true} />
                  )}

                  {structure.type === "textarea" && (
                    <TextArea {...structure.formStructure} />
                  )}

                  {structure.type === "image" && (
                    <InputImage {...structure.formStructure} />
                  )}
                </div>
              );
            })}
          </div>
          <div className="col-sm-12 kelaut-form-div">
            <div className="mt-3">
              <div className="float-right">
                {structures.formButton.map((button, i) => (
                  <button key={i} {...button}>
                    {button.name}
                  </button>
                ))}
              </div>
              <div style={{ clear: "both" }}></div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
}
