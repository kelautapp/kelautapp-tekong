import React from "react";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ detail = "" }) {
  return (
    <div className="bullet-list">
      <div className="bullet"></div>
      <p className="detail">{detail}</p>
    </div>
  );
}
