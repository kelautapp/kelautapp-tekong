// eslint-disable-next-line import/no-anonymous-default-export
export default [
  { id: 1, detail: "Toilet" },
  { id: 2, detail: "Catch & Hooks" },
  { id: 3, detail: "Bed & Pillow" },
  { id: 4, detail: "Ice Box" },
  { id: 5, detail: "Life Jacket" },
  { id: 6, detail: "Diving Suit" },
];
