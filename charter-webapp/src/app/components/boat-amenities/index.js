import React, { useContext, useState, useEffect } from "react";
import BaseVariantComp from "../../kelaut/animations/framer-variants/components/base-variant-comp";
import { generalCardVariants } from "../../kelaut/animations/framer-variants/general-page";
import BulletList from "../bullet-list";
import KelautCard from "../kelaut-card";
import Modal from "../modal";
// import data from "./data";
import SubMenu from "../sub-menu";
import AmenitiesForm from "../amenities-form";
import { LoaderContext } from "../../hooks";
import { useBoatAmenities } from "../../hooks/boat-management/boat-amenities";
import ToastrService from "../../../services/toastr";
import { useTranslation } from "react-i18next";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ id, amenities = null }) {
  const [show, setShow] = useState(false);
  const [data, setData] = useState({ amenities: [] });
  const { setLoading } = useContext(LoaderContext);
  const [t] = useTranslation();

  const {
    loading,
    data: amenitiesData,
    complete,
    onCreate,
  } = useBoatAmenities();

  function openModal() {
    setShow(true);
  }

  function handleCloseModal() {
    setShow(false);
  }

  function handleAction(mode) {
    if (mode === "edit") {
      openModal(true);
    }
  }

  function getListOfActions() {
    return [
      {
        title: "Edit",
        onClick: () => handleAction("edit"),
      },
    ];
  }

  function onHandleSubmit(amenities) {
    let temp = {
      id: id,
      amenities: JSON.stringify(amenities),
    };
    onCreate(temp);
  }

  useEffect(() => {
    if (loading !== null) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (complete !== null) {
      setShow(false);
      ToastrService.success(t("SuccessUpdateBoatAmenities"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [complete]);

  useEffect(() => {
    if (amenities) {
      let parseAmenities = JSON.parse(amenities);
      setData(parseAmenities);
    }
  }, [amenities]);

  useEffect(() => {
    if (amenitiesData) {
      if (amenitiesData.amenities) {
        setData(JSON.parse(amenitiesData.amenities));
      }
    }
  }, [amenitiesData]);

  return (
    <BaseVariantComp variants={generalCardVariants}>
      <KelautCard
        title={t("Amenities")}
        enableOptions
        customOptions={() => <SubMenu lists={getListOfActions()} />}
      >
        {data.amenities.length === 0 && <p>{t("NoAmenitiesMsg")}</p>}
        {data.amenities.map((d, i) => (
          <BulletList key={i} detail={d.name} />
        ))}
        <Modal
          title={t("EditAmenitiesModal")}
          handleClose={() => handleCloseModal()}
          show={show}
        >
          <AmenitiesForm onHandleSubmit={onHandleSubmit} initial={data} />
        </Modal>
      </KelautCard>
    </BaseVariantComp>
  );
}
