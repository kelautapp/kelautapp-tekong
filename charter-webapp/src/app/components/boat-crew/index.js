import React, { useContext, useState, useEffect } from "react";
import BaseVariantComp from "../../kelaut/animations/framer-variants/components/base-variant-comp";
import { generalCardVariants } from "../../kelaut/animations/framer-variants/general-page";
import KelautCard from "../kelaut-card";
import Modal from "../modal";
// import data from "./data";
import SubMenu from "../sub-menu";
import { LoaderContext, SetupContext } from "../../hooks";
import CrewBoatManagement from "../crew-boat-management";
import { useBoatCrew } from "../../hooks/boat-management/boat-crew";
import { CreateBoatCrew } from "../../types/boat/boat-crew";
import { useParams } from "react-router-dom";
import CrewList from "../crew-lists";
import ToastrService from "../../../services/toastr";
import { useTranslation } from "react-i18next";

// eslint-disable-next-line import/no-anonymous-default-export
export default function () {
  const [t] = useTranslation();
  const { id } = useParams();
  const {
    loading,
    datas,
    onCreate,
    onGet,
    onDelete,
    complete,
    completeUpdate,
    completeDelete,
    error,
    onUpdateV2,
  } = useBoatCrew();
  const [show, setShow] = useState(false);
  // const [data, setData] = useState({ amenities: [] });
  const { setLoading } = useContext(LoaderContext);

  const { setupState } = useContext(SetupContext);

  function openModal() {
    setShow(true);
  }

  function handleCloseModal() {
    setShow(false);
  }

  function handleAction(mode) {
    if (mode === "edit") {
      openModal(true);
    }
  }

  function getListOfActions() {
    return [
      {
        title: "Edit",
        onClick: () => handleAction("edit"),
      },
    ];
  }

  function handleCreate(data) {
    let boatCrew = new CreateBoatCrew(data);
    boatCrew.boatId = id;
    onCreate(boatCrew);
  }

  useEffect(() => {
    if (loading !== null) {
      setLoading(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (id) {
      onGet(id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (complete) {
      ToastrService.success(t("SuccessRegBoatCrew"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [complete]);

  useEffect(() => {
    if (completeUpdate) {
      ToastrService.success(t("SuccessUpdBoatCrew"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeUpdate]);

  useEffect(() => {
    if (completeDelete) {
      ToastrService.success(t("SuccessRmvBoatCrew"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeDelete]);

  useEffect(() => {
    if (error) {
      ToastrService.error(t("UnexpectedError"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error]);

  return (
    <BaseVariantComp variants={generalCardVariants}>
      <KelautCard
        title={t("CrewMemberTitle")}
        enableOptions
        customOptions={() => <SubMenu lists={getListOfActions()} />}
      >
        {datas.length === 0 && <p>{t("NoCrewAdded")}</p>}
        <CrewList crewData={datas} />
        {setupState && setupState.user && setupState.user.profileId && (
          <Modal
            title={t("EditCrewTitle")}
            handleClose={() => handleCloseModal()}
            show={show}
          >
            <CrewBoatManagement
              profileId={setupState.user.profileId}
              handleCreate={handleCreate}
              crewData={datas}
              handleDelete={onDelete}
              handleUpdate={onUpdateV2}
            />
          </Modal>
        )}
      </KelautCard>
    </BaseVariantComp>
  );
}
