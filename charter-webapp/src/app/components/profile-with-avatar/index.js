import React from "react";
import Image from "../../../../src/assets/images/carousel/banner_1.jpg";
import { useTranslation } from "react-i18next";
export default function ProfileWithAvatar({
  photo = null,
  backdrop = null,
  name = "N/A",
  email = "N/A",
  phone = "N/A",
  onEdit = () => {},
  onChgPwd = () => {},
}) {
  function getAvatarName(name) {
    if (name) {
      let temp = name.split(" ");
      let newName = temp.reduce((name, item, i) => {
        if (i < 2) {
          return (name += item.charAt(0));
        }
        return name;
      }, "");
      return newName;
    }
    return "N/A";
  }

  const [t] = useTranslation();

  return (
    <div className="profile-with-avatar">
      <div className="backdrop">
        <img src={backdrop || Image} alt="avatar" />
        <div className="action" onClick={onEdit}>
          <i className="mdi mdi-table-edit" />
        </div>
      </div>

      <div className="profile-detail">
        {photo ? (
          <div className="profile-image">
            <img src={photo} alt="profile" />
          </div>
        ) : (
          <div className="avatar">{getAvatarName(name)}</div>
        )}

        <div className="summary">
          <p className="name">{name}</p>
          <div className="extra-info">
            <span className="symbol">@</span>
            <span className="detail">{email}</span>
          </div>
          <div className="extra-info">
            <span className="symbol">
              <i className="mdi mdi-phone" />
            </span>
            <span className="detail">{phone}</span>
          </div>
        </div>
      </div>
      <div className="action">
        <button className="btn btn-lg btn-default" onClick={onEdit}>
          {" "}
          {t("EditProfileButton")}
        </button>
        <button className="btn btn-lg btn-default" onClick={onChgPwd}>
          {" "}
          {t("ChangePasswordButton")}
        </button>
      </div>
    </div>
  );
}
