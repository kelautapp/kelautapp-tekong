import React, { useState, useEffect } from "react";

export default function PhotoList({ data, onConfirm = () => {} }) {
  const [show, setShow] = useState(false);

  const deleteBtn = (data) => {
    return (
      <div className="delete-btn-container">
        <p>Delete</p>
        <div className="action-container">
          <span className="delete" onClick={() => setShow(false)}>
            <i className="mdi mdi-close-circle" />
          </span>
          <span className="proceed">
            <i
              className="mdi mdi-check-circle"
              onClick={() => confirm(data.id)}
            />
          </span>
        </div>
      </div>
    );
  };

  function onDelete() {
    setShow(true);
  }

  function confirm(id) {
    onConfirm(id);
  }

  useEffect(() => {}, []);
  return (
    <div className="photo-container">
      <div className="photo-list">
        {show ? (
          deleteBtn(data)
        ) : (
          <>
            <div className="img-info">
              <img src={data.url} alt={data.name} />
              <p>{data.name}</p>
            </div>

            <span className="action-btn" onClick={onDelete}>
              <i className="mdi mdi-close" />
            </span>
          </>
        )}
      </div>
    </div>
  );
}
