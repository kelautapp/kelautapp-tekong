import React from "react";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ title = "", detail = "" }) {
  return (
    <div className="display-info">
      <p className="title">{title}</p>
      <p className="detail">{detail}</p>
    </div>
  );
}
