import React, { useState } from "react";
import BookingDate from "../booking-date";
import * as moment from "moment";
import Badge from "react-bootstrap/Badge";
import _ from "lodash";
import { useTranslation } from "react-i18next";

// eslint-disable-next-line import/no-anonymous-default-export
export default function () {
  const [bookings] = useState([]);
  const [t] = useTranslation();
  // const [bookings] = useState([
  //   {
  //     id: 1,
  //     bookingNumber: "BNF2002 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 2,
  //     bookingNumber: "BNF2003 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 3,
  //     bookingNumber: "BNF2004 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 4,
  //     bookingNumber: "BNF2004 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 5,
  //     bookingNumber: "BNF2004 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 6,
  //     bookingNumber: "BNF2004 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     //   image: Boat,
  //   },
  //   {
  //     id: 7,
  //     bookingNumber: "BNF2001 KL-2",
  //     location: "Pulau Langkawi",
  //     state: "Kedah",
  //     date: "2022-01-12",
  //     //   image: Boat,
  //   },
  // ]);

  function getTime(date) {
    return moment(date).format("HH:mma");
  }
  return (
    <div className="table-responsive">
      {!_.isEmpty(bookings) && (
        <table className="table ub-dashboard">
          <thead>
            <tr>
              <td>{t("ColumnNameDate")}</td>
              <td>{t("ColumnNameTrip")}</td>
              <td align="center">{t("ColumnNameTime")}</td>
            </tr>
          </thead>
          <tbody>
            {bookings.map((booking) => (
              <tr key={booking.id}>
                <td>
                  <BookingDate date={booking.date} />
                </td>
                <td>
                  <p style={{ marginBottom: 5, fontSize: 14 }}>
                    {booking.location}
                  </p>
                  <p style={{ marginBottom: 0 }}>{booking.bookingNumber}</p>
                </td>
                <td align="center">
                  <Badge pill variant="light" className="badge-time">
                    {getTime(booking.date)}
                  </Badge>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
      {_.isEmpty(bookings) && <p>{t("UpcomingBookingNoTripMessage")}</p>}
    </div>
  );
}
