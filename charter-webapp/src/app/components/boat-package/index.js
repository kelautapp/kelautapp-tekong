import React, { useState, useEffect, useContext } from "react";
import KelautCard from "../kelaut-card";
import Modal from "../modal";
import Package from "../package";
import PackageForm from "./package-form";
import { useParams } from "react-router-dom";
import { LoaderContext } from "../../hooks";
import { useBoatPackage } from "../../hooks/boat-management/boat-package";
import { toast } from "react-toastify";
import {
  CreateBoatPackage,
  UpdateBoatPackage,
} from "../../types/boat/boat-package";
import Skeleton from "react-loading-skeleton";
import SubMenu from "../sub-menu";
import { useTranslation } from "react-i18next";

const AddButton = ({ onClick }) => {
  return <i className="mdi mdi-plus-circle add-button" onClick={onClick} />;
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function () {
  const [show, setShow] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [initial, setInitial] = useState(null);
  const [deleteData, setDeleteData] = useState(null);
  let { id } = useParams();
  const { setLoading } = useContext(LoaderContext);
  const [localLoading, setLocalLoading] = useState(null);
  const {
    loading,
    data: packageData,
    onCreate,
    complete,
    onGet,
    datas,
    onUpdate,
    completeUpdate,
    getLoading,
    setDatas,
    onDelete,
    completeDelete,
  } = useBoatPackage();

  const [t] = useTranslation();

  function openModal() {
    setShow(true);
  }

  function onHandleSubmit(packageData) {
    if (packageData.id) {
      let data = new UpdateBoatPackage(packageData);
      onUpdate(data);
    } else {
      let data = new CreateBoatPackage(packageData);
      data.setBoatId(id);
      onCreate(data);
    }
  }

  function getListOfActions(data) {
    return [
      {
        title: "Edit",
        onClick: () => handleAction(data, "edit"),
      },
      {
        title: "Delete",
        onClick: () => handleAction(data, "delete"),
      },
    ];
  }

  function handleAction(data, mode) {
    if (mode === "edit") {
      setInitial(data);
      setShow(true);
    } else {
      setDeleteData(data);
      setShowDelete(true);
    }
  }

  function handleCloseModal() {
    setShow(false);
    setInitial(null);
  }

  function handleCloseDeleteModal() {
    setDeleteData(null);
    setShowDelete(false);
  }

  function handleBoatPackageDeletion(packageData) {
    if (packageData) {
      onDelete(packageData);
    }
  }

  useEffect(() => {
    if (loading !== null) {
      setLoading(loading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (getLoading !== null) {
      setLocalLoading(getLoading);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getLoading]);

  useEffect(() => {
    if (complete) {
      toast.success(t("SuccessCreateBP"));
      setDatas((prevDatas) => [...prevDatas, packageData]);
      handleCloseModal();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [complete]);

  useEffect(() => {
    if (completeDelete) {
      toast.success(`${t("SuccessDeleteBP")}: ${deleteData.packageName}`);
      setDatas((prevDatas) => prevDatas.filter((d) => d.id !== deleteData.id));
      handleCloseDeleteModal();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeDelete]);

  useEffect(() => {
    if (completeUpdate) {
      toast.success(t("SuccessUpdateBP"));
      setDatas((prevDatas) => {
        let filterData = prevDatas.map((data) => {
          if (data.id === packageData.id) {
            return packageData;
          } else return data;
        });

        return filterData;
      });
      handleCloseModal();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeUpdate]);

  useEffect(() => {
    if (id) onGet(id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  return (
    <KelautCard
      title={t("Package")}
      enableOptions
      customOptions={() => <AddButton onClick={openModal} />}
    >
      {datas.length === 0 && <p>{t("NotAvailableBP")}</p>}
      {localLoading === true ? (
        <Skeleton height={200} />
      ) : (
        datas.map((d) => (
          <Package
            key={d.id}
            data={d}
            customOptions={() => <SubMenu lists={getListOfActions(d)} />}
          />
        ))
      )}
      {}

      <Modal
        title={initial ? t("ModalTitleUP") : t("ModalTitleNP")}
        handleClose={() => handleCloseModal()}
        show={show}
      >
        <PackageForm initial={initial} onHandleSubmit={onHandleSubmit} />
      </Modal>

      {/* delete modal confirmation */}
      <Modal
        title={t("BoatDetailBoatPackageDelete")}
        handleClose={() => handleCloseDeleteModal()}
        show={showDelete}
      >
        {deleteData && (
          <div className="delete-modal">
            <p className="message">
              {t("DeletePmsg")}
              <span className="name">{deleteData.packageName}</span> ?
            </p>
            <div className="d-flex flex-row justify-content-between">
              <button
                className="btn font-weight-medium auth-form-btn btn-default"
                onClick={handleCloseDeleteModal}
              >
                {t("Cancel")}
              </button>
              <button
                className="btn font-weight-medium auth-form-btn kelaut-primary-btn"
                onClick={() => handleBoatPackageDeletion(deleteData)}
              >
                {t("Confirm")}
              </button>
            </div>
          </div>
        )}
      </Modal>
    </KelautCard>
  );
}
