import * as Yup from "yup";
import i18n from "../../../../i18n";

export const formStructure = {
  formSplit: "one",
  forms: [
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("PNameLabel"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "packageName",
        name: "packageName",
        placeholder: i18n.t("PNamePlaceholder"),
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("PHourLabel"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "tripHour",
        name: "tripHour",
        placeholder: i18n.t("PHourPlaceholder"),
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("PPriceLabel"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "price",
        name: "price",
        placeholder: i18n.t("PPricePlaceholder"),
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("PStartTimeLabel"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "startTime",
        name: "startTime",
        placeholder: i18n.t("PStartTimePlaceholder"),
      },
    },
    {
      type: "textarea",
      formStructure: {
        label: i18n.t("PDescLabel"),
        className:
          "form-control form-control-lg input-border kelaut-textarea-ui",
        id: "description",
        name: "description",
        placeholder: i18n.t("PDescPlaceholder"),
        rows: 5,
        maxLength: 200,
      },
    },
  ],
  formButton: [
    {
      type: "submit",
      name: i18n.t("RegPBtn"),
      className:
        "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
    },
  ],
};

export const validate = Yup.object({
  packageName: Yup.string().required(i18n.t("ThisFieldIsRequired")),
  tripHour: Yup.number()
    .required(i18n.t("ThisFieldIsRequired"))
    .typeError(i18n.t("TypeErrorNumberOnly")),
  startTime: Yup.string().required(i18n.t("ThisFieldIsRequired")),
  description: Yup.string(i18n.t("TypeErrorTextOnly")).required(
    i18n.t("ThisFieldIsRequired")
  ),
  price: Yup.number()
    .required(i18n.t("ThisFieldIsRequired"))
    .moreThan(0, i18n.t("ValidationPriceP"))
    .typeError(i18n.t("TypeErrorNumberOnly")),
});
