import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { FormBuilderComponent } from "../..";
import { formStructure, validate } from "./config";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ initial = null, onHandleSubmit = () => {} }) {
  const [initialValues, setInitialValues] = useState({
    id: "",
    packageName: "",
    tripHour: "",
    startTime: "",
    description: "",
    price: "",
  });

  const [t] = useTranslation();

  useEffect(() => {
    if (initial) setInitialValues(initial);
    else {
      setInitialValues({
        id: "",
        packageName: "",
        tripHour: "",
        startTime: "",
        description: "",
        price: "",
      });
    }
  }, [initial]);

  return (
    <FormBuilderComponent
      initialValue={initialValues}
      validation={validate}
      structures={
        initialValues.id
          ? {
              ...formStructure,
              formButton: [
                { ...formStructure.formButton[0], name: t("UpdateBPBtn") },
              ],
            }
          : formStructure
      }
      onHandleSubmit={(values) => onHandleSubmit(values)}
    />
  );
}
