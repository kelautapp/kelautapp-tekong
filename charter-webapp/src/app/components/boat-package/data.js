// eslint-disable-next-line import/no-anonymous-default-export
export default [
  {
    id: 1,
    title: "Package A",
    hour: 13,
    startTime: "7:00 AM",
    price: 1000,
    description: `Aliquip ipsum qui fugiat deserunt aute adipisicing ipsum irure
    consectetur aliqua cillum nisi dolore pariatur. Proident non
    excepteur labore sunt anim exercitation Lorem veniam ut non nisi.
    Nisi duis Lorem nisi anim occaecat dolor sunt et qui ea nisi.
    Lorem voluptate labore qui irure. Labore minim mollit ea commodo
    proident quis commodo esse. Ipsum dolore commodo aliquip ea ex.
    Pariatur pariatur cillum nulla occaecat laboris amet esse. Tempor
    officia occaecat duis qui amet quis sint consequat. Ipsum sit
    deserunt adipisicing ipsum sit voluptate consectetur nostrud
    exercitation sint reprehenderit. Proident nostrud aliqua officia
    occaecat do veniam laborum excepteur aliqua nisi culpa. Culpa qui
    voluptate id dolore consectetur elit. Non pariatur officia ut sint
    consequat ex. Deserunt officia ad culpa ut adipisicing sit amet
    velit ad. Sit duis fugiat eiusmod reprehenderit excepteur cillum
    occaecat minim aliqua eiusmod magna eiusmod sint reprehenderit.
    Minim qui anim tempor ea in aute laboris non sunt consectetur
    officia quis. Sint adipisicing ullamco consectetur irure
    consectetur voluptate aliquip laborum tempor fugiat enim mollit
    anim duis. Elit veniam aliqua officia veniam nisi veniam. Aliquip
    adipisicing veniam culpa labore dolor laboris sit incididunt
    excepteur velit aliquip cillum pariatur Lorem. Quis fugiat
    proident ad dolor id ipsum in sint ullamco dolor excepteur do.
    Dolor laboris qui in sint id proident nisi magna commodo anim
    consequat exercitation. Ex tempor proident do id amet sint et nisi
    aliquip nisi mollit aliquip sunt. Ipsum ad incididunt occaecat
    officia proident adipisicing est in veniam sunt reprehenderit in
    incididunt. Ut dolore cillum sit ipsum culpa aliquip qui excepteur
    veniam nulla.`,
  },
  {
    id: 2,
    title: "Package B",
    hour: 13,
    startTime: "7:00 AM",
    price: 1000,
    description: `Aliquip ipsum qui fugiat deserunt aute adipisicing ipsum irure
    consectetur aliqua cillum nisi dolore pariatur. Proident non
    excepteur labore sunt anim exercitation Lorem veniam ut non nisi.
    Nisi duis Lorem nisi anim occaecat dolor sunt et qui ea nisi.
    Lorem voluptate labore qui irure. Labore minim mollit ea commodo
    proident quis commodo esse. Ipsum dolore commodo aliquip ea ex.
    Pariatur pariatur cillum nulla occaecat laboris amet esse. Tempor
    officia occaecat duis qui amet quis sint consequat. Ipsum sit
    deserunt adipisicing ipsum sit voluptate consectetur nostrud
    exercitation sint reprehenderit. Proident nostrud aliqua officia
    occaecat do veniam laborum excepteur aliqua nisi culpa. Culpa qui
    voluptate id dolore consectetur elit. Non pariatur officia ut sint
    consequat ex. Deserunt officia ad culpa ut adipisicing sit amet
    velit ad. Sit duis fugiat eiusmod reprehenderit excepteur cillum
    occaecat minim aliqua eiusmod magna eiusmod sint reprehenderit.
    Minim qui anim tempor ea in aute laboris non sunt consectetur
    officia quis. Sint adipisicing ullamco consectetur irure
    consectetur voluptate aliquip laborum tempor fugiat enim mollit
    anim duis. Elit veniam aliqua officia veniam nisi veniam. Aliquip
    adipisicing veniam culpa labore dolor laboris sit incididunt
    excepteur velit aliquip cillum pariatur Lorem. Quis fugiat
    proident ad dolor id ipsum in sint ullamco dolor excepteur do.
    Dolor laboris qui in sint id proident nisi magna commodo anim
    consequat exercitation. Ex tempor proident do id amet sint et nisi
    aliquip nisi mollit aliquip sunt. Ipsum ad incididunt occaecat
    officia proident adipisicing est in veniam sunt reprehenderit in
    incididunt. Ut dolore cillum sit ipsum culpa aliquip qui excepteur
    veniam nulla.`,
  },
  {
    id: 3,
    title: "Package C",
    hour: 13,
    startTime: "7:00 AM",
    price: 1000,
    description: `Aliquip ipsum qui fugiat deserunt aute adipisicing ipsum irure
    consectetur aliqua cillum nisi dolore pariatur. Proident non
    excepteur labore sunt anim exercitation Lorem veniam ut non nisi.
    Nisi duis Lorem nisi anim occaecat dolor sunt et qui ea nisi.
    Lorem voluptate labore qui irure. Labore minim mollit ea commodo
    proident quis commodo esse. Ipsum dolore commodo aliquip ea ex.
    Pariatur pariatur cillum nulla occaecat laboris amet esse. Tempor
    officia occaecat duis qui amet quis sint consequat. Ipsum sit
    deserunt adipisicing ipsum sit voluptate consectetur nostrud
    exercitation sint reprehenderit. Proident nostrud aliqua officia
    occaecat do veniam laborum excepteur aliqua nisi culpa. Culpa qui
    voluptate id dolore consectetur elit. Non pariatur officia ut sint
    consequat ex. Deserunt officia ad culpa ut adipisicing sit amet
    velit ad. Sit duis fugiat eiusmod reprehenderit excepteur cillum
    occaecat minim aliqua eiusmod magna eiusmod sint reprehenderit.
    Minim qui anim tempor ea in aute laboris non sunt consectetur
    officia quis. Sint adipisicing ullamco consectetur irure
    consectetur voluptate aliquip laborum tempor fugiat enim mollit
    anim duis. Elit veniam aliqua officia veniam nisi veniam. Aliquip
    adipisicing veniam culpa labore dolor laboris sit incididunt
    excepteur velit aliquip cillum pariatur Lorem. Quis fugiat
    proident ad dolor id ipsum in sint ullamco dolor excepteur do.
    Dolor laboris qui in sint id proident nisi magna commodo anim
    consequat exercitation. Ex tempor proident do id amet sint et nisi
    aliquip nisi mollit aliquip sunt. Ipsum ad incididunt occaecat
    officia proident adipisicing est in veniam sunt reprehenderit in
    incididunt. Ut dolore cillum sit ipsum culpa aliquip qui excepteur
    veniam nulla.`,
  },
  {
    id: 4,
    title: "Package D",
    hour: 13,
    startTime: "7:00 AM",
    price: 1000,
    description: `Aliquip ipsum qui fugiat deserunt aute adipisicing ipsum irure
    consectetur aliqua cillum nisi dolore pariatur. Proident non
    excepteur labore sunt anim exercitation Lorem veniam ut non nisi.
    Nisi duis Lorem nisi anim occaecat dolor sunt et qui ea nisi.
    Lorem voluptate labore qui irure. Labore minim mollit ea commodo
    proident quis commodo esse. Ipsum dolore commodo aliquip ea ex.
    Pariatur pariatur cillum nulla occaecat laboris amet esse. Tempor
    officia occaecat duis qui amet quis sint consequat. Ipsum sit
    deserunt adipisicing ipsum sit voluptate consectetur nostrud
    exercitation sint reprehenderit. Proident nostrud aliqua officia
    occaecat do veniam laborum excepteur aliqua nisi culpa. Culpa qui
    voluptate id dolore consectetur elit. Non pariatur officia ut sint
    consequat ex. Deserunt officia ad culpa ut adipisicing sit amet
    velit ad. Sit duis fugiat eiusmod reprehenderit excepteur cillum
    occaecat minim aliqua eiusmod magna eiusmod sint reprehenderit.
    Minim qui anim tempor ea in aute laboris non sunt consectetur
    officia quis. Sint adipisicing ullamco consectetur irure
    consectetur voluptate aliquip laborum tempor fugiat enim mollit
    anim duis. Elit veniam aliqua officia veniam nisi veniam. Aliquip
    adipisicing veniam culpa labore dolor laboris sit incididunt
    excepteur velit aliquip cillum pariatur Lorem. Quis fugiat
    proident ad dolor id ipsum in sint ullamco dolor excepteur do.
    Dolor laboris qui in sint id proident nisi magna commodo anim
    consequat exercitation. Ex tempor proident do id amet sint et nisi
    aliquip nisi mollit aliquip sunt. Ipsum ad incididunt occaecat
    officia proident adipisicing est in veniam sunt reprehenderit in
    incididunt. Ut dolore cillum sit ipsum culpa aliquip qui excepteur
    veniam nulla.`,
  },
];
