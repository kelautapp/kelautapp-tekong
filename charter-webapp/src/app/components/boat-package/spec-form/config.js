import * as Yup from "yup";
import { listOfYear } from "./data";
import i18n from "../../../../i18n";

const years = listOfYear().map((year) => ({ label: year, value: year }));
export const formStructure = {
  formSplit: "one",
  forms: [
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("FormLabelBoatHP"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "horsePower",
        name: "horsePower",
        placeholder: i18n.t("FormPlaceHolderBoatHP"),
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("FormLabelBoatSpeed"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "maxSpeed",
        name: "maxSpeed",
        placeholder: i18n.t("FormPlaceholderBoatSpeed"),
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("FormLabelBoatLength"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "length",
        name: "length",
        placeholder: i18n.t("FormPlaceholderBoatLength"),
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("FormLabelBoatCapacity"),
        type: "text",
        className: "form-control form-control-lg input-border",
        id: "capacity",
        name: "capacity",
        placeholder: i18n.t("FormPlaceholderBoatCapacity"),
      },
    },
    {
      type: "input-select",
      formStructure: {
        label: i18n.t("FormLabelBoatYear"),
        id: "year",
        name: "year",
        placeholder: i18n.t("FormPlaceholderBoatYear"),
        options: [...years],
        isSearchable: true,
      },
    },
  ],
  formButton: [
    {
      type: "submit",
      name: i18n.t("UpdateBoatSummaryBtn"),
      className:
        "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
    },
  ],
};

export const validate = Yup.object({
  maxSpeed: Yup.string().required(i18n.t("RequireBoatSpeed")),
  horsePower: Yup.string().required(i18n.t("RequireBoatHP")),
  length: Yup.string(),
  year: Yup.string().required(i18n.t("RequireBoatYear")),
  capacity: Yup.number().required(i18n.t("RequireBoatCapacity")),
});
