import React, { useState, useEffect } from "react";
import { FormBuilderComponent } from "../..";
import { formStructure, validate } from "./config";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ initial = null, onHandleSubmit = () => {} }) {
  const [initialValues, setInitialValues] = useState({
    horsePower: "",
    maxSpeed: "",
    length: "",
    capacity: "",
    year: "",
    id: "",
  });

  useEffect(() => {
    if (initial) setInitialValues(initial);
    else {
      setInitialValues({
        horsePower: "",
        maxSpeed: "",
        length: "",
        capacity: "",
        year: "",
        id: "",
      });
    }
  }, [initial]);

  return (
    <FormBuilderComponent
      initialValue={initialValues}
      validation={validate}
      structures={formStructure}
      onHandleSubmit={(values) => onHandleSubmit(values)}
    />
  );
}
