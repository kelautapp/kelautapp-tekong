export function listOfYear() {
  const currentYear = new Date().getFullYear();
  let years = [];
  for (let i = 30; i > -1; i--) {
    years.push((currentYear - i).toString());
  }

  return years;
}
