import React from "react";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ path, size = "medium" }) {
  return (
    <div className="weather-icon-container ">
      <div className={size}>
        <img src={path} className="weather-icon" alt="weather symbol" />
      </div>
    </div>
  );
}
