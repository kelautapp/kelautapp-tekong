import React from "react";

function CustomOptions(props) {
  if (props.CustomOption) {
    return <props.CustomOption />;
  }
  return (
    <p className="text-muted mb-1">
      {" "}
      <i className="mdi mdi-dots-horizontal"></i>
    </p>
  );
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({
  title,
  enableOptions = false,
  children,
  bottomMenu,
  bottomMenuObject,
  customOptions,
  ...otherProps
}) {
  // const []
  return (
    <div className="card mb-3" {...otherProps}>
      <div className="card-body">
        <div className="d-flex flex-row justify-content-between">
          <h4 className="card-title mb-1 kelaut-card-title">{title}</h4>
          {enableOptions && <CustomOptions CustomOption={customOptions} />}
        </div>
        {children}
      </div>
      {bottomMenu && (
        <div className="bottom-menu" onClick={bottomMenuObject}>
          <p className="bottom-menu-text">{bottomMenu}</p>
        </div>
      )}
    </div>
  );
}
