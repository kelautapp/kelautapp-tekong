import React, { useEffect } from "react";
import useBoat from "../../hooks/boat-management/boat";
import BoatSpecifications from "../boat-specifications";
import { useParams } from "react-router-dom";
import BoatAmenities from "../boat-amenities";
import BoatSummary from "../boat-summary";
import BoatFishingType from "../boat-fishing-type";
import i18n from "i18next";

const showLists = {
  horsePower: i18n.t("BoatDetailBoatSpecificationHorsePower"),
  maxSpeed: i18n.t("BoatDetailBoatSpecificationMaxSpeed"),
  length: i18n.t("BoatDetailBoatSpecificationLength"),
  capacity: i18n.t("BoatDetailBoatSpecificationCapacity"),
  year: i18n.t("BoatDetailBoatSpecificationYearModel"),
};
// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ setStatus = () => {} }) {
  let { id } = useParams();
  const { loading, boat, onGetBoat } = useBoat();

  useEffect(() => {
    if (id) {
      onGetBoat(id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (id) {
      onGetBoat(id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  useEffect(() => {
    if (loading !== null) {
      // onGetBoat(id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (boat !== null) {
      // onGetBoat(id);
      setStatus(boat.status);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [boat]);

  return (
    <>
      {boat && (
        <>
          <BoatSummary data={boat} />
          <BoatSpecifications data={boat} showList={showLists} />
          <BoatAmenities id={id} amenities={boat.amenities} />
          <BoatFishingType id={id} fishingType={boat.fishingType} />
        </>
      )}
    </>
  );
}
