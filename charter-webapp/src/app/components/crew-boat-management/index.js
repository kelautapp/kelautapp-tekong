import React, { useState, useEffect } from "react";
import Select from "react-select";
import { useCrew } from "../../hooks/crew";
import Skeleton from "react-loading-skeleton";
import { useTranslation } from "react-i18next";

export default function CrewBoatManagement({
  profileId = null,
  handleCreate = () => {},
  crewData = [],
  handleDelete = () => {},
  handleUpdate = () => {},
}) {
  // const { setupState } = useContext(SetupContext);

  const { onGet, datas, loading } = useCrew();
  const [crews, setCrews] = useState([]);
  const [options, setOptions] = useState([]);
  const [t] = useTranslation();
  const roles = [
    { value: "Captain", label: t("Captain") },
    { value: "Crew", label: t("Crew") },
  ];

  function addCrew() {
    setCrews((prevCrew) => {
      let temp = [...prevCrew];
      temp.push({
        id: null,
        name: null,
        role: null,
      });

      return temp;
    });
  }

  function onChange(event) {
    if (event && event.value) {
      handleCreate(event.value);
    }
  }

  function onChangeRole(event, id) {
    const deleteCaptain = crews.filter(
      (c) => c.role.toLowerCase() === "captain"
    );
    let dataToBeUpdate = [
      {
        id: id,
        role: event.value,
      },
    ];
    if (deleteCaptain.length > 0) {
      deleteCaptain.map(
        (d) => d.id !== id && dataToBeUpdate.push({ id: d.id, role: "Crew" })
      );
    }
    handleUpdate(dataToBeUpdate);
  }

  function getAvatarName(name) {
    if (name) {
      let temp = name.split(" ");
      let newName = temp.reduce((name, item, i) => {
        if (i < 2) {
          return (name += item.charAt(0));
        }
        return name;
      }, "");
      return newName;
    }
    return "N/A";
  }

  useEffect(() => {
    if (datas.length > 0) {
      let temp = datas
        .filter((d) => d.status === "active")
        .map((d) => ({ value: d, label: d.name }));

      setOptions(temp);
    }
  }, [datas]);

  useEffect(() => {
    if (crews.length > 0) {
      let ids = crews.map((c) => c.crewId);
      let temp = datas
        .filter((d) => d.status === "active" && ids.indexOf(d.id) < 0)
        .map((d) => ({ value: d, label: d.name }));

      setOptions(temp);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [crews]);

  useEffect(() => {
    if (profileId) {
      onGet(profileId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [profileId]);

  useEffect(() => {
    if (crewData) {
      setCrews(crewData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [crewData]);
  return (
    <div className="kelaut-boat-crew">
      {loading ? (
        <Skeleton height={200} />
      ) : (
        <>
          {crews.map((crew, i) => {
            let temp = crew.id ? (
              <div className="border" key={i}>
                <div className="crew-profile">
                  <div className="detail-container">
                    {crew.photo === null && (
                      <div className="avatar">{getAvatarName(crew.name)}</div>
                    )}
                    {crew.photo && (
                      <img className="crew-image" src={crew.photo} alt="temp" />
                    )}
                    <div className="crew-detail">
                      <p className="name">{crew.name}</p>

                      <Select
                        className="crew-role"
                        options={roles}
                        onChange={(e) => onChangeRole(e, crew.id)}
                        value={
                          crew.role
                            ? roles.find((option) => option.value === crew.role)
                            : ""
                        }
                        name={"name" + i}
                      ></Select>
                    </div>
                  </div>

                  <div
                    className="crew-action"
                    onClick={() => handleDelete(crew.id)}
                  >
                    <i className="mdi mdi-delete-forever" />
                  </div>
                  <div></div>
                </div>
              </div>
            ) : (
              <div className="border" key={i}>
                <Select
                  className="crew-list"
                  options={options}
                  onChange={onChange}
                ></Select>
              </div>
            );
            return temp;
          })}

          <div className="action border" onClick={addCrew}>
            + {t("AddNewCrew")}
          </div>
        </>
      )}
    </div>
  );
}
