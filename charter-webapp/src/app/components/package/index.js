import React from "react";
import { useTranslation } from "react-i18next";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ data = null, customOptions = () => {} }) {
  const [t] = useTranslation();
  return (
    <div className="package">
      {data && (
        <>
          <div className="title">
            <div className="d-flex flex-row justify-content-between">
              <p>{data.packageName}</p>
              {customOptions()}
              {/* <p className="text-muted mb-1">
                {" "}
                <i className="mdi mdi-dots-horizontal"></i>
              </p> */}
            </div>
          </div>

          <div className="duration">
            <p>
              {data.tripHour} hour trip, {data.startTime}
            </p>
          </div>

          <div className="price">
            <p>RM {data.price}</p>
          </div>

          <div className="description-header">
            <p>{t("PackageTripDetail")}</p>
          </div>

          <div className="description-content">
            <p>{data.description}</p>
          </div>
        </>
      )}
    </div>
  );
}
