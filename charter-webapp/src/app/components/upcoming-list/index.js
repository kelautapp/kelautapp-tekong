import React, { useState } from "react";
import BookingDate from "../booking-date";
import * as moment from "moment";
import Badge from "react-bootstrap/Badge";

export default function () {
  const [bookings] = useState([
    {
      id: 1,
      bookingNumber: "BNF2002 KL-2",
      location: "Pulau Langkawi",
      state: "Kedah",
      date: "2022-01-12 01:30:00",
      //   image: Boat,
    },
    {
      id: 2,
      bookingNumber: "BNF2003 KL-2",
      location: "Pulau Langkawi",
      state: "Kedah",
      date: "2022-01-12 12:30:00",
      //   image: Boat,
    },
    {
      id: 3,
      bookingNumber: "BNF2004 KL-2",
      location: "Pulau Langkawi",
      state: "Kedah",
      date: "2022-01-12 23:00:00",
      //   image: Boat,
    },
    {
      id: 4,
      bookingNumber: "BNF2004 KL-2",
      location: "Pulau Langkawi",
      state: "Kedah",
      date: "2022-01-12 23:00:00",
      //   image: Boat,
    },
    {
      id: 5,
      bookingNumber: "BNF2004 KL-2",
      location: "Pulau Langkawi",
      state: "Kedah",
      date: "2022-01-12 23:00:00",
      //   image: Boat,
    },
    {
      id: 6,
      bookingNumber: "BNF2004 KL-2",
      location: "Pulau Langkawi",
      state: "Kedah",
      date: "2022-01-12 23:00:00",
      //   image: Boat,
    },
  ]);

  function getTime(date) {
    return moment(date).format("HH:mm");
  }

  return (
    <div className="table-responsive">
      <table className="table">
        <thead>
          <tr>
            <td>Date</td>
            <td>Trip</td>
            <td>Time</td>
          </tr>
        </thead>
        <tbody>
          {bookings.map((booking) => (
            <tr key={booking.id}>
              <td>
                <BookingDate date={booking.date} />
              </td>
              <td>
                <p style={{ marginBottom: 5, fontSize: 14 }}>
                  {booking.location}
                </p>
                <p>{booking.bookingNumber}</p>
              </td>
              <td>
                <Badge pill variant="light" style={{ color: "#000" }}>
                  {getTime(booking.date)}
                </Badge>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
