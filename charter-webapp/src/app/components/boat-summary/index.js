import React, { useState, useContext, useEffect } from "react";
import { useTranslation } from "react-i18next";
import ToastrService from "../../../services/toastr";
import { LoaderContext } from "../../hooks";
import { useBoatSummary } from "../../hooks/boat-management/boat-summary";
// import ReactStars from "react-rating-stars-component";
import BaseVariantComp from "../../kelaut/animations/framer-variants/components/base-variant-comp";
import { generalCardVariants } from "../../kelaut/animations/framer-variants/general-page";
import KelautCard from "../kelaut-card";
import Modal from "../modal";
import SubMenu from "../sub-menu";
import SummaryForm from "../summary-form";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ data }) {
  const [show, setShow] = useState(false);
  const [t] = useTranslation();
  const { setLoading } = useContext(LoaderContext);
  const {
    loading,
    data: summaryData,
    completeUpdate,
    onUpdate,
    setData: setSummaryData,
  } = useBoatSummary();

  function openModal() {
    setShow(true);
  }

  function handleCloseModal() {
    setShow(false);
  }

  function handleAction(mode) {
    if (mode === "edit") {
      openModal(true);
    }
  }

  function getListOfActions() {
    return [
      {
        title: "Edit",
        onClick: () => handleAction("edit"),
      },
    ];
  }

  function onHandleSubmit(summaryData) {
    onUpdate(summaryData);
  }

  useEffect(() => {
    if (loading !== null) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (completeUpdate !== null) {
      setShow(false);
      ToastrService.success(t("SuccessUpdateBoatSummary"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeUpdate, t]);

  useEffect(() => {
    setSummaryData(data);
  }, [setSummaryData, data]);

  return (
    <div className="row">
      <div className="col-sm-12">
        <BaseVariantComp variants={generalCardVariants}>
          <KelautCard
            title=""
            enableOptions
            customOptions={() => <SubMenu lists={getListOfActions()} />}
          >
            <div className="boat-summary">
              <p className="boat-name">{summaryData && summaryData.name}</p>
              <div>
                <p className="boat-location">
                  <span className="place-symbol">
                    <i className="mdi mdi-map-marker" />
                  </span>
                  <span className="place">
                    {summaryData && summaryData.location
                      ? summaryData.location
                      : t("NoBoatLocationMsg")}
                  </span>
                  {/* <span className="state">Johor</span> */}
                </p>
              </div>
              {/* comment the rating component until got flow for review */}
              {/* <div className="boat-rating">
                <p className="number-rating">4.9</p>
                <ReactStars
                  count={5}
                  size={16}
                  activeColor="#FF9501"
                  edit={false}
                  value={4.9}
                />
                <p className="total-rating">(37)</p>
              </div> */}
              <p className="boat-description">
                {summaryData && summaryData.description}
              </p>
            </div>
            <Modal
              title={t("EditBoatModalTitle")}
              handleClose={() => handleCloseModal()}
              show={show}
            >
              <SummaryForm
                initial={summaryData}
                onHandleSubmit={onHandleSubmit}
              />
            </Modal>
          </KelautCard>
        </BaseVariantComp>
      </div>
    </div>
  );
}
