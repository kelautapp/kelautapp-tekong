import React from "react";
import BookingDate from "../booking-date";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ booking }) {
  return (
    <div className="booking-container">
      <div className="booking-details">
        <div className="booking-detail">
          <img src={booking.image} className="boat-image" alt="boat" />
          <div className="booking-info">
            <div className="booking-number">{booking.bookingNumber}</div>
            <div>
              <i className="mdi mdi-map-marker boat-marker" />
              <span className="boat-location">{booking.location},</span>
              <span className="boat-state">{booking.state}</span>
            </div>
          </div>
        </div>
        <BookingDate date={booking.date} />
      </div>
    </div>
  );
}
