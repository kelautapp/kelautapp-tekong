import React, { useState, useRef, useEffect } from "react";

function useOutsideAlerter(ref) {
  const [count, setCount] = useState(0);
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        // alert("You clicked outside of me!");
        setCount((prevCount) => prevCount + 1);
      }
    }

    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);

  return { count };
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ lists = [] }) {
  const [show, setShow] = useState(false);
  const wrapperRef = useRef(null);
  const { count } = useOutsideAlerter(wrapperRef);

  useEffect(() => {
    if (count > 0) {
      setShow(false);
    }
  }, [count]);

  return (
    <div className="sub-menu" ref={wrapperRef}>
      <p className="text-muted mb-1 icon" onClick={() => setShow(true)}>
        {" "}
        <i className="mdi mdi-dots-horizontal"></i>
      </p>
      {show && (
        <ul className="content">
          {lists.map((list, i) => (
            <li key={i} onClick={list.onClick}>
              {list.title}
            </li>
          ))}
          {/* <li>Edit</li>
          <li>Delete</li> */}
        </ul>
      )}
    </div>
  );
}
