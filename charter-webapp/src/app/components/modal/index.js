import React from "react";
import { Modal } from "react-bootstrap";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ handleClose, show, title, children }) {
  return (
    <Modal
      show={show}
      onHide={handleClose}
      className="crew-input-modal"
      backdrop="static"
      centered
    >
      <Modal.Header closeButton className="crew-modal-header">
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body className="crew-modal-body">{children}</Modal.Body>
    </Modal>
  );
}
