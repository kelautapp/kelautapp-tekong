import * as Yup from "yup";
import i18n from "../../../i18n";

export const validate = Yup.object({
  oldPassword: Yup.string().required(i18n.t("ThisFieldIsRequired")),
  newPassword: Yup.string().required(i18n.t("ThisFieldIsRequired")),
});
