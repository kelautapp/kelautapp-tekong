import React from "react";
import { FormBuilderComponent } from "..";
import { validate } from "./validate";
import i18n from "../../../i18n";

const ChangePasswordFormStructure = {
  formSplit: "one",
  forms: [
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("CurrentPassword"),
        type: "password",
        className: "form-control form-control-lg input-border",
        id: "oldPassword",
        name: "oldPassword",
        placeholder: i18n.t("CurrentPasswordPlaceholder"),
      },
    },
    {
      type: "input-text",
      formStructure: {
        label: i18n.t("NewPassword"),
        type: "password",
        className: "form-control form-control-lg input-border",
        id: "newPassword",
        name: "newPassword",
        placeholder: i18n.t("NewPasswordPlaceholder"),
      },
    },
  ],
  formButton: [
    {
      type: "submit",
      name: i18n.t("ChangePasswordButton"),
      className:
        "btn btn-lg font-weight-medium auth-form-btn kelaut-primary-btn",
    },
  ],
};
export default function ChangePasswordForm({ onHandleSubmit = () => {} }) {
  return (
    <FormBuilderComponent
      initialValue={{
        oldPassword: "",
        newPassword: "",
      }}
      validation={validate}
      structures={ChangePasswordFormStructure}
      onHandleSubmit={(data) => onHandleSubmit(data)}
    />
  );
}
