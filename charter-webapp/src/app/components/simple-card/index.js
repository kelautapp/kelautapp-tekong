import React from "react";
import BaseVariantComp from "../../kelaut/animations/framer-variants/components/base-variant-comp";
import { generalCardVariants } from "../../kelaut/animations/framer-variants/general-page";
import KelautCard from "../kelaut-card";

export default function SimpleCard({
  title,
  enableOptions = false,
  customOptions = null,
  children,
}) {
  return (
    <BaseVariantComp variants={generalCardVariants}>
      <KelautCard
        title={title}
        enableOptions={enableOptions}
        customOptions={customOptions}
      >
        {children}
      </KelautCard>
    </BaseVariantComp>
  );
}
