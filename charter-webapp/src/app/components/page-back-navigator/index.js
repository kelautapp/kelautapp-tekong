import React from "react";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ title, ...otherProps }) {
  return (
    <div className="back-page-container" {...otherProps}>
      <p>
        <i className="mdi mdi-chevron-left" /> <span>{title}</span>
      </p>
    </div>
  );
}
