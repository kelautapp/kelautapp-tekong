import React from "react";
import * as moment from "moment";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ date }) {
  function getMonth(date) {
    return moment(date).format("MMM");
  }

  function getDate(date) {
    return moment(date).format("D");
  }

  function getDay(date) {
    return moment(date).format("ddd");
  }
  return (
    <div className="booking-date">
      <p className="day">{getDay(date)}</p>
      <p className="date">{getDate(date)}</p>
      <p className="month">{getMonth(date)}</p>
    </div>
  );
}
