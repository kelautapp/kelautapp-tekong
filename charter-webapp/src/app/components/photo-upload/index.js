import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import ImageUploading from "react-images-uploading";
import { v4 as uuidv4 } from "uuid";
import PhotoList from "../photo-list";

export default function PhotoUpload({
  boatId = null,
  boatPhoto = [],
  handleUpload = () => {},
  handleDelete = () => {},
}) {
  const [imagesUpload, setImagesUpload] = useState([]);
  const [t] = useTranslation();
  const maxNumber = 20;

  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    const newImageList = imageList.map((image) => {
      let tempFile = { ...image };
      tempFile["name"] = generateFileName(tempFile.file.name);
      tempFile["boatId"] = boatId;
      return tempFile;
    });

    setImagesUpload(newImageList);
  };

  function generateFileName(name) {
    return `${boatId}/${uuidv4()}-${name}`;
  }

  async function uploadImageToS3(images) {
    await handleUpload(images);
  }

  function handleConfirm(id) {
    handleDelete(id);
  }

  useEffect(() => {
    if (imagesUpload.length > 0) {
      uploadImageToS3(imagesUpload);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [imagesUpload]);

  return (
    <div>
      {boatPhoto.map((photo) => (
        <PhotoList key={photo.id} data={photo} onConfirm={handleConfirm} />
      ))}
      <ImageUploading
        multiple
        value={imagesUpload}
        onChange={onChange}
        maxNumber={maxNumber}
        dataURLKey="data_url"
        acceptType={["jpg", "png", "jpeg"]}
      >
        {({
          imageList,
          onImageUpload,
          onImageRemoveAll,
          onImageUpdate,
          onImageRemove,
          isDragging,
          dragProps,
        }) => (
          // write your building UI
          <div>
            <div className="kelaut-upload" onClick={onImageUpload}>
              <p>
                <i className="mdi mdi-plus" />
                {t("UploadBoatPhoto")}
              </p>
            </div>
          </div>
        )}
      </ImageUploading>
    </div>
  );
}
