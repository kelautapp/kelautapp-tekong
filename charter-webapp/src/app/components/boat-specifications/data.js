export default [
  { id: 1, title: "Horse Power", detail: "1050 HP" },
  { id: 2, title: "Maximum Speed", detail: "25 knots" },
  { id: 3, title: "Length", detail: "57 ft" },
  { id: 4, title: "Capacity", detail: "6 persons" },
  { id: 5, title: "Year Model", detail: "2001" },
];
