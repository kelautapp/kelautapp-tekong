import React, { useEffect, useState, useContext } from "react";
import { useTranslation } from "react-i18next";
import ToastrService from "../../../services/toastr";
import { LoaderContext } from "../../hooks";
import { useBoatSpecification } from "../../hooks/boat-management/boat-specification";
import BaseVariantComp from "../../kelaut/animations/framer-variants/components/base-variant-comp";
import { generalCardVariants } from "../../kelaut/animations/framer-variants/general-page";
import { UpdateBoatSpecification } from "../../types/boat/boat-specification";
import SpecForm from "../boat-package/spec-form";
import DisplayInfo from "../display-info";
import KelautCard from "../kelaut-card";
import Modal from "../modal";
import SubMenu from "../sub-menu";

// eslint-disable-next-line import/no-anonymous-default-export
export default function ({ data, showList = null }) {
  const [show, setShow] = useState(false);
  const [formattedData, setFormattedData] = useState(null);
  const { setLoading } = useContext(LoaderContext);
  const {
    loading,
    data: specData,
    completeUpdate,
    onUpdate,
  } = useBoatSpecification();
  const [t] = useTranslation();

  function openModal() {
    setShow(true);
  }

  function handleCloseModal() {
    setShow(false);
  }

  function getListOfActions() {
    return [
      {
        title: "Edit",
        onClick: () => handleAction("edit"),
      },
    ];
  }

  function handleAction(mode) {
    if (mode === "edit") {
      openModal(true);
    }
  }

  function onHandleSubmit(specData) {
    let spec = new UpdateBoatSpecification(specData);
    onUpdate(spec);
  }

  function formatTheDataToDisplay(data) {
    if (data) {
      if (showList) {
        let tempData = {};
        // eslint-disable-next-line array-callback-return
        Object.keys(data).map((k) => {
          if (showList[k]) {
            tempData[showList[k]] = data[k];
          }
        });

        setFormattedData(tempData);
      } else {
        setFormattedData({ ...data });
      }
    }
  }

  function getUnit(value, key) {
    switch (key) {
      case "Horse Power":
        return `${value} HP`;
      case "Maximum Speed":
        return `${value} Knots`;
      case "Length":
        return `${value} ft`;
      case "Capacity":
        return `${value} person`;
      default:
        return value;
    }
  }

  useEffect(() => {
    if (data) formatTheDataToDisplay(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  useEffect(() => {
    if (loading !== null) setLoading(loading);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (completeUpdate !== null) {
      setShow(false);
      ToastrService.success(t("SuccessUpdateBoatSpec"));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [completeUpdate, t]);

  useEffect(() => {
    if (specData) formatTheDataToDisplay(specData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [specData]);

  return (
    <BaseVariantComp variants={generalCardVariants}>
      <KelautCard
        title={t("BoatSpecTitle")}
        enableOptions
        customOptions={() => <SubMenu lists={getListOfActions()} />}
      >
        {formattedData &&
          Object.keys(formattedData).map((k) => {
            return (
              <DisplayInfo
                key={k}
                title={t(k)}
                detail={getUnit(formattedData[k], k)}
              />
            );
          })}

        <Modal
          title={t("EditBoatSpecModal")}
          handleClose={() => handleCloseModal()}
          show={show}
        >
          <SpecForm initial={data} onHandleSubmit={onHandleSubmit} />
        </Modal>
      </KelautCard>
    </BaseVariantComp>
  );
}
