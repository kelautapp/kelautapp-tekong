import React, { useState, useEffect } from "react";
import { useWeatherMap } from "../../hooks/weather";
import WeatherIcon from "../weather-icon";
import CloudDay from "../../../assets/images/weather/cloudy.png";
import Raining from "../../../assets/images/weather/rain.png";
import Thunderstorm from "../../../assets/images/weather/heavy-rain.png";
import Clear from "../../../assets/images/weather/sunny.png";

import * as moment from "moment";
import Skeleton from "react-loading-skeleton";
import { useTranslation } from "react-i18next";

const WeatherStatus = {
  clouds: {
    comp: <WeatherIcon path={CloudDay} size="small" />,
    desc: "Cold & Sunshine",
  },
  rain: { comp: <WeatherIcon path={Raining} size="small" />, desc: "Raining" },
  thunderstorm: {
    comp: <WeatherIcon path={Thunderstorm} size="small" />,
    desc: "Heavy Rain",
  },
  clear: { comp: <WeatherIcon path={Clear} size="small" />, desc: "Sunny Day" },
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function () {
  const [lat, setLat] = useState(1.164);
  const [lng, setLng] = useState(101.898);
  const [weatherData, setWeatherData] = useState(null);
  const { loading, datas, getCurrentWeatherByCoordinate } = useWeatherMap();
  const [t] = useTranslation();

  function setPosition(position) {
    setLat(position.coords.latitude);
    setLng(position.coords.longitude);
  }

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(setPosition);
  }, []);

  useEffect(() => {
    getCurrentWeatherByCoordinate(lat, lng, 40);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lat, lng]);

  function formattedStatusToKelautStatus(status) {
    switch (status) {
      case "thunderstorm":
        return "thunderstorm";
      case "drizzle":
      case "rain":
        return "rain";
      case "clear":
        return "clear";
      default:
        return "clouds";
    }
  }

  useEffect(() => {
    if (datas) {
      if (datas.length > 0) {
        let temp = {};
        // eslint-disable-next-line array-callback-return
        datas.forEach((d) => {
          let split = d.dt_txt.split(" ");
          if (!temp[split[0]]) {
            temp[split[0]] = {
              am: null,
              pm: null,
            };
          }

          temp[split[0]][split[1] === "09:00:00" ? "am" : "pm"] = {
            temp: Math.round(d.main.temp, 2),
            status: formattedStatusToKelautStatus(
              d.weather[0].main.toLowerCase()
            ),
            day: t(moment(split[0], "YYYY-MM-DD").format("ddd")),
            month: moment(split[0], "YYYY-MM-DD").format("DD MMM"),
          };
        });
        setWeatherData(temp);
      }
    }
  }, [datas, t]);

  return (
    <div className="weather-forecast">
      {loading ? (
        <Skeleton height={200} />
      ) : (
        <>
          <div className="header">
            <div className="date-section"></div>
            <div className="time-section">
              <div>{t("Morning")}</div>
              <div>{t("Evening")}</div>
            </div>
          </div>
          {weatherData &&
            Object.keys(weatherData).map(
              (key, i) =>
                weatherData[key].pm &&
                weatherData[key].am && (
                  <div className="content" key={weatherData[key].dt + " " + i}>
                    <div className="date-data">
                      <span className="day">{weatherData[key].am.day}, </span>
                      <span className="month">{weatherData[key].am.month}</span>
                    </div>
                    <div className="symbol">
                      <div className="morning">
                        <div className="image">
                          {WeatherStatus[weatherData[key].am.status].comp}
                          <span>{weatherData[key].am.temp}</span>
                        </div>
                      </div>
                      <div className="evening">
                        <div className="image">
                          {WeatherStatus[weatherData[key].pm.status].comp}
                          {/* <Raining></Raining> */}
                          <span>{weatherData[key].pm.temp}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                )
            )}
        </>
      )}
    </div>
  );
}
