/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getProfile = /* GraphQL */ `
  query GetProfile($id: ID!) {
    getProfile(id: $id) {
      id
      name
      email
      companyName
      location
      phone
      birthDate
      language
      nationality
      startCareer
      status
      photo
      backdrop
    }
  }
`;
export const getProfileByEmail = /* GraphQL */ `
  query GetProfileByEmail($email: AWSEmail!) {
    getProfileByEmail(email: $email) {
      id
      name
      email
      companyName
      location
      phone
      birthDate
      language
      nationality
      startCareer
      status
      photo
      backdrop
    }
  }
`;
export const listProfile = /* GraphQL */ `
  query ListProfile {
    listProfile {
      id
      name
      email
      companyName
      location
      phone
      birthDate
      language
      nationality
      startCareer
      status
      photo
      backdrop
    }
  }
`;
export const getVerification = /* GraphQL */ `
  query GetVerification($id: ID!) {
    getVerification(id: $id) {
      id
      status
      description
      profileId
    }
  }
`;
export const getVerificationByProfileId = /* GraphQL */ `
  query GetVerificationByProfileId($profileId: ID!) {
    getVerificationByProfileId(profileId: $profileId) {
      id
      status
      description
      profileId
    }
  }
`;
export const getBoat = /* GraphQL */ `
  query GetBoat($id: ID!) {
    getBoat(id: $id) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
      amenities
      fishingType
    }
  }
`;
export const getBoats = /* GraphQL */ `
  query GetBoats($profileId: ID!) {
    getBoats(profileId: $profileId) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
    }
  }
`;
export const getBoatEngine = /* GraphQL */ `
  query GetBoatEngine($boatId: ID!) {
    getBoatEngine(boatId: $boatId) {
      id
      manufacturer
      number
      engineCount
      horsePower
      speed
      status
      boatId
    }
  }
`;
export const getBoatNavigation = /* GraphQL */ `
  query GetBoatNavigation($boatId: ID!) {
    getBoatNavigation(boatId: $boatId) {
      id
      number
      deviceName
      status
      boatId
    }
  }
`;
export const getBoatFacility = /* GraphQL */ `
  query GetBoatFacility($boatId: ID!) {
    getBoatFacility(boatId: $boatId) {
      id
      number
      facilityName
      status
      boatId
    }
  }
`;
export const getBoatFeatures = /* GraphQL */ `
  query GetBoatFeatures($boatId: ID!) {
    getBoatFeatures(boatId: $boatId) {
      id
      number
      featureName
      status
      boatId
    }
  }
`;
export const getBoatFishingType = /* GraphQL */ `
  query GetBoatFishingType($boatId: ID!) {
    getBoatFishingType(boatId: $boatId) {
      id
      number
      fishingType
      status
      boatId
    }
  }
`;
export const getBoatPackage = /* GraphQL */ `
  query GetBoatPackage($boatId: ID!) {
    getBoatPackage(boatId: $boatId) {
      id
      number
      packageName
      tripHour
      startTime
      description
      price
      boatId
    }
  }
`;

export const getBoatPhoto = /* GraphQL */ `
  query GetBoatPhoto($boatId: ID!) {
    getBoatPhoto(boatId: $boatId) {
      id
      name
      type
      size
      fileKey
      createdAt
      updatedAt
      boatId
    }
  }
`;

export const getCrewByProfileId = /* GraphQL */ `
  query GetCrewByProfileId($profileId: ID!) {
    getCrewByProfileId(profileId: $profileId) {
      id
      name
      idNumber
      dob
      photo
      status
      profileId
    }
  }
`;

export const getBoatCrewByBoatId = /* GraphQL */ `
  query GetBoatCrewByBoatId($boatId: ID!) {
    getBoatCrewByBoatId(boatId: $boatId) {
      id
      name
      idNumber
      dob
      photo
      profileId
      boatId
      crewId
      role
    }
  }
`;
