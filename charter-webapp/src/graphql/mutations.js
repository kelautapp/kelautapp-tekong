/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createProfile = /* GraphQL */ `
  mutation CreateProfile($input: CreateProfile) {
    createProfile(input: $input) {
      id
      name
      email
      companyName
      location
      phone
      birthDate
      language
      nationality
      startCareer
      status
      photo
      backdrop
    }
  }
`;
export const updateProfile = /* GraphQL */ `
  mutation UpdateProfile($input: UpdateProfile) {
    updateProfile(input: $input) {
      id
      name
      email
      companyName
      location
      phone
      birthDate
      language
      nationality
      startCareer
      status
      photo
      backdrop
    }
  }
`;
export const deleteProfile = /* GraphQL */ `
  mutation DeleteProfile($input: DeleteProfile) {
    deleteProfile(input: $input) {
      id
      name
      email
      companyName
      location
      phone
      birthDate
      language
      nationality
      startCareer
      status
      photo
      backdrop
    }
  }
`;
export const createVerification = /* GraphQL */ `
  mutation CreateVerification($input: CreateVerification) {
    createVerification(input: $input) {
      id
      status
      description
      profileId
    }
  }
`;
export const updateVerification = /* GraphQL */ `
  mutation UpdateVerification($input: UpdateVerification) {
    updateVerification(input: $input) {
      id
      status
      description
      profileId
    }
  }
`;
export const createVerificationDoc = /* GraphQL */ `
  mutation CreateVerificationDoc($input: CreateVerificationDoc) {
    createVerificationDoc(input: $input) {
      id
      name
      type
      size
      fileKey
      createdAt
      updatedAt
      verificationId
    }
  }
`;
export const createBoat = /* GraphQL */ `
  mutation CreateBoat($input: CreateBoat) {
    createBoat(input: $input) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
      amenities
      fishingType
    }
  }
`;
export const updateBoat = /* GraphQL */ `
  mutation UpdateBoat($input: UpdateBoat) {
    updateBoat(input: $input) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
      amenities
      fishingType
    }
  }
`;

export const updateBoatStatus = /* GraphQL */ `
  mutation UpdateBoatStatus($input: UpdateBoatStatus) {
    updateBoatStatus(input: $input) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
      amenities
      fishingType
    }
  }
`;

export const updateBoatLocation = /* GraphQL */ `
  mutation UpdateBoatLocation($input: UpdateBoatLocation) {
    updateBoatLocation(input: $input) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
    }
  }
`;
export const createBoatEngine = /* GraphQL */ `
  mutation CreateBoatEngine($input: CreateBoatEngine) {
    createBoatEngine(input: $input) {
      id
      manufacturer
      number
      engineCount
      horsePower
      speed
      status
      boatId
    }
  }
`;
export const updateBoatEngine = /* GraphQL */ `
  mutation UpdateBoatEngine($input: UpdateBoatEngine) {
    updateBoatEngine(input: $input) {
      id
      manufacturer
      number
      engineCount
      horsePower
      speed
      status
      boatId
    }
  }
`;
export const createBoatNavigation = /* GraphQL */ `
  mutation CreateBoatNavigation($input: CreateBoatNavigation) {
    createBoatNavigation(input: $input) {
      id
      number
      deviceName
      status
      boatId
    }
  }
`;
export const updateBoatNavigation = /* GraphQL */ `
  mutation UpdateBoatNavigation($input: UpdateBoatNavigation) {
    updateBoatNavigation(input: $input) {
      id
      number
      deviceName
      status
      boatId
    }
  }
`;
export const createBoatFacility = /* GraphQL */ `
  mutation CreateBoatFacility($input: CreateBoatFacility) {
    createBoatFacility(input: $input) {
      id
      number
      facilityName
      status
      boatId
    }
  }
`;
export const updateBoatFacility = /* GraphQL */ `
  mutation UpdateBoatFacility($input: UpdateBoatFacility) {
    updateBoatFacility(input: $input) {
      id
      number
      facilityName
      status
      boatId
    }
  }
`;
export const createBoatFeatures = /* GraphQL */ `
  mutation CreateBoatFeatures($input: CreateBoatFeatures) {
    createBoatFeatures(input: $input) {
      id
      number
      featureName
      status
      boatId
    }
  }
`;
export const updateBoatFeatures = /* GraphQL */ `
  mutation UpdateBoatFeatures($input: UpdateBoatFeatures) {
    updateBoatFeatures(input: $input) {
      id
      number
      featureName
      status
      boatId
    }
  }
`;
export const createBoatFishingType = /* GraphQL */ `
  mutation CreateBoatFishingType($input: CreateBoatFishingType) {
    createBoatFishingType(input: $input) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
      amenities
      fishingType
    }
  }
`;
export const updateBoatFishingType = /* GraphQL */ `
  mutation UpdateBoatFishingType($input: UpdateBoatFishingType) {
    updateBoatFishingType(input: $input) {
      id
      number
      fishingType
      status
      boatId
    }
  }
`;
export const createBoatPackage = /* GraphQL */ `
  mutation CreateBoatPackage($input: CreateBoatPackage) {
    createBoatPackage(input: $input) {
      id
      number
      packageName
      tripHour
      startTime
      description
      price
      boatId
    }
  }
`;
export const updateBoatPackage = /* GraphQL */ `
  mutation UpdateBoatPackage($input: UpdateBoatPackage) {
    updateBoatPackage(input: $input) {
      id
      number
      packageName
      tripHour
      startTime
      description
      price
      boatId
    }
  }
`;

export const deleteBoatPackage = /* GraphQL */ `
  mutation DeleteBoatPackage($input: DeleteBoatPackage) {
    deleteBoatPackage(input: $input) {
      id
      number
      packageName
      tripHour
      startTime
      description
      price
      boatId
    }
  }
`;

export const updateBoatSpecification = /* GraphQL */ `
  mutation UpdateBoatSpecification($input: UpdateBoatSpecification) {
    updateBoatSpecification(input: $input) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
      amenities
      fishingType
    }
  }
`;

export const createBoatAmenities = /* GraphQL */ `
  mutation CreateBoatAmenities($input: CreateBoatAmenities) {
    createBoatAmenities(input: $input) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
      amenities
      fishingType
    }
  }
`;

export const updateBoatSummary = /* GraphQL */ `
  mutation UpdateBoatSummary($input: UpdateBoatSummary) {
    updateBoatSummary(input: $input) {
      id
      number
      name
      location
      lat
      lng
      description
      rating
      horsePower
      maxSpeed
      length
      capacity
      year
      zoom
      profileId
      status
      amenities
      fishingType
    }
  }
`;

export const createBoatPhoto = /* GraphQL */ `
  mutation CreateBoatPhoto($input: CreateBoatPhoto) {
    createBoatPhoto(input: $input) {
      id
      name
      type
      size
      fileKey
      createdAt
      updatedAt
      boatId
    }
  }
`;

export const deleteBoatPhoto = /* GraphQL */ `
  mutation DeleteBoatPhoto($input: DeleteBoatPhoto) {
    deleteBoatPhoto(input: $input) {
      id
      name
      type
      size
      fileKey
      createdAt
      updatedAt
      boatId
    }
  }
`;

export const createCrew = /* GraphQL */ `
  mutation CreateCrew($input: CreateCrew) {
    createCrew(input: $input) {
      id
      name
      idNumber
      dob
      photo
      status
      profileId
    }
  }
`;

export const updateCrew = /* GraphQL */ `
  mutation UpdateCrew($input: UpdateCrew) {
    updateCrew(input: $input) {
      id
      name
      idNumber
      dob
      photo
      status
      profileId
    }
  }
`;

export const updateCrewStatus = /* GraphQL */ `
  mutation UpdateCrewStatus($input: UpdateCrewStatus) {
    updateCrewStatus(input: $input) {
      id
      name
      idNumber
      dob
      photo
      status
      profileId
    }
  }
`;

export const createBoatCrew = /* GraphQL */ `
  mutation CreateBoatCrew($input: CreateBoatCrew) {
    createBoatCrew(input: $input) {
      id
      name
      idNumber
      dob
      photo
      profileId
      boatId
      crewId
      role
    }
  }
`;

export const deleteBoatCrew = /* GraphQL */ `
  mutation DeleteBoatCrew($input: DeleteBoatCrew) {
    deleteBoatCrew(input: $input) {
      id
      name
      idNumber
      dob
      photo
      profileId
      boatId
      crewId
      role
    }
  }
`;

export const updateBoatCrew = /* GraphQL */ `
  mutation UpdateBoatCrew($input: UpdateBoatCrew) {
    updateBoatCrew(input: $input) {
      id
      name
      idNumber
      dob
      photo
      profileId
      boatId
      crewId
      role
    }
  }
`;
