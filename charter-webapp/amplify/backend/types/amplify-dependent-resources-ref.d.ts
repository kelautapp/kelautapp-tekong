export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "charterwebappba0c7a9e": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        }
    },
    "hosting": {
        "S3AndCloudFront": {
            "Region": "string",
            "HostingBucketName": "string",
            "WebsiteURL": "string",
            "S3BucketSecureURL": "string"
        }
    },
    "storage": {
        "charterfiles": {
            "BucketName": "string",
            "Region": "string"
        }
    }
}